﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InventorPortal/InventorPortal.Master" AutoEventWireup="true" CodeBehind="CreateAccount.aspx.cs" Inherits="Inteum.Server.Web.InventorPortal.CreateAccount"  enableEventValidation="false" %>
<%@ Register src="~/WebControls/SetupAccount.ascx" tagname="SetupAccount" tagprefix="wctrl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<tb:TitleBar ID="title1" runat="server" Text="Create Account" />

<div class="content_indent">
<div id="divSubTitle" runat="server" class="subtitle">Please confirm your contact information below or enter in the required fields to associate your account with a contact record.</div>
<div id="divAlreadyAccount" runat="server" class="subtitle"><span id="spanAlreadyAccount" runat="server">Already have an account?</span> <a href="login.aspx"><span id="loginhere" runat="server">Login Here</span></a></div>

<wctrl:SetupAccount ID="setupAccount1" runat="server" IsLoginProtected="false" IsDisplayCredentials="true" OnSelectionMade="setupAccount1_SelectionMade" />

<wtf:FormButtonControl ID="btnSave" runat="server" Text="Create an Account" OnClick="btnSave_Click" LoadingText="Creating..." IsWrapTable="true" />
<div style="height:10px;"></div>
</div>

<wtd:ModalDialogControl ID="mdcNextSteps" runat="server" IsDefaultButtons="false" Title="Next Steps" IsHideTopClose="true">
    <DialogContent>
        <asp:Label ID="lblNextSteps" runat="server"></asp:Label>
    </DialogContent>
    <DialogFooter>
        <asp:Button ID="btnOk" runat="server" Text="OK" OnClick="btnOk_Click" />
    </DialogFooter>
</wtd:ModalDialogControl>

<wtc:ConfirmPromptControl ID="cpcWarning" runat="server" PromptType="Warning" PromptButtons="Close" />

</asp:Content>
