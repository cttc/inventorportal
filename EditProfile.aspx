﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InventorPortal/InventorPortal.Master" AutoEventWireup="true" CodeBehind="EditProfile.aspx.cs" Inherits="Inteum.Server.Web.InventorPortal.EditProfile" %>
<%@ Register src="~/WebControls/SetupAccount.ascx" tagname="SetupAccount" tagprefix="wctrl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<tb:TitleBar ID="title1" runat="server" Text="Edit Profile" />

<div class="content_indent">

<wctrl:SetupAccount ID="setupAccount1" runat="server" OnPostbackOccurred="setupAccount1_PostbackOccurred"/>

<wtf:FormButtonControl ID="btnSave" runat="server" Text="Save Changes" OnClick="btnSave_Click" LoadingText="Saving..." IsWrapTable="true" />

</div>

<wtc:ConfirmPromptControl ID="cpcError" runat="server" PromptType="Error" />
<wtc:ConfirmPromptControl ID="cpcSuccess" runat="server" PromptType="Success" />

</asp:Content>
