//<![CDATA[

var   bMaintance = false;
var   btnClicked  = null;
var   anchorClicked = null;
var   old_WebForm_OnSubmi = null;
var   observer = [];
var   last_sel = [];
var sysApplication = Sys.WebForms.PageRequestManager.getInstance();
sysApplication.add_endRequest(endRequest);

function beginRequest() {
    alert('AJAX Request started');
  
}

function endRequest() {
  console.log('***********endRequest*************');
  if ( $('body').hasClass('Add_New_Disclosure') )
    PrepDisclosureForm();
}       


observer['disclosure'] = new MutationObserver( function( mutations ) {
  var bUpdate = false;
  var mutationTarget;

  console.log("In MutationObserver['disclosure']");

  // Select dropdown elements  
  switch (mutations[0].target.nodeName) {
    case 'SELECT': 
      mutationTarget = "select[name='" + mutations[0].target.name  + "']";
      break;
    case 'DIV':
      if ( mutations[0].target.nextElementSibling.localName == 'input' )
        mutationTarget = "input[type=checkbox]#" + mutations[0].target.nextElementSibling.id;
      break;
  }
  switch (mutationTarget) {
    case disclosure.CreativeWork.checkbox.federal:
    case disclosure.CreativeWork.checkbox.foundation:
    case disclosure.CreativeWork.checkbox.industry:
    case disclosure.CreativeWork.checkbox.internal:
    case disclosure.CreativeWork.checkbox.other:
    case disclosure.CreativeWork.checkbox.state:
    case disclosure.CreativeWork.checkbox.none:

    case disclosure.Invention.checkbox.federal:
    case disclosure.Invention.checkbox.foundation:
    case disclosure.Invention.checkbox.industry:
    case disclosure.Invention.checkbox.internal:
    case disclosure.Invention.checkbox.other:
    case disclosure.Invention.checkbox.state:
    case disclosure.Invention.checkbox.none:

    case disclosure.Software.checkbox.federal:
    case disclosure.Software.checkbox.foundation:
    case disclosure.Software.checkbox.industry:
    case disclosure.Software.checkbox.internal:
    case disclosure.Software.checkbox.other:
    case disclosure.Software.checkbox.state:
    case disclosure.Software.checkbox.none:

    case disclosure.TangibleMaterials.checkbox.federal:
    case disclosure.TangibleMaterials.checkbox.foundation:
    case disclosure.TangibleMaterials.checkbox.industry:
    case disclosure.TangibleMaterials.checkbox.internal:
    case disclosure.TangibleMaterials.checkbox.other:
    case disclosure.TangibleMaterials.checkbox.state:
    case disclosure.TangibleMaterials.checkbox.none:     {
      bUpdate = true; 
      
      break;      
    }
        
    default: 
    case disclosure.CreativeWork.lic_officer: 
    case disclosure.Invention.lic_officer: 
    case disclosure.Software.lic_officer: 
    case disclosure.TangibleMaterials.lic_officer: 
    case disclosure.CreativeWork.mod_improvement:
    case disclosure.CreativeWork.publications: 
    case disclosure.Invention.publications: 
    case disclosure.Software.publications:    
    case disclosure.TangibleMaterials.material_related_vi: 
    case disclosure.Software.modification: { 
      bUpdate = true;

      break;
    }
  
    
    case disclosure.CreativeWork.third_party: {
      var label = $(mutations[0].target).find(":selected")[0].label;;
      if ( typeof last_sel[disclosure.CreativeWork.third_party] === 'undefined' || 
          last_sel[disclosure.CreativeWork.third_party] === null )
        last_sel[disclosure.CreativeWork.third_party] = label;
      else {
        if ( last_sel[disclosure.CreativeWork.third_party] != label )
          last_sel[disclosure.CreativeWork.third_party] = label;
          bUpdate = true;
      }

      if ( label === 'Yes' ) {
        observerNode = document.querySelector( disclosure.CreativeWork.mta_signed );
        if ( observerNode != null && observerNode.length )
          observer['disclosure'].observe(observerNode, {attributes:true});
      }
      break;
    }
    case disclosure.Invention.third_party: {
      var label = $(mutations[0].target).find(":selected")[0].label;;
      if ( typeof last_sel[disclosure.Invention.third_party] === 'undefined' || 
          last_sel[disclosure.Invention.third_party] === null )
        last_sel[disclosure.Invention.third_party] = label;
      else {
        if ( last_sel[disclosure.Invention.third_party] != label )
          last_sel[disclosure.Invention.third_party] = label;
          bUpdate = true;
      }

      if ( label === 'Yes' ) {
        observerNode = document.querySelector( disclosure.Invention.mta_signed );
        if ( observerNode != null && observerNode.length )
          observer['disclosure'].observe(observerNode, {attributes:true});
      }
      break;
    }
    case disclosure.Software.third_party: {
      var label = $(mutations[0].target).find(":selected")[0].label;;
      if ( typeof last_sel[disclosure.Software.third_party] === 'undefined' || 
          last_sel[disclosure.Software.third_party] === null )
        last_sel[disclosure.Software.third_party] = label;
      else {
        if ( last_sel[disclosure.Software.third_party] != label )
          last_sel[disclosure.Software.third_party] = label;
          bUpdate = true;
      }

      if ( label === 'Yes' ) {
        observerNode = document.querySelector( disclosure.Software.mta_signed );
        if ( observerNode != null && observerNode.length )
          observer['disclosure'].observe(observerNode, {attributes:true});
      }
      break;
    }
    case disclosure.TangibleMaterials.third_party: {
      var label = $(mutations[0].target).find(":selected")[0].label;;
      if ( typeof last_sel[disclosure.TangibleMaterials.third_party] === 'undefined' || 
          last_sel[disclosure.TangibleMaterials.third_party] === null )
        last_sel[disclosure.TangibleMaterials.third_party] = label;
      else {
        if ( last_sel[disclosure.TangibleMaterials.third_party] != label )
          last_sel[disclosure.TangibleMaterials.third_party] = label;
          bUpdate = true;
      }

      if ( label === 'Yes' ) {
        observerNode = document.querySelector( disclosure.TangibleMaterials.mta_signed );
        if ( observerNode != null && observerNode.length )
          observer['disclosure'].observe(observerNode, {attributes:true});
      }
      break;
    }
    case disclosure.CreativeWork.mta_signed: {
      if ( typeof last_sel[disclosure.CreativeWork.third_party] === 'undefined' || 
          last_sel[disclosure.CreativeWork.third_party] === null ) {
        if (  $(mutations[0].target).find(":selected").length )
          last_sel[disclosure.CreativeWork.third_party] = $(mutations[0].target).find(":selected")[0].label;
        else
          last_sel[disclosure.Software.third_party] = null;
      }
      else {
        if ( last_sel[disclosure.CreativeWork.third_party] != $(mutations[0].target).find(":selected")[0].label )
          last_sel[disclosure.CreativeWork.third_party] = $(mutations[0].target).find(":selected")[0].label;
          bUpdate = true;
      }
      break;
    }
    case disclosure.Invention.mta_signed: {
      if ( typeof last_sel[disclosure.Invention.third_party] === 'undefined' || 
          last_sel[disclosure.Invention.third_party] === null ) {
        if (  $(mutations[0].target).find(":selected").length )
          last_sel[disclosure.Invention.third_party] = $(mutations[0].target).find(":selected")[0].label;
        else
          last_sel[disclosure.Software.third_party] = null;        
      }
      else {
        if ( last_sel[disclosure.Invention.third_party] != $(mutations[0].target).find(":selected")[0].label )
          last_sel[disclosure.Invention.third_party] = $(mutations[0].target).find(":selected")[0].label;
          bUpdate = true;
      }
      break;
    }
    case disclosure.Software.mta_signed: {
      if ( typeof last_sel[disclosure.Software.third_party] === 'undefined' || 
          last_sel[disclosure.Software.third_party] === null ) {
        if (  $(mutations[0].target).find(":selected").length )
          last_sel[disclosure.Software.third_party] = $(mutations[0].target).find(":selected")[0].label;
        else
          last_sel[disclosure.Software.third_party] = null;
      }
      else {
        if ( last_sel[disclosure.Software.third_party] != $(mutations[0].target).find(":selected")[0].label )
          last_sel[disclosure.Software.third_party] = $(mutations[0].target).find(":selected")[0].label;
          bUpdate = true;
      }
      break;      
    }
    case disclosure.TangibleMaterials.mta_signed: {
      if ( typeof last_sel[disclosure.TangibleMaterials.third_party] === 'undefined' || 
          last_sel[disclosure.TangibleMaterials.third_party] === null ) {
        if (  $(mutations[0].target).find(":selected").length )
          last_sel[disclosure.TangibleMaterials.third_party] = $(mutations[0].target).find(":selected")[0].label;
        else
          last_sel[disclosure.Software.third_party] = null;
      }
      else {
        if ( last_sel[disclosure.TangibleMaterials.third_party] != $(mutations[0].target).find(":selected")[0].label )
          last_sel[disclosure.TangibleMaterials.third_party] = $(mutations[0].target).find(":selected")[0].label;
          bUpdate = true;
      }
      break;
    }
    case disclosure.CreativeWork.export_control: {
      if ( typeof last_sel[disclosure.CreativeWork.export_control] === 'undefined' || 
          last_sel[disclosure.CreativeWork.export_control] === null )
        last_sel[disclosure.CreativeWork.export_control] = $(mutations[0].target).find(":selected")[0].label;
      else {
        if ( last_sel[disclosure.CreativeWork.export_control] != $(mutations[0].target).find(":selected")[0].label )
          last_sel[disclosure.CreativeWork.export_control] = $(mutations[0].target).find(":selected")[0].label;
          bUpdate = true;
      }
      break;
    }
    case disclosure.Invention.export_control: {
      if ( typeof last_sel[disclosure.Invention.export_control] === 'undefined' || 
          last_sel[disclosure.Invention.export_control] === null )
        last_sel[disclosure.Invention.export_control] = $(mutations[0].target).find(":selected")[0].label;
      else {
        if ( last_sel[disclosure.Invention.export_control] != $(mutations[0].target).find(":selected")[0].label )
          last_sel[disclosure.Invention.export_control] = $(mutations[0].target).find(":selected")[0].label;
          bUpdate = true;
      }
      break;
    }
    case disclosure.Software.export_control: {
      if ( typeof last_sel[disclosure.Software.export_control] === 'undefined' || 
          last_sel[disclosure.Software.export_control] === null )
        last_sel[disclosure.Software.export_control] = $(mutations[0].target).find(":selected")[0].label;
      else {
        if ( last_sel[disclosure.Software.export_control] != $(mutations[0].target).find(":selected")[0].label )
          last_sel[disclosure.Software.export_control] = $(mutations[0].target).find(":selected")[0].label;
          bUpdate = true;
      }
      break;
    }
    case disclosure.TangibleMaterials.export_control: {
      if ( typeof last_sel[disclosure.TangibleMaterials.export_control] === 'undefined' || 
          last_sel[disclosure.TangibleMaterials.export_control] === null )
        last_sel[disclosure.TangibleMaterials.export_control] = $(mutations[0].target).find(":selected")[0].label;
      else {
        if ( last_sel[disclosure.TangibleMaterials.export_control] != $(mutations[0].target).find(":selected")[0].label )
          last_sel[disclosure.TangibleMaterials.export_control] = $(mutations[0].target).find(":selected")[0].label;
          bUpdate = true;
      }
      break;
    }
  }


  // Update if we need to or IF, we have two (or more) mutations because
  // that means that there's been an addition... 
  if ( bUpdate ||  mutations.length > 1 ) {
    PrepDisclosureForm();
  }
    
  console.log("Out MutationObserver['disclosure']");
  
  return; 
} ); 

observer['misc_dlg'] = new MutationObserver(function(mutations) {
  var bDlgClosed = false;
  var mutationTarget;
  
  console.log("In MutationObserver['misc_dlg']");
 
  mutationTarget = "#" + mutations[0].target.id;
   console.log("                                - " + mutationTarget + "( " +  mutations[0].target.hidden + " )");

  switch( mutationTarget ) {
    case MiscDlg.main_confirmation: {
      bDlgClosed = true;
      break;       
    }
    case MiscDlg.AddInventor.dlgID: {
      if( $(mutations[0].target).is(':visible') ) {
        OnClick_AddInventorBtn();
      }
      else {
        bDlgClosed = true;
      }
      break;
    }    
    case MiscDlg.AddInventor.searching: {
      console.log("in");
      if( !$(mutations[0].target).is(':visible') ) {
        OnClick_AddInventorBtn();
      }
      break;
    }
  }
  
  if ( bDlgClosed ) {
    console.log("observer['misc_dlg'] disconnected");
    //observer['misc_dlg'].disconnect();
  }

  if (btnClicked != null) {
    OnReady();
    btnClicked = null; 
  }
  
  // Update if we need to or IF, we have two (or more) mutations because
  // that means that there's been an addition... 
  if ( btnClicked ||  mutations.length > 1 ) {
    PrepDisclosureForm();
  }  
  console.log("Out MutationObserver['misc_dlg']");
  
  return;   
});  

observer['misc_dlg_extend'] = new MutationObserver(function(mutations) {
  var bDlgClosed = false;
  var mutationTarget;
  
  console.log("In MutationObserver['misc_dlg_extend']");

 
  mutationTarget = "#" + mutations[0].target.id;
 
  switch( mutationTarget ) {
    case MiscDlg.AddInventor.searching: {
      onLoadingDlg(mutationTarget);
      break;
    }
  }
  
  if ( bDlgClosed ) {
    console.log("observer['misc_dlg_extend'] disconnected");
    observer['misc_dlg_extend'].disconnect();
  }
    
   console.log("Out MutationObserver['misc_dlg_extend']");

  return;   
});

var dashboard = {
  Default:          '12345',
  DetailedView:     '12345',
  Settings:         '12345',
  Users:            '12345',
  Logout:           '2342',
  Login:            '2234'
}

/**  Discloser Types **/
var disclosure = {
  CreativeWork: {  
    value:                '2355080',
    lic_officer:          "select[name='ctl00$ContentPlaceHolder1$customFields1$customfield2355109$ctl03']",
    mod_improvement:      "select[name='ctl00$ContentPlaceHolder1$customFields1$customfield2355234$ctl03']",     
    publications:         "select[name='ctl00$ContentPlaceHolder1$customFields1$customfield2419030$ctl03']",
    third_party:          "select[name='ctl00$ContentPlaceHolder1$customFields1$customfield2355221$ctl03']",
    mta_signed:           "select[name='ctl00$ContentPlaceHolder1$customFields1$customfield2355276$ctl03']",
    export_control:       "select[name='ctl00$ContentPlaceHolder1$customFields1$customfield2355281$ctl03']",
    checkbox : {
      federal:            "input[type='checkbox']#ctl00_ContentPlaceHolder1_customFields1_customfield2355192_cbxInput",
      foundation:         "input[type='checkbox']#ctl00_ContentPlaceHolder1_customFields1_customfield2355194_cbxInput",
      industry:           "input[type='checkbox']#ctl00_ContentPlaceHolder1_customFields1_customfield2355196_cbxInput",
      internal:           "input[type='checkbox']#ctl00_ContentPlaceHolder1_customFields1_customfield2355198_cbxInput",
      other:              "input[type='checkbox']#ctl00_ContentPlaceHolder1_customFields1_customfield2355202_cbxInput",
      state:              "input[type='checkbox']#ctl00_ContentPlaceHolder1_customFields1_customfield2355204_cbxInput",       
      none:               "input[type='checkbox']#ctl00_ContentPlaceHolder1_customFields1_customfield2355206_cbxInput"       
    }    
  },
  Invention: {
    value:                '2354643',
    lic_officer:          "select[name='ctl00$ContentPlaceHolder1$customFields1$customfield2354766$ctl03']",
    publications:         "select[name='ctl00$ContentPlaceHolder1$customFields1$customfield2354828$ctl03']",
    third_party:          "select[name='ctl00$ContentPlaceHolder1$customFields1$customfield2354893$ctl03']",
    mta_signed:           "select[name='ctl00$ContentPlaceHolder1$customFields1$customfield2354901$ctl03']",
    export_control:       "select[name='ctl00$ContentPlaceHolder1$customFields1$customfield2354932$ctl03']",
    checkbox : {
      federal:            "input[type='checkbox']#ctl00_ContentPlaceHolder1_customFields1_customfield2354848_cbxInput",
      foundation:         "input[type='checkbox']#ctl00_ContentPlaceHolder1_customFields1_customfield2354852_cbxInput",
      industry:           "input[type='checkbox']#ctl00_ContentPlaceHolder1_customFields1_customfield2354856_cbxInput",
      internal:           "input[type='checkbox']#ctl00_ContentPlaceHolder1_customFields1_customfield2354860_cbxInput",
      other:              "input[type='checkbox']#ctl00_ContentPlaceHolder1_customFields1_customfield2354864_cbxInput",
      state:              "input[type='checkbox']#ctl00_ContentPlaceHolder1_customFields1_customfield2354868_cbxInput",       
      none:               "input[type='checkbox']#ctl00_ContentPlaceHolder1_customFields1_customfield2354872_cbxInput"       
    }
  },
  Software: {
    value:                '2354691',
    lic_officer:          "select[name='ctl00$ContentPlaceHolder1$customFields1$customfield2355331$ctl03']",
    publications:         "select[name='ctl00$ContentPlaceHolder1$customFields1$customfield2419589$ctl03']",
    third_party:          "select[name='ctl00$ContentPlaceHolder1$customFields1$customfield2355366$ctl03']",
    mta_signed:           "select[name='ctl00$ContentPlaceHolder1$customFields1$customfield2355371$ctl03']",
    modification:         "select[name='ctl00$ContentPlaceHolder1$customFields1$customfield2612544$ctl03']",
    export_control:       "select[name='ctl00$ContentPlaceHolder1$customFields1$customfield2355376$ctl03']",
    checkbox : {
      federal:            "input[type='checkbox']#ctl00_ContentPlaceHolder1_customFields1_customfield2355352_cbxInput",
      foundation:         "input[type='checkbox']#ctl00_ContentPlaceHolder1_customFields1_customfield2355354_cbxInput",
      industry:           "input[type='checkbox']#ctl00_ContentPlaceHolder1_customFields1_customfield2355356_cbxInput",
      internal:           "input[type='checkbox']#ctl00_ContentPlaceHolder1_customFields1_customfield2355358_cbxInput",
      other:              "input[type='checkbox']#ctl00_ContentPlaceHolder1_customFields1_customfield2355360_cbxInput",
      state:              "input[type='checkbox']#ctl00_ContentPlaceHolder1_customFields1_customfield2355362_cbxInput",       
      none:               "input[type='checkbox']#ctl00_ContentPlaceHolder1_customFields1_customfield2355364_cbxInput"       
    }      
  },
  TangibleMaterials: {
    value:                '2354715',
    dropdown: {
      lic_officer:          "select[name='ctl00$ContentPlaceHolder1$customFields1$customfield2355639$ctl03']",
      material_type:        "select[name='ctl00$ContentPlaceHolder1$customFields1$customfield2412205$ctl03']",
      material_related_vi:  "select[name='ctl00$ContentPlaceHolder1$customFields1$customfield2408368$ctl03']",
      third_party:          "select[name='ctl00$ContentPlaceHolder1$customFields1$customfield2355669$ctl03']",
      mta_signed:           "select[name='ctl00$ContentPlaceHolder1$customFields1$customfield2355673$ctl03']",
      export_control:       "select[name='ctl00$ContentPlaceHolder1$customFields1$customfield2355677$ctl03']"      
    },
    checkbox : {
      federal:            "input[type='checkbox']#ctl00_ContentPlaceHolder1_customFields1_customfield2355653_cbxInput",
      foundation:         "input[type='checkbox']#ctl00_ContentPlaceHolder1_customFields1_customfield2355655_cbxInput",
      industry:           "input[type='checkbox']#ctl00_ContentPlaceHolder1_customFields1_customfield2355657_cbxInput",
      internal:           "input[type='checkbox']#ctl00_ContentPlaceHolder1_customFields1_customfield2355659_cbxInput",
      other:              "input[type='checkbox']#ctl00_ContentPlaceHolder1_customFields1_customfield2355661_cbxInput",
      state:              "input[type='checkbox']#ctl00_ContentPlaceHolder1_customFields1_customfield2355663_cbxInput",       
      none:               "input[type='checkbox']#ctl00_ContentPlaceHolder1_customFields1_customfield2355665_cbxInput"       
    },
    material_type: {
      animal: {
        value:            '2412280',
        name:             'Animal Model'
      },      
      monoclonal:{
        value:            '2412284',
        name:             'Monoclonal Antibody'
      },
      polyclonal:{
        value:            '2412286',
        name:             'Polyclonal Antibody'
      },
      protein:{
        value:            '2412282',
        name:             'Protein/Peptide'
      },
      other:{
        value:            '2421130',
        name:             'Other'
      }
    }
  }
};

var MiscDlg = {
  main_confirmation:    "#ctl00_ContentPlaceHolder1_cpcSubmit_pnlMainConfirm",
  AddInventor: {
    dlgID:              "#ctl00_ContentPlaceHolder1_customFields1_sectionInventors_mdcEdit_pnlMain",
    searching:          "#ctl00_ContentPlaceHolder1_customFields1_sectionInventors_mdcEdit_contentTemplate_searchContact1_txtSearch_btnSearch_divLoading",
    add_contact:        "ctl00_ContentPlaceHolder1_customFields1_sectionInventors_mdcEdit_contentTemplate_searchContact1_mdcAddContact_pnlMain"
  }
};

$(document).ready(function() {
  console.log("in $(document).ready");
    OnReady(); 
});

function hasClass(element, cls) {
    return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
}
window.onload = function () {
  var el = document.getElementsByTagName("BODY")[0];
  
  if ( (hasClass(el, 'Detailed_View') || hasClass(el, 'Dashboard')) && bMaintance ) {
    var ChildScope = angular.element(document.getElementById('maintance_ctrl')).scope();
    ChildScope.$ctrl.openComponentModal();
  }
    
    
}

function AddBackToTop() {
  var offset = 220;
  var duration = 1200;
  
  /* 
    Add the html structure for the back to top btn.
  */ 
  $('<div id="back-to-top" class="back-top-top">Back to top</div>').appendTo('form');
  
  // determine the window position and show or hide based on the current position and our offset 
  $(window).scroll(function() {
      if ($(this).scrollTop() > offset) {
          $('#back-to-top').fadeIn(duration);
      } else {
          $('#back-to-top').fadeOut(duration);
      }
  });
  
  // Hanle the onclick event to scroll all the way back up.
  $('#back-to-top').click(function(event) {
      event.preventDefault();
      $('html, body').animate({scrollTop: 0}, duration);
      return false;
  });  
}

function AdjustDetailViewTable() {
  console.log('        In AdjustDetailViewTable ');
  
  // Add the fa-user class to the anchor and remove the text
  // Since it's a very long text, "All Signatures Received"
  $('.Detailed_View th:nth-child(8) a')
    .addClass('fa fa-users')
    .text('');
  
  
  // add the class to the Reviewed Stage Col
  $('.Detailed_View td:nth-child(6)').each( function() {    
    $('span:contains("Draft")', $(this))
      .parent()
      .addClass('draft');
    $('span:contains("Submitted")', $(this))
      .parent()
      .addClass('submitted');
    $('span:contains("Approved")', $(this))
      .parent()
      .text("Accepted")
      .addClass('accepted');
  });
  
  // Add the class to the All Signature Received Col
  $('.Detailed_View td:nth-child(8)').each( function() {    
    $('span:contains("No")', $(this))
      .before('<i class="fa fa-user-plus warning" aria-hidden="false"></i>')
      .addClass('hide');
    $('span:contains("Yes")', $(this))  
      .before('<i class="fa fa-check accepted" aria-hidden="true"></i>')
      .addClass('hide');
  });  
 
    
  // Check to see if there's a footer for the table...
  // if there is, change the colspan by sub 1.  This is done becasue we hide a col.
  var e = $('#ctl00_ContentPlaceHolder1_rgMain_ctl00 tr.rgPager > td');
  if( $(e).length )
    $(e).attr('colspan', $(e).attr('colspan')-1);

  console.log('        Out AdjustDetailViewTable ');  
}

function BindEvents() {

  /////////////////////////////////////// 
  // Bind Events to the submit buttons //
  ///////////////////////////////////////

  // Save As Draft
  $('input[type="submit"][value="Save As Draft"]').each( function(){
    $(this).on("click", function() {
      btnClicked = $(this);
    }); 
  });

  // Submit for Review
  $('input[type="submit"][value="Submit for Review"]').each( function(){
    $(this).on("click", function() {
        btnClicked = $(this);
    }); 
  }); 
  
  // General Delete Btn for Disclosers
  $('a:contains("Delete")').each( function() {
    $(this).on("click", function() { 
      anchorClicked = $(this);
    }); 
  });
  
  // Error Message - Cannot Save on close
  $("#ctl00_ContentPlaceHolder1_cpcError_btnClose_btnSubmit").on("click", function(){
    setTimeout(OnReady, 500);
  });
  
  // No Button (Change Discloser Type)
/*
  $('#ctl00$ContentPlaceHolder1$cpcChangeType$btnNo').on('click', function(){
    btnClicked = $(this);   
  });

  // Cycle through all of the Funding Check boxes
  $('span.input_label > input[type=checkbox]', '#ctl00_ContentPlaceHolder1_customFields1_pnlTop > a:contains("Funding") + div').each(function() {
    var input = $(this);  // The funding source checkbox
    var divContainer = $(this).parent().parent().parent().next();  // Funding Table for the current checkbox

    // If the container contains an input with Add Row as the value then we can proceed...
    // We check becasue None does not have a funding table associated with it
    if ( $('input[type="submit"][value="Add Row"]', divContainer).length ) {      
      // Bind a on change event to the Funding Source checkbox
      $(input).on('change', ProcessFundingCheckBox);
    }
  });
  
  // Submit Disclosure Dlg Btns
  $('#ctl00_ContentPlaceHolder1_cpcSubmit_btnYes_btnSubmit').on('click', function(){
    setTimeout(OnClick_SubmitDisclosureDlg_Yes, 500);
  });
  
  // Bind the onchange event to the sbumitted status drop down.
  $('#ctl00_ContentPlaceHolder1_divSubmittedStatus select[name="ctl00$ContentPlaceHolder1$ddnStatus$ctl03"]').on('change', function(){
    setTimeout(PrepDisclosureForm, 700);
  });
*/    
  // Bind the click event to the Add Inventor btn
  $('input[type="submit"][value="Add Inventor"]').on('click', function(){
    setTimeout(OnClick_AddInventorBtn, 500);
  });
  
  // Bind to any Edit link for Inventors
  $('#ctl00_ContentPlaceHolder1_customFields1_sectionInventors_rgMain_ctl00 a:contains("Edit")').each(function(){
    $(this).on('click', function(){
      setTimeout($.proxy(OnClick_AddInventorBtn, $(this)),500);
    });
  });

  // Bind to any Delete links for Inventors
  $('#ctl00_ContentPlaceHolder1_customFields1_sectionInventors_rgMain_ctl00 a:contains("Delete")').each(function(){
    $(this).on('click', function(){
      setTimeout($.proxy(OnClick_InventorDelete, $(this)),500);
    });
  });

  // Bind to the Span for the btnAdd_divLoading changing from hidden to showing and back
  $('input[type="submit"][value="Add Row"] + span').each(function() {
    observer["Add Row"].observe(this, {attributes: true});
  });
  
  TypographicalEmphasis();
}

function BindEvents_AddRow_SaveDelete( parentElement, bSaved ) {
  
    // Rebind to the Add Row and any links that may contain Save or Delete
    $('input[type="submit"][value="Add Row"] + span', $(parentElement) ).each(function() {      
      observer['Add Row'].observe(this, {attributes: true}); 
    });
    
    // Now that we've added a row we need to add a event to the Save and Delete
    // so we can re apply typographical emphasis after they have been clicked 
    $('a:contains("Save")', $(parentElement)).each( function() {
      $(this).on("click", function() { 
        setTimeout($.proxy(OnClick_SaveDelete_Row, this, true), 500); 

      });         
    });
    $('a:contains("Delete")', $(parentElement)).each( function() {
      $(this).on("click", function() { 
        setTimeout($.proxy(OnClick_SaveDelete_Row, this, false), 500);         
      });                
    });
      
    TypographicalEmphasis( $(parentElement) );
    
    //  Hide / Remove the Changes mad, please save the draft message.
     
    $el = $('span.feedback_failure:contains("Changes made, please")', $(parentElement));
    
    if (typeof(bSaved)  === "undefined" || bSaved === null ) {
      $el.hide();
    }
    else {
      $el
        .html( (bSaved) ? "The row was saved... please continue." : "The row was deleted... please continue.")
        .delay(5000)
        .fadeOut('slow');
    }
    
    
    /*
    .each( function() {
      $(this).html((bSaved) ? "Rows was saved..." : "Row ws deleted")
      $(this).css({"-webkit-transition":"visibility 5s, opacity 5s linear", "transition":"visibility 5s, opacity 5s linear"});
      var element = $(this);
      setTimeout($.proxy( function() {
        var one = 1;
        $(this)
          .fadeOut
          .hide();
      }, this ), 5000);
  
      
    });
    */
}

function CheckStateOfLoading( e, bReverse ) {
  if ( bReverse ) {
    if( $("div#" + e ).is(":hidden") || $("div#" + e ).css("display") == 'none' ) {
      return  setTimeout(CheckStateOfLoading, 20, e, bReverse);
    }
    else {
      return MoveElementsOnDisclosureForms();
    }    
  }
  else {
    if( $("div#" + e ).is(":visible") || $("div#" + e ).css("display") != 'none') {
      return  setTimeout(CheckStateOfLoading, 20, e, bReverse);
    }
    else {
      return MoveElementsOnDisclosureForms();
    }
  }
}

function CheckTableState( ParentTable ) {
  alert("got in");
}

function Construct() {
  $('form #ctl00_header_div ~ *').wrapAll(' <div class="page-body"></div>');
  $('.page-body > table').width("95%");
  $('#ctl00_ContentPlaceHolder1_pnlLogin')
    .prepend( '    <div class="compat-desktop">' +  
              '      <h2 id="Browser_compatibility">Browser Compatibility Check</h2>' +  
              '      <p></p>' +  
              '      <noscript>' +  
              '        <div class="awesome-fancy-styling">' +  
              '          <p>This site requires JavaScript. Please enable JavaScript prior to continuing+</p>' +  
              '        </div>' +  
              '        <style>' +  
              '          table#login_credentials,' +  
              '          div+htab,' +  
              '          input[type=submit] {' +  
              '            display:none;' +  
              '            visibility: hidden;' +  
              '          }' +  
              '          ' +  
              '          #login_credentials_failed {' +  
              '            display: block;' +  
              '            visibility: visible;' +  
              '          }' +  
              '          ' +  
              '        </style>' +  
              '      </noscript>' +  
              '      <div class="htab"> ' +  
              '        <div id="compat-desktop" style="display: block;">' +  
              '          <table class="compat-table">' +  
              '            <tbody>' +  
              '              <tr>' +  
              '                <th>Feature</th>' +  
              '                <th>Chrome</th>' +  
              '                <th>Firefox <br />(Gecko)</th>' +  
              '                <th>Internet <br />Explorer</th>' +  
              '                <th>Opera</th>' +  
              '                <th>Safari</th>' +  
              '              </tr>' +  
              '              <tr class="version">' +  
              '                <td>Basic support:</td>' +  
              '                <td>37+0+</td>' +  
              '                <td>38+0+</td>' +  
              '                <td>9+0+</td>' +  
              '                <td>30+0+</td>' +  
              '                <td>6+0+</td>' +  
              '              </tr>' +  
              '              <tr class="version">' +  
              '                <td>You have:</td>' +  
              '                <td id="Chrome" data-value="37"></td>' +  
              '                <td id="Firefox" data-value="38"></td>' +  
              '                <td id="IE" data-value="9"></td>' +  
              '                <td id="Opera" data-value="30"></td>' +  
              '                <td id="Safari" data-value="6"></td>' +  
              '              </tr>              ' +  
              '            </tbody>' +  
              '          </table>' +  
              '        </div>' +  
              '      </div>' +  
              '    </div>');


  // Move oroginal login informatino up and recreate the secondary navbar
  $('#ctl00_header_div').toggle();
  $('header#navbar').after($('#ctl00_pnlLoggedIn'));
  $('#ctl00_pnlLoggedIn *').wrapAll('<div class="container"><div id="secondary-nav"><ul class="menu nav navbar-nav secondary"></ul></div></div>');
  $('#ctl00_lblTitleLoggedIn').replaceWith('<li class="first leaf"><p><span id="ctl00_lblTitleLoggedIn">Welcome back, </span></p></li>');
  $('#ctl00_lblTitleLoggedIn').after( $('#ctl00_lblLoggedIn') );
  $('#ctl00_lnkLogout').wrap('<li class="last leaf"></li>');
  // Remove all empty divs within the now create4d secondary-nav
  $('ul.navbar-nav.secondary div').remove();
  // Hide any other elements that is not a li within the ul
  $('#ctl00_lnkFeedback').remove();
  

  //  This is strickly cosmetics - 
  //  Wrapp the Daskbord_stats_container with a wrapper
  $('.dashboard_stats_container').wrap('<div class="dashboard_stats_container_wrapper"></div>');

}

function HideTangibleMaterialSections() {
  // Get each anchor for the fieldset heading / label   
  
  curSelection = "";
  switch( $(disclosure.TangibleMaterials.dropdown.material_type).val() ) {
    case disclosure.TangibleMaterials.material_type.animal.value: {
      curSelection = disclosure.TangibleMaterials.material_type.animal.name;
      break;
    }
    case disclosure.TangibleMaterials.material_type.monoclonal.value: {
      curSelection = disclosure.TangibleMaterials.material_type.monoclonal.name;
      break;
    }
    case disclosure.TangibleMaterials.material_type.polyclonal.value: {
      curSelection = disclosure.TangibleMaterials.material_type.polyclonal.name;
      break;
    }
    case disclosure.TangibleMaterials.material_type.protein.value: {
      curSelection = disclosure.TangibleMaterials.material_type.protein.name;
      break;
    }
    case disclosure.TangibleMaterials.material_type.other.value: {
      curSelection = disclosure.TangibleMaterials.material_type.other.name;
      break;
    }    
    default:
      break;;
  }
  $('a',"#ctl00_ContentPlaceHolder1_customFields1_pnlTop").each( function(){
    // Hide each fieldset
    
    ToggleTangibaleMaeterialSections( $(this).text(), $(this), (curSelection == $(this).text())  );
  });
  
}

function ToggleTangibaleMaeterialSections( fieldset, fieldsetLabel, bHide ) {
  if (typeof bHide === "undefined" || bHide === null)  bHide = true;

  switch( fieldset  )   {
    case disclosure.TangibleMaterials.material_type.polyclonal.value:               // Polyclonal Antibody - Coming from selection, locate the fieldset Label  
      fieldsetLabel = $('a:contains("Polyclonal Antibody")',"#ctl00_ContentPlaceHolder1_customFields1_pnlTop");
    case 'Polyclonal Antibody': { // Polyclonal Antibody
      $(fieldsetLabel)
        .toggle(bHide)  // Hide/Show the Fieldset Label
        .next()
        .toggle(bHide)  // Hide/Show the Fieldset Body
        .next()
        .toggle(bHide); // Hide/Show the spacer div after the body
      
      break;
    }
    case disclosure.TangibleMaterials.material_type.protein.value:               // Protein/Peptide - Coming from selection, locate the fieldset Label     
      fieldsetLabel = $('a:contains("Protein/Peptide")',"#ctl00_ContentPlaceHolder1_customFields1_pnlTop");
    case 'Protein/Peptide': {     // Protein/Peptide
      $(fieldsetLabel)
        .toggle(bHide)  // Hide/Show the Fieldset Label
        .next()
        .toggle(bHide)  // Hide/Show the Fieldset Body
        .next()
        .toggle(bHide); // Hide/Show the spacer div after the body
      break;
    }
    case disclosure.TangibleMaterials.material_type.monoclonal.value:               // Monoclonal Antibody - Coming from selection, locate the fieldset Label
      fieldsetLabel = $('a:contains("Monoclonal Antibody")',"#ctl00_ContentPlaceHolder1_customFields1_pnlTop");
    case 'Monoclonal Antibody': { // Monoclonal Antibody
      $(fieldsetLabel)
        .toggle(bHide)  // Hide/Show the Fieldset Label
        .next()
        .toggle(bHide)  // Hide/Show the Fieldset Body
        .next()
        .toggle(bHide); // Hide/Show the spacer div after the body
      break;
    }
    case disclosure.TangibleMaterials.material_type.animal.value:               // Animal Model - Coming from selection, locate the fieldset Label
      fieldsetLabel = $('a:contains("Animal Model")',"#ctl00_ContentPlaceHolder1_customFields1_pnlTop");
    case 'Animal Model': {        // Animal Model
      $(fieldsetLabel)
        .toggle(bHide)  // Hide/Show the Fieldset Label
        .next()
        .toggle(bHide)  // Hide/Show the Fieldset Body
        .next()
        .toggle(bHide); // Hide/Show the spacer div after the body
      break;
    }
    case disclosure.TangibleMaterials.material_type.other.value:               // Other - Coming from selection, locate the fieldset Label   
      fieldsetLabel = $('a:contains("Other")',"#ctl00_ContentPlaceHolder1_customFields1_pnlTop");
    case 'Other': {               // Other
      $(fieldsetLabel)
        .toggle(bHide)  // Hide/Show the Fieldset Label
        .next()
        .toggle(bHide)  // Hide/Show the Fieldset Body
        .next()
        .toggle(bHide); // Hide/Show the spacer div after the body
      break;
    }    

    case 'Contacts':              // Contacts
    case 'General Questions':     // General Questions
    case 'Funding':               // Funding
    case 'Compliance':            // 
    
    default: {
      break;
    }
  }
  
}

function MoveElementsOnDisclosureForms() {
  /**********************************************************************
  *  Move the the Draft, Submited and Aproved Notification and Disclosure 
  *  out of it's Normal spot and up on top of the disclosure.
  ***********************************************************************/
  // Create our wrapper for us to move stuff into.
  $("#ctl00_ContentPlaceHolder1_updatePanelDisclosure").prepend("<div id='ct100_ContentPlaceHolder_wrapper'></div>");

  // In the event of Admin state, let's show the Original Submited Date and Last Submited Date as well 
  if ( $("#ctl00_ContentPlaceHolder1_txtOrigSubmitDate").length ) {
    if ( $("#ctl00_ContentPlaceHolder1_txtLastSubmitDate").length ) 
      $("#ct100_ContentPlaceHolder_wrapper").prepend($("#ctl00_ContentPlaceHolder1_txtLastSubmitDate"));
    
    $("#ct100_ContentPlaceHolder_wrapper").prepend($("#ctl00_ContentPlaceHolder1_txtOrigSubmitDate"));
  }
  // Move the Disclosure Type out of the table and below the Draft/Submited/Approved notification Box
  $("#ct100_ContentPlaceHolder_wrapper").prepend($("#ctl00_ContentPlaceHolder1_ddnType"));

  // Move the Draft notification box out of the table and next to the download as pdf buttons
  if( $("#ctl00_ContentPlaceHolder1_pnlStatusDraft").length )
  $("#ct100_ContentPlaceHolder_wrapper").prepend($("#ctl00_ContentPlaceHolder1_pnlStatusDraft"));
  
  // Move the Submited notification box out of the table and next to the download as pdf buttons
  if( $("#ctl00_ContentPlaceHolder1_pnlStatusSubmitted").length )
  $("#ct100_ContentPlaceHolder_wrapper").prepend($("#ctl00_ContentPlaceHolder1_pnlStatusSubmitted"));
  
  // Move the Approved notification box out of the table and next to the download as pdf buttons
  if( $("#ctl00_ContentPlaceHolder1_pnlStatusApproved").length )
    $("#ct100_ContentPlaceHolder_wrapper").prepend($("#ctl00_ContentPlaceHolder1_pnlStatusApproved"));
  
  $("#ctl00_ContentPlaceHolder1_pnlStatusApproved div#ctl00_ContentPlaceHolder1_divApprovedSubtitle").text(function(){
    return $(this).text().replace("approved", "accepted");
  });
}

function OnClick_AddInventorAddContactCloseDlg () {
  // Wait until the Dlg is no longer visible, this means we are back to the form
  if( $('#ctl00_ContentPlaceHolder1_customFields1_sectionInventors_mdcEdit_contentTemplate_searchContact1_mdcAddContact_updatePanelMain').is(":visible") ) {
    return setTimeout(OnClick_AddInventorAddContactCloseDlg, 200); 
  } else {
    setTimeout(OnClick_AddInventorBtn, 200);
  }
  //BindEvents();
}

function OnClick_AddInventorBtn() {
  
  console.log('   OnClick_AddInventorBtn');
 
  /**********************************************************************
  * Rearrange the Add Inventor Dlg as well as change the wording.
  ***********************************************************************/
  $('#ctl00_ContentPlaceHolder1_customFields1_sectionInventors_mdcEdit_contentTemplate').children('div').each(function(){
    
    switch( this.id ) {
      case 'ctl00_ContentPlaceHolder1_customFields1_sectionInventors_mdcEdit_contentTemplate_rdoRole': {
        // Move the Inventor Disclosure Role and Right section below the Find Contact By Last Name or First Name section
        $(this)
          .insertAfter( $('#ctl00_ContentPlaceHolder1_customFields1_sectionInventors_mdcEdit_contentTemplate_searchContact1_mdcPhone') );

        // This is the Title of the  section
        $('.input_label:first-of-type', $(this))
          .html('Do you wish to allow this inventor to make changes to this disclosure?');
        
        // Change the wording of the Invetor's Disclosure Role and Rights
        $('#ctl00_ContentPlaceHolder1_customFields1_sectionInventors_mdcEdit_contentTemplate_rdoRole_ctl03 input + label', $(this))
          .each(function(){
            if( $(this).text().indexOf("Editor") >= 0 ) {
              $(this).html('Yes <span class="fieldtooltip">Give this inventor <strong>Full</strong> control over this disclosure.</span>');
            }
            else if( $(this).text().indexOf("View Only") >= 0 ) {
              $(this).html('No <span class="fieldtooltip"><strong>Do not</strong> give this inventor <strong>Full</strong> control over this disclosure.</span>');
            }
          }); 
          
        break;        
      }
      case 'ctl00_ContentPlaceHolder1_customFields1_sectionInventors_mdcEdit_contentTemplate_ddlRoleType': 
      case 'ctl00_ContentPlaceHolder1_customFields1_sectionInventors_mdcEdit_contentTemplate_ddlSignType': 
      case 'ctl00_ContentPlaceHolder1_customFields1_sectionInventors_mdcEdit_contentTemplate_dtSignDate': 
      case 'ctl00_ContentPlaceHolder1_customFields1_sectionInventors_mdcEdit_contentTemplate_ddlTemplate':
      case 'ctl00_ContentPlaceHolder1_customFields1_sectionInventors_mdcEdit_contentTemplate_txtSignificance': {
        $(this).hide();
        break;
      }
      
      case 'ctl00_ContentPlaceHolder1_customFields1_sectionInventors_mdcEdit_contentTemplate_txtContribution': {        
        break;
      }
      case 'ctl00_ContentPlaceHolder1_customFields1_sectionInventors_mdcEdit_contentTemplate_searchContact1_pnlSearch': {        
        // Change / rename all refreances to Contact and replace it with Inventor
          
        // This is the link
        $('#ctl00_ContentPlaceHolder1_customFields1_sectionInventors_mdcEdit_contentTemplate_searchContact1_lnkAddContact2', $(this) ).text(
          ($('#ctl00_ContentPlaceHolder1_customFields1_sectionInventors_mdcEdit_contentTemplate_searchContact1_lnkAddContact2', $(this) )
            .text()
            .replace(/\sContact/g, ' Inventor'))
        );
        // This is the text after the link
        $('#ctl00_ContentPlaceHolder1_customFields1_sectionInventors_mdcEdit_contentTemplate_searchContact1_cantFindText2', $(this) ).text(
          ($('#ctl00_ContentPlaceHolder1_customFields1_sectionInventors_mdcEdit_contentTemplate_searchContact1_cantFindText2', $(this) )
            .text()
            .replace(/\sContact/g, ' Inventor'))
        );
        
        break;
      }
    }
  });
 
  // Move the Feedback span if one is available
  $('#ctl00_ContentPlaceHolder1_customFields1_sectionInventors_mdcEdit_contentTemplate_rdoRole span.feedback_failure')
    .insertBefore( $('#ctl00_ContentPlaceHolder1_customFields1_sectionInventors_mdcEdit_contentTemplate') );
  
  TypographicalEmphasis('#ctl00_ContentPlaceHolder1_customFields1_sectionInventors_mdcEdit_contentTemplate_searchContact1_txtSearch_lblLabel', true);

  // This Observer watches for the Add Inventor to display so we can fix the dlg
  observerNode = document.querySelector ( MiscDlg.AddInventor.searching );
  observer['misc_dlg_extend'].observe(observerNode, {attributes: true});
  
  
  
}

function OnClick_AddInventorChooseBtn() {
    console.log("In OnClick_AddInventorChooseBtn");

  
  if( $('#ctl00_ContentPlaceHolder1_customFields1_sectionInventors_mdcEdit_contentTemplate_searchContact1_gvResults').length ) 
    return setTimeout(OnClick_AddInventorChooseBtn, 100);
  else 
    return OnClick_AddInventorBtn();
}

function OnClick_AddInventorAddNewContact() {
  // If the Add Contact (Add Inventor) Dlg is visibile then bind events to the Save Changes, Close and the Dlg [x] 
  if( $('#ctl00_ContentPlaceHolder1_customFields1_sectionInventors_mdcEdit_contentTemplate_searchContact1_mdcAddContact_pnlMain').is(':visible') ) {
    // Change / rename all refreances to Contact and replace it with Inventor
    var str = $('#ctl00_ContentPlaceHolder1_customFields1_sectionInventors_mdcEdit_contentTemplate_searchContact1_mdcAddContact_updatePanelMain')
      .html();
      
    $('#ctl00_ContentPlaceHolder1_customFields1_sectionInventors_mdcEdit_contentTemplate_searchContact1_mdcAddContact_updatePanelMain')
      .html( str.replace(/\sContact/g, ' Inventor') );    

    // Change / rename all refreances to Contact and replace it with Inventor
    var str = $('#ctl00_ContentPlaceHolder1_customFields1_sectionInventors_mdcEdit_contentTemplate_searchContact1_lnkAddContact2')
      .parent()
      .html();

    $('#ctl00_ContentPlaceHolder1_customFields1_sectionInventors_mdcEdit_contentTemplate_searchContact1_lnkAddContact2')
      .parent()
      .html( str.replace(/\sContact/g, ' Inventor') );      
    // Bind OnClick to Save Changes Btn.
    $('#ctl00_ContentPlaceHolder1_customFields1_sectionInventors_mdcEdit_contentTemplate_searchContact1_mdcAddContact_btnSaveChanges_btnSubmit')
      .on('click', function() {
        setTimeout(OnClick_AddInventorAddContactCloseDlg, 200);
      });   
    // Bind OnClick to the Cancel or Close Link 
    $('#ctl00_ContentPlaceHolder1_customFields1_sectionInventors_mdcEdit_contentTemplate_searchContact1_mdcAddContact_hypCancel')
      .on('click', function(){
        setTimeout(OnClick_AddInventorAddContactCloseDlg, 200);        
      });
      
    // Bind OnClick to the Dlg [x] close dlg button    
    $('#ctl00_ContentPlaceHolder1_customFields1_sectionInventors_mdcEdit_contentTemplate_searchContact1_mdcAddContact_updatePanelMain .mdc_ss_mdl_hdr a')
      .on('click', function(){
        setTimeout(OnClick_AddInventorAddContactCloseDlg, 200);  
      });
      
  }
  else {
    return setTimeout(OnClick_AddInventorAddNewContact, 100);
  }
    
}

function OnClick_AddInventorRemoveBtn() {
  if( $('#ctl00_ContentPlaceHolder1_customFields1_sectionInventors_mdcEdit_contentTemplate_searchContact1_pnlSelected').length ) 
    return setTimeout(OnClick_AddInventorRemoveBtn, 100);
  else 
    return OnClick_AddInventorBtn();
}

function OnClick_AddInventorSaveBtn() {
  // Wait until the Dlg is no longer visible, this means we are back to the form
  if( $('#ctl00_ContentPlaceHolder1_customFields1_sectionInventors_mdcEdit_pnlMain').is(":visible") )
    // If the dlg is still visible, and the error messages is visible / exists then stop the timmer
    // And rebind for this dlg.
    if( ($('#ctl00_ContentPlaceHolder1_customFields1_sectionInventors_mdcEdit_pnlMain .feedback_failure').length  ||  
         $('#ctl00_ContentPlaceHolder1_customFields1_sectionInventors_mdcEdit_contentTemplate_searchContact1_cicError').is(':visible')) &&
        !$('#ctl00_ContentPlaceHolder1_customFields1_sectionInventors_mdcEdit_btnSaveChanges_divLoading').is(':visible') ) {
      return OnClick_AddInventorBtn();
    }
    else {  // Else continue waiting...
      return setTimeout(OnClick_AddInventorSaveBtn, 200); 
    }
  else {  // ReBind the click event to the Add Inventor btn
    $('input[type="submit"][value="Add Inventor"]').on('click', function(){
      setTimeout(OnClick_AddInventorBtn, 500);
    });
  }
    BindEvents();
  }

function OnClick_AddInventorSearchBtn() {
  // Wait to for the "progress" bar not to be visibile any longer to continue with processing
  if( $('#ctl00_ContentPlaceHolder1_customFields1_sectionInventors_mdcEdit_contentTemplate_searchContact1_txtSearch_btnSearch_divLoading').is(":visible") )
    return setTimeout(OnClick_AddInventorSearchBtn, 200);
  else 
    OnClick_AddInventorBtn();
/*
  // Bind the click event to the Add Inventor's' Dlg Choose Btn
  $('#ctl00_ContentPlaceHolder1_customFields1_sectionInventors_mdcEdit_contentTemplate_searchContact1_gvResults input[type="submit"][value="Choose"]').on('click', function(){
    setTimeout(OnClick_AddInventorChooseBtn, 200);
  });  
  
  // Change / rename all refreances to Contact and replace it with Inventor
  var str = $('#ctl00_ContentPlaceHolder1_customFields1_sectionInventors_mdcEdit_contentTemplate_searchContact1_lnkAddContact2')
    .parent()
    .html();
  
  
  $('#ctl00_ContentPlaceHolder1_customFields1_sectionInventors_mdcEdit_contentTemplate_searchContact1_lnkAddContact2')
    .parent()
    .html( str.replace(/\sContact/g, ' Inventor') );
    
  // Bind the click event to the Add Inventor's' Dlg  Add a new Contact Link
  $('a#ctl00_ContentPlaceHolder1_customFields1_sectionInventors_mdcEdit_contentTemplate_searchContact1_lnkAddContact2').on('click', function() {
    setTimeout(OnClick_AddInventorAddNewContact, 1000);
  });
*/
}

function OnClick_AddRow( ) {
  
  //  Wait until the "loading div" (*_btnAdd_divLoading) is no longer visible
  //  When it's no longer visible then the row was successfully loaded
  if( $(this).is(':visible') ) {
    return  setTimeout($.proxy(OnClick_AddRow, this), 100);
  } 
  else {
    // Pointer to the parent element, this is useful since we need to re apply all of the typographical emphasis.
    var parentDiv = "#" + $(this).attr('id').replace("_btnAdd_divLoading", '');
    BindEvents_AddRow_SaveDelete( $(parentDiv) );
/*    
    $( el ).each(function() {      
      observer['Add Row'].observe(this, {attributes: true}); 
      TypographicalEmphasis(parentDiv);      
      
      // Now that we've added a row we need to add a event to the Save and Delete
      // so we can re apply typographical emphasis after they have been clicked 
      $('a:contains("Save")').each( function() {
        $(this).on("click", function() { 
          setTimeout($.proxy(OnClick_SaveDelete_Row, this), 500); 

        });         
      });
      $('a:contains("Delete")').each( function() {
        $(this).on("click", function() { 
          setTimeout($.proxy(OnClick_SaveDelete_Row, this), 500);         
        });                
      });    

    });
*/
  } 
}

function OnClick_SaveDelete_Row( bSaved ) {
  
  //  Wait until this instance is no longer visible
  if( $(this).is(':visible') ) {
    return  setTimeout($.proxy(OnClick_SaveDelete_Row, this, bSaved), 100);
  } 
  else {
    // We can assume that the row has been removed if it's no longer visible
    // AT this point we need to re "Emphasis"
    var el = "#" + this.id.replace(/_(lnk)\w+/g, '');
    BindEvents_AddRow_SaveDelete( $(el), bSaved );  
  }  
}

function OnClick_FundingAddRow_ChangeTextToTextArea(  ) {
  var fieldName = '#' + $(this).attr('id').replace('_btnAdd_btnSubmit', '');
  
  if( $( fieldName + '_btnAdd_divLoading' ).is(":visible")   ) {
    return setTimeout( $.proxy(OnClick_FundingAddRow_ChangeTextToTextArea, this), 250);
  } 

  $('tr:nth-of-type(1n + 1) > td:nth-of-type(2)', $(fieldName + '_tblRows')).each( function(){
    var inputOriginal = $('input[type="text"]', this);
    var id = $(inputOriginal).attr('id');
    var name = $(inputOriginal).attr('name');
    var val = $(inputOriginal).val();

    $(inputOriginal).replaceWith( $('<textarea style="width:100%" rows=2 name="' + name + '" id="'+ id +'">') );
    $('textarea#' + id).text(val);
  });
}

function OnClick_InventorDelete() {
  if( $(this).is(":visible") )
    return setTimeout($.proxy(OnClick_InventorDelete, $(this)),500);
  else 
    BindEvents();
}
 
function OnClick_PublicDisclosureDetailsDivLoading() {
  
  if( $(this).is(":visible") )
    return setTimeout($.proxy(OnClick_PublicDisclosureDetailsDivLoading, $(this)),500);
  else 
    TypographicalEmphasis();  
} 
 
function OnClick_SubmitDisclosureDlg_Yes() {
  
  setTimeout(RePrepDisclosureForm, 500, $('div#' + 'ctl00_ContentPlaceHolder1_cpcSubmit_btnYes_divLoading'));

}
 
function onLoadingDlg( el ) {

  // Wait for the Add Inventor Dlg to be visible
  if( $(el).is(":visible") )
    return setTimeout($.proxy(onLoadingDlg, $(this)),500);

  OnClick_AddInventorBtn();

}
 
function OnSelChange_DisclosureType() {
  // Wait for the Confirmation box to appear prior to continueing.
  if( $('#ctl00_ContentPlaceHolder1_cpcChangeType_pnlMainConfirm').is(':visible') ) {
    // Now that the Confirmation Dlg is up, bind events to the buttons
    $('#ctl00_ContentPlaceHolder1_cpcChangeType_pnlMainConfirm input[type="submit"][value="No"]')
      .on('click', function() {
        btnClicked = $(this);
        setTimeout(OnReady, 200);
      });
    
    $('#ctl00_ContentPlaceHolder1_cpcChangeType_pnlMainConfirm a#ctl00_ContentPlaceHolder1_cpcChangeType_lnkPopupCloseConfirm')
      .on('click', function() {
        anchorClicked = $(this);
        setTimeout(OnReady, 200);        
      });
    
  }
  else {
    // The Change Disclosure Type Confirmation Dlg is not yet visible  - Let's try again
    return setTimeout(OnSelChange_DisclosureType, 200);    
  }
  
  
}

function OnReady() {
  console.log("in OnReady");

  Construct();
  AddBackToTop();
    
  // Take the Disclosure Type value and set it as a class for the body
  $('body').attr('class', $('select[name="ctl00$ContentPlaceHolder1$ddnType$ctl03"] option:selected').text().replace(/\s/g, '_'));
    
  // if an admin is logged in then let's set the admin class to the body
  if ( $('#ctl00_pnlNavAdmin').length ) {
    $('body').addClass('admin');
  } 

  // Add the current tab value and set it as a class for the body 
  if( $('.nav_tab_on').length )
    $('body').addClass($('.nav_tab_on a').text().replace(/\s/g, '_'));  
  else
    $('body').addClass("Dashboard");
    
  // Determine if we are looking at a disclosure
  if ( $('body.Add_New_Disclosure').length ) {
    // Determine if the disclosure is read only (i.e. submitted or above)
    if( $('#ctl00_ContentPlaceHolder1_ddnType select[disabled]').length ) {
      $('body').addClass('readonly');
    }  

    // Move the title of  Disclosure to the top
    if( $("#ctl00_ContentPlaceHolder1_txtDisclosureTitle_txtInput").text() ) {
      $("#ctl00_ContentPlaceHolder1_title1_h1Title")
        .append(" - " + $("#ctl00_ContentPlaceHolder1_txtDisclosureTitle_txtInput").text() );
    }
  
    PrepDisclosureForm();   // Prop the Disclosure form: make require red, hide elements etc.
  }
 
  /****************
  * Detailed View *
  *****************/
  //  In the event that we are on the Detailed View page; 
  if( $('.Detailed_View').length ) {
    AdjustDetailViewTable();
  }

  /****************
  * Logout Styles *
  *****************/
  $('form[action="logout.aspx"] .login_content table tr:last-child td')
    .on('click', function() {
      // Change window.location (Address) to the url from this element inside it
      window.location = $(this).find('a').attr('href');
    })
    .css('cursor', 'pointer');
  
  /****************
  * Login Screen  *
  *****************/
  if ( $('form[action="./login.aspx"]').length ) {
    showCompatibility();
   // $('#IPM_DLG').trigger("click");
  }
}

function PrepDisclosureForm() { 
  console.log('    In PrepDisclosureForm ');
  var observerNode = null;
  var nthChild = 0;
  var selList;

  // Disclosure specific custimization
  switch( $("div#ctl00_ContentPlaceHolder1_ddnType select.input_dropdown").find("option:selected").val() ) {
    case disclosure.CreativeWork.value: {       // Creative Work Disclosure

      break;
    }
    default:
    case disclosure.Invention.value: {          // Invention Disclosure

      break;
    }    
    case disclosure.Software.value: {           // Software Disclosure

      break;
    }    
    case disclosure.TangibleMaterials.value: {  // Tangible Materials Disclosure   
      // Hide all of the Tangible Material Sections with the exception of the one
      // that is currently selected.      
      HideTangibleMaterialSections();
      break;
    }
  }


  /////////////////////////////////////// 
  //      Common to ALL disclosure     //
  ///////////////////////////////////////
  OnClick_AddInventorSearchBtn();

  // Add link to the CTTC Directory to CTTC Licensing Officer Contacted are
  if ( !$('#fieldtooltip_extended').length ) {
    $('<span id="fieldtooltip_extended"><a href="http://www.cttc.co/staff-directory#licensing" target="_blank" style="margin-left: 5px;">Staff Directory (Licensing Team)</a></span>')
      .appendTo( '#ctl00_ContentPlaceHolder1_customFields1_customfield2354766 .fieldtooltip' );
                 
    // Add text to the Funding Information - this is due to the 512 character limit
    $('.expand_region:contains("Funding") div.fieldtooltip')
      .append('<span id="fieldtooltip_extended"><p>To begin, click the check box for the first funding type below, click ADD ROW,' + 
              ' enter the funding source and award no(s), click SAVE and repeat for each row.  ' +
              'If no funding was used, click the checkbox by “none”.</p></span>');
              
    TypographicalEmphasis();
  }
  MoveElementsOnDisclosureForms();
  
  if ( $('#ctl00_ContentPlaceHolder1_customFields1_sectionInventors_mdcEdit_contentTemplate_searchContact1_mdcAddContact_contentTemplate_contact1_confirmContact1_h1Title').length ) {
    custimizeAddContactDlg();
  }
  
  console.log('    Out PrepDisclosureForm ');
}

function custimizeAddContactDlg() {
  el = $('#ctl00_ContentPlaceHolder1_customFields1_sectionInventors_mdcEdit_contentTemplate_searchContact1_mdcAddContact_contentTemplate_contact1_confirmContact1_lnkNo');
  
  if( !el.is(":visible") )
    return setTimeout(custimizeAddContactDlg, 200);
  else {
      $(el)
        .detach()
        .prependTo('#ctl00_ContentPlaceHolder1_customFields1_sectionInventors_mdcEdit_contentTemplate_searchContact1_mdcAddContact_pnlDefaultButtons tbody table tr ')
        .wrap('<td></td>')
        .text('Create New Contact')
        .parent().append('<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>');
  }
  
}

function ProcessFundingCheckBox() {
  var divContainer = $(this)
    .parent() // <span class="input_label">
    .parent() // <div> 
    .parent() // Outer <div>
    .next();  // Jump to the next div... that houses the table
  if( $(this).prop('checked')  ) {
    $(divContainer).show();    
  }
  else {
    $(divContainer).hide();
  }
}

function ProcessSelection( selList, selectionValue, defaultValue ) {

  if( selectionValue == '' || selectionValue == null || selectionValue == 'undefined' )
    selectionValue = defaultValue;
  
  switch( selectionValue ) {  
    ////////////////////////////////////////////
    // Publishing / Public Disclosure Section //
    ////////////////////////////////////////////
  
    /*
      Publishing / Public Disclosure - Inventor Publications  
    */
      case "2424955":   // Yes
      case "2391675": { // Yes
        $(selList)
          .parent()
          .parent()
          .next()
          .show();
        break;   
      }
      case "2424953":   // No 
      case "2391673": { // No      
        $(selList)
          .parent()
          .parent()
          .next()
          .hide();
        break;
      }
    
    ///////////////////////////////////////
    //        Compliance Section         //
    ///////////////////////////////////////
    
    /*
      Compliance -Third Party Material
    */
      case "2391641": { // Yes
        $(selList)
          .parent() 
          .parent() 
          .next()
          .show()   // This is the Third Party Meterial Details section
          .next()
          .show()   // This is the Third Party MTA Singned section
        break;
      }
      case "2391639": {  // No 
        $(selList)
          .parent()
          .parent()
          .next()
          .hide()   // This is the Third Party Meterial Details section
          .next()
          .hide()   // This is the Third Party MTA Singned section
          break;
      }
    
    ///////////////////////////////////////
    //     Tangible Material Section     //
    ///////////////////////////////////////
    
    /*
      Tangible Material - Tangible Material Type
    */
      case "hide_all_tangible": { // Ficticious id used to hide all
        HideTangibleMaterialSections();
        break;
      }
      case "2412280":  // Animal Model
      case "2412284":  // Monoclonal Antibody
      case "2412286":  // Polyclonal Antibody
      case "2412282":  // Protein/Peptide 
      case "2421130": { // Other 
        // Hide all of the Tangible Material Sections prio to displaying the correct section based on selection
        HideTangibleMaterialSections();
        ToggleTangibaleMaeterialSections(selectionValue, selList);
        break;
      }
    /*
      Tangible Material - General Question - Related to another 
    */      
      case "2408371": { // No
        $(selList)
          .parent()
          .parent()
          .next()
          .hide() // Identify the related disclosure
        break;
      }
      case "2425477": { // Yes
        $(selList)
          .parent()
          .parent()
          .next()
          .show() // Identify the related disclosure       
        break;
      }
      
  }  
  
      ///////////////////////////////////////
    //        Compliance Section         //
    ///////////////////////////////////////
    
    /*
      Compliance -Third Party Material
    */

  
}

function RePrepDisclosureForm(divLoading) {  
  if( $(divLoading).length === 0 || $(divLoading).is(":hidden") ) 
    return PrepDisclosureForm();
  else 
    return setTimeout(RePrepDisclosureForm, 200, divLoading);  
}

function TypographicalEmphasis( el, bOnParent ) {
  console.log('        In TypographicalEmphasis ');
  parentElement = document.body;
  
  if ( !(typeof el === "undefined" || el === null) ) { 
    parentElement = $(el);
  }
  
  var strSub="(Required) *";
  $('span:contains('+strSub+')', parentElement).each(function(){
    //if ( this.firstElementChild != null && this.firstElementChild.localName != 'span' ) {
      $(this).html(
        $(this).html()
          .replace(strSub, '<span class=required>'+strSub+'</span>')        
      );  
  });
  
  if( !(typeof bOnParent === "undefined" || bOnParent === null ) && bOnParent === true ) {
    $(parentElement).each(function(){
      $(this).html(
        $(this).html()
          .replace(/\[b](.*)\[\/b]/g, "<strong>$1</strong>")
        );
    });    
    $(parentElement).each(function(){
      $(this).html(
        $(this).html()
          .replace(/\[u](.*)\[\/u]/g, "<u>$1</u>")
        );
    });
    $(parentElement).each(function(){
      $(this).html(
        $(this).html()
          .replace(/\[i](.*)\[\/i]/g, "<em>$1</em>")
        );
    });    
  }
  else {    
    strSub="[b]";
    $('div.fieldtooltip:contains('+strSub+')', parentElement).each(function(){
      //console.log(this);
      $(this).html(
        $(this).html()
          .replace(/\[b](.*)\[\/b]/g, "<strong>$1</strong>")
        );
    });
    strSub="[u]";
    $('div.fieldtooltip:contains('+strSub+')', parentElement).each(function(){
      //console.log(this);
      $(this).html(
        $(this).html()
          .replace(/\[u](.*)\[\/u]/g, "<u>$1</u>")
        );
    });
    strSub="[i]";
    $('div.fieldtooltip:contains('+strSub+')', parentElement).each(function(){
      //console.log(this);
      $(this).html(
        $(this).html()
          .replace(/\[i](.*)\[\/i]/g, "<em>$1</em>")
        );
    });    
  }
  console.log('        Out TypographicalEmphasis ');
}

function get_browser(){
    var ua=navigator.userAgent,tem,M=ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || []; 
    if(/trident/i.test(M[1])){
        tem=/\brv[ :]+(\d+)/g.exec(ua) || []; 
        return {name:'IE',version:(tem[1]||'')};
        }   
    if(M[1]==='Chrome'){
        tem=ua.match(/\bOPR\/(\d+)/)
        if(tem!=null)   {return {name:'Opera', version:tem[1]};}
        }   
    M=M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
    if((tem=ua.match(/version\/(\d+)/i))!=null) {M.splice(1,1,tem[1]);}
    return {
      name: M[0],
      version: M[1]
    };
 }      
 
 function showCompatibility() {
    var browser = get_browser();        
    var el = document.getElementById(browser.name);       
    var minVer = el.getAttribute('data-value');
    
    if(  parseFloat(browser.version) >=  parseFloat(minVer) ) 
      el.innerHTML = "<i class='fa fa-check' style='color: green; font-size:2em;'></i>";
    else {
      el.innerHTML = "<i class='fa fa-times' style='color: green; font-size:2em;'></i>";

      var errMsg = document.getElementById("error_msg");
      errMsg.innerHTML = 'Your browser (' + browser.name  + ' version: ' + browser.version + ') is out of date. It may have known security flaws and may not display all features of this websites. Please update your browser and try again.';
      
      /* Hide the login */
      el = document.getElementById('login_credentials').className += " version_error";        
      /* Show the error message */
      el = document.getElementById('browser_support_error').className += " version_error";
    }   
 }

//]]>