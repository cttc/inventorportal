(function ($) {
    'use strict';

/*globals angular, window, jQuery*/

angular.module('cttcApp', ['ngAnimate', 'ngSanitize', 'ui.bootstrap']);
angular.module('cttcApp').controller('ModalController', function ($uibModal, $log, $document) {
  var $ctrl = this;
  $ctrl.items = [{
    "title" : "Scheduled Maintenance Alert",
    "dateRange" : "Thursday, November 30th starting at 20:00 CST.  Service should resume at approximately 20:30 CST",
    "icon" : "tools-icon"     
  }];

  $ctrl.animationsEnabled = true;

  $ctrl.openComponentModal = function () {
    var modalInstance = $uibModal.open({
      animation: $ctrl.animationsEnabled,
      component: 'modalComponent',
      resolve: {
        getTitle: function() { return $ctrl.items[0].title; },
        getDates: function() { return $ctrl.items[0].dateRange; },        
        getIcon: function() { return $ctrl.items[0].icon; }        
      }
    });

    modalInstance.result.then(function (selectedItem) {
      $ctrl.selected = selectedItem;
    }, function () {
      $log.info('modal-component dismissed at: ' + new Date());
    });
  };
});

// Please note that $uibModalInstance represents a modal window (instance) dependency.
// It is not the same as the $uibModal service used above.

angular.module('cttcApp').component('modalComponent', {
  templateUrl: 'maintenanceDlg.html',
  bindings: {
    resolve: '<',
    close: '&',
    dismiss: '&'
  },
  controller: function () {
    var $ctrl = this;
    

    $ctrl.$onInit = function () {
      $ctrl.title = $ctrl.resolve.getTitle;
      $ctrl.getDates = $ctrl.resolve.getDates;
      $ctrl.getIcon = $ctrl.resolve.getIcon;
    };

    $ctrl.ok = function () {
      $ctrl.dismiss({$value: 'closed'});
    };

    $ctrl.cancel = function () {
      $ctrl.dismiss({$value: 'cancel'});
    };
  }
});    
    
  

window.countOfSesquatches = function () {

  'use strict';

  var root = angular.element(document.getElementsByTagName('body'));

  var watchers = [];

  var f = function (element) {
      angular.forEach(['$scope', '$isolateScope'], function (scopeProperty) {
          if (element.data() && element.data().hasOwnProperty(scopeProperty)) {
              angular.forEach(element.data()[scopeProperty].$$watchers, function (watcher) {
                  watchers.push(watcher);
              });
          }
      });

      angular.forEach(element.children(), function (childElement) {
          f(angular.element(childElement));
      });
  };

  f(root);

  // Remove duplicate watchers
  var watchersWithoutDuplicates = [];
  angular.forEach(watchers, function(item) {
      if(watchersWithoutDuplicates.indexOf(item) < 0) {
          watchersWithoutDuplicates.push(item);
      }
  });

  console.log(watchersWithoutDuplicates.length);
};

})(window.jQuery);