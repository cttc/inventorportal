<script type="text/ng-template" id="InventorPortalMaintenance.html">
 <div class="modal-header">
    <h3 class="modal-title">Inventor Portal Maintenance</h3>
  </div>
  <div style="display:inline-flex">    
    <div class="col-sm-3 col-md-2 link-card" style="margin-top: 20px; flex: 1; text-align: center;">
        <div class="link-card-icon ng-scope tools-icon"></div>     
    </div>
    <div class="col-sm-9 col-md-10">
          <p>&nbsp;</p>
          <p></p>
          <p>The Inventor Portal will be down for <strong> maintenance on Monday, September 11th starting at 07:00 CST.  Service should resume at 10:00 CST </strong>the same day.  During this time, there will be no access to the Inventor Portal Service. If you need to submit an invention disclosure during this outage, please <a href="message-us">contact us</a> to request a paper form.</p>
          <p> We apologize for the inconvenience.</p>
          <p>&nbsp;</p>
    </div>
  </div>
  <div class="modal-footer text-center">
  <button class="btn btn-warning" ng-click="cancel()">Close</button>
  </div>      
</form>     
</script>