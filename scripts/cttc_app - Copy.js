/*globals angular, window, jQuery*/

(function ($) {
    'use strict';

    var cttcApp = angular.module('cttcApp', [
        'ngAnimate',
        'ngSanitize',
        'ui.bootstrap'
    ]);
    // jQuery mods



    cttcApp.config(function () {
        console.log('In app config...');
    });

    cttcApp.run(function () {
        console.log('In app run...');

    });
    
/*   
    cttcApp.directive('maintenanceModal', [function () {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                Title: '@',
                DateRange: '@'
            },
            controller: function ($scope, $modal, $log) {
              
                var messageModal; 

                $scope.openMessageDialog = function () {
                   
                    messageModal = $modal.open({                   
                        templateUrl: '/templates/maintenanceModal.html',
                        controller: function ($scope, $modalInstance) {

                          $log.debug('in maintenanceModal controller');
                          
                          $scope.submit = function () {
                            $modalInstance.close($scope.formData);
                          };
                          
                          $scope.cancel = function () {
                              $modalInstance.dismiss('cancel');
                          };
                        },
                        size: 'lg' 
                      
                    });              
                  
                    messageModal.result.then(function (formData) {
                      
                    }, function () {
                      $log.info('Message Modal dismissed at: ' + new Date());
                    });
                
                };
              
            },
          template: '<div class="maintenance-modal"><ng-transclude></ng-transclude></div>',
          transclude: true            
        };
    }]);
*/
  
    cttcApp.directive('messageModal', ["messageService", function () {
      return {
        restrict: 'E',
        replace: true,
        template: '<div class="message-modal"><ng-transclude></ng-transclude></div>',
        transclude: true,
        scope: {
          Title:      '@',
          DateRange:  '@'
        },
        controller: function ($scope, $modal, $log) {
          // $scope is the appropriate scope for the directive
/*        
          
          var messageModal;
          
          $scope.openMaintenanceDialog = function () {
            messageModal = $modal.open({
              tempateURL: '/templates/maintenanceModal.html',
              controller: function ($scope, $modalInstance) {
                  $log.debug('in openVanderbiltDialog controller');

                  $scope.incoming = function () {
                      $modalInstance.close('incoming');
                  };

                  $scope.outgoing = function () {
                      $modalInstance.close('outgoing');
                  };

                  $scope.cancel = function () {
                      $modalInstance.dismiss('cancel');
                  };
              },
              size: 'lg'
            });
          }
*/          
          console.log("in controler");
        }              

      };
    }]);
  
})(window.jQuery);

window.countOfSesquatches = function () {

    'use strict';

    var root = angular.element(document.getElementsByTagName('body'));

    var watchers = [];

    var f = function (element) {
        angular.forEach(['$scope', '$isolateScope'], function (scopeProperty) {
            if (element.data() && element.data().hasOwnProperty(scopeProperty)) {
                angular.forEach(element.data()[scopeProperty].$$watchers, function (watcher) {
                    watchers.push(watcher);
                });
            }
        });

        angular.forEach(element.children(), function (childElement) {
            f(angular.element(childElement));
        });
    };

    f(root);

    // Remove duplicate watchers
    var watchersWithoutDuplicates = [];
    angular.forEach(watchers, function(item) {
        if(watchersWithoutDuplicates.indexOf(item) < 0) {
            watchersWithoutDuplicates.push(item);
        }
    });

    console.log(watchersWithoutDuplicates.length);
};