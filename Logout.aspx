﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Logout.aspx.cs" Inherits="Inteum.Server.Web.InventorPortal.Logout" MasterPageFile="~/InventorPortal/InventorPortal.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

        <asp:Panel ID="pnlAwaitingApproval" runat="server" Visible="true">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td valign="top" align="center">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="left">
                                    <div class="login_prompt"><wtc:ConfirmInfoControl ID="cicInfo" runat="server" /></div>
                                    <div class="login">
                                       <div class="login_content">
                                            <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                                <tr>
                                                    <td>
                                                        <div id="divTitle" runat="server" class="large_subtitle" style="text-align:center;">Inventor Portal Logout</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div style="height: 30px;" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <center id="logoffMsg" runat="server">You have been logged out of Inventor Portal.</center>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <center><span id="clickMsg" runat="server">Click</span> <a href="Default.aspx" id="clickLink" runat="server">here</a> <span id="toLogOffMsg" runat="server">to log back in.</span></center>
                                                    </td>
                                                </tr>
                                            </table>
                                       </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </asp:Panel>

</asp:Content>
