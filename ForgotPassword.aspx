﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InventorPortal/InventorPortal.Master" AutoEventWireup="true" CodeBehind="ForgotPassword.aspx.cs" Inherits="Inteum.Server.Web.InventorPortal.ForgotPassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<asp:Panel ID="pnlForgot" runat="server">
    <tb:TitleBar ID="title1" runat="server" Text="Forgot Password?" />
    <div class="content_indent">
    <div id="divSubtitle2" runat="server" class="subtitle">Enter your user name or email address below 
        and a temporary password will be sent to the email address associated with your 
        user account. Please follow the instructions in the email to reset your password.</div>
    <wtf:FormTextBoxControl ID="txtUser" runat="server" LabelText="Enter your user name or email:" Width="300" />
    <telerik:RadCaptcha ID="RadCaptchaControl" runat="server" ErrorMessage="The code you entered is not valid." EnableRefreshImage="true">
       </telerik:RadCaptcha>

  <div style="height:10px;"></div>
    <wtf:FormButtonControl ID="btnForgot" runat="server" Text="Recover" LoadingText="Loading..." OnClick="btnForgot_Click" IsWrapTable="true"/>
    </div>
</asp:Panel>

<asp:Panel ID="pnlReset" runat="server">
    <tb:TitleBar ID="title2" runat="server" Text="Forgot Password Recovery" />
    <div class="content_indent">
    <div id="divSubtitle" runat="server" class="subtitle">Reset your password by first entering your temporary password you received from the forgot password 
    email, then entering a new password and confirming that password.</div>
    <wtf:FormTextBoxControl ID="txtUserName" runat="server"  LabelText="User Name:" />
    <wtf:FormTextBoxControl ID="txtTempPassword" runat="server" LabelText="Temporary Password: (Received in the forgot password email)" />
    <wtf:FormTextBoxControl ID="txtNewPassword" runat="server" TextMode="Password" autocomplete="off" LabelText="New Password:" />
    <wtf:FormTextBoxControl ID="txtConfirmPassword" runat="server" TextMode="Password" autocomplete="off" LabelText="Confirm Password:" />
    <wtf:FormButtonControl ID="btnReset" runat="server" Text="Recover" LoadingText="Saving..." OnClick="btnReset_Click" IsWrapTable="true" />
    </div>
</asp:Panel>

<wtc:ConfirmPromptControl ID="cpcSuccess" runat="server" PromptType="Information" />
<wtc:ConfirmPromptControl ID="cpcError" runat="server" PromptType="Error" />

</asp:Content>
