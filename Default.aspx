﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InventorPortal/InventorPortal.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Inteum.Server.Web.InventorPortal.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<tb:TitleBar ID="title1" runat="server" Text="Dashboard" />

<div class="content_indent">
<div id="divSubtitle" runat="server" class="subtitle">Welcome to Inventor Portal</div>

<div id="divRecentActivity" runat="server" class="subtitlebar">Recent Activity</div>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td valign="top">
            <telerik:RadGrid 
                ID="gvActivity" 
                runat="server" 
                Width="100%"
                AllowPaging="true" 
                AllowCustomPaging="true"
                AllowSorting="false"
                ShowFooter="true" 
                ShowHeader="false" 
                GridLines="None"
                AutoGenerateColumns="false" 
                OnNeedDataSource="RadGrid_NeedDataSource">
                <MasterTableView 
                    OverrideDataSourceControlSorting="true" 
                    NoMasterRecordsText="<div style='padding:10px;'>No recent activity to display.</div>">
                    <Columns>
                       <telerik:GridTemplateColumn>
                           <ItemTemplate>
                                <div style="padding:10px;">
                                    <div class="small_desc"><asp:Label ID="Label1" runat="server" Text='<%# FormatDateTime(Eval("RCreatedD")) %>'></asp:Label></div>
                                    <div style="height:5px;"></div>
                                    <div><asp:Label ID="Label2" runat="server" Text='<%# Eval("Entry") %>'></asp:Label></div>
                                </div>
                            </ItemTemplate>

                       </telerik:GridTemplateColumn> 
                    </Columns>
               </MasterTableView>
            </telerik:RadGrid>
        </td>
        <td style="width:20px;"></td>
        <td valign="top" class="dashboard_stats">
            <div class="dashboard_stats_container">
                <div id="divDraft" runat="server" class="dashboard_stats_item">Draft</div>
                <div id="divDraftDisclosures" runat="server" class="dashboard_stats_item">Disclosures</div>
                <div class="dashboard_stats_item"><asp:HyperLink ID="hypDraftDisclosures" runat="server" NavigateUrl="mydisclosures.aspx?status=E" CssClass="nav_dashboard"></asp:HyperLink></div>
            </div>

            <div class="dashboard_stats_container">
                <div id="divReady" runat="server" class="dashboard_stats_item">Disclosures Ready</div>
                <div id="divForApproval" runat="server" class="dashboard_stats_item">For Approval</div>
                <div class="dashboard_stats_item"><asp:HyperLink ID="hypApprovalDisclosures" runat="server" NavigateUrl="mydisclosures.aspx?status=S" CssClass="nav_dashboard"></asp:HyperLink></div>
            </div>

            <asp:Panel ID="pnlUsersWaiting" runat="server" Visible="false">
                <div class="dashboard_stats_container">
                    <div id="divUserAccounts" runat="server" class="dashboard_stats_item">User Accounts</div>
                    <div id="divAwaitingApproval" runat="server" class="dashboard_stats_item">Awaiting Approval</div>
                    <div class="dashboard_stats_item"><asp:HyperLink ID="hypUserAccounts" runat="server" NavigateUrl="admin/users.aspx?status=awaitingApproval" CssClass="nav_dashboard"></asp:HyperLink></div>
                </div>
            </asp:Panel>
        </td>
    </tr>
</table>
    <div class="subtitle">
        <asp:Button runat="server" ID="btnClear" Text="Delete All Recent Activities" OnClick="btnClear_Click"/>
        <asp:ConfirmButtonExtender ID="cbeDelete" runat="server" 
                        TargetControlID="btnClear" 
                        ConfirmText="Are you sure you want to delete all the activities?">
                    </asp:ConfirmButtonExtender>
    </div>
</div>
</asp:Content>
