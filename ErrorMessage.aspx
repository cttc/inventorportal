﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ErrorMessage.aspx.cs" Inherits="Inteum.Server.Web.InventorPortal.ErrorMessage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="css/style.css" rel="stylesheet" type="text/css" id="css1" runat="server" />
</head>
<body>   
      <form id="form2" runat="server">
        <asp:Panel ID="Panel1" runat="server" Visible="true">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td valign="top" align="center">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="left">
                                    <div class="login">
                                       <div class="login_content">
                                            <table border="0" cellpadding="3" cellspacing="0" width="100%">                                            
                                                <tr>
                                                    <td>
                                                        <div id="divErrorTitle" runat="server" class="large_subtitle" style="text-align:center;">An Error Has Occurred </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div style="height: 30px;" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <center><asp:Label ID="lblErrorMessage" runat="server" Text="An error has occurred on the website." Width="500px" Style="word-wrap: normal; word-break: break-all;"/></center>
                                                    </td>
                                                </tr>
                                                 <tr>
                                                    <td>
                                                        <div style="height: 30px;" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <center><asp:Label ID="lblLink1" runat="server" Text="Click the back button on the browser to go back to previous page," /> </center> 
                                                        <center><asp:Label ID="lblLink2" runat="server" Text="or click " /> <a href="Default.aspx"><span id="lnkHere" runat="server">here</span></a> <asp:Label ID="lblLink3" runat="server" Text=" to go back to home page." /></center>
                                                    </td>
                                                </tr>
                                            </table>
                                       </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </form>
</body>
</html>



