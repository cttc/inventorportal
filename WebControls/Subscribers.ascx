﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Subscribers.ascx.cs" Inherits="Inteum.Server.Web.InventorPortal.WebControls.Subscribers" %>

<asp:HiddenField ID="hidInvDiscFk" runat="server" />
<asp:HiddenField ID="hidTechnolFk" runat="server" />

<asp:UpdatePanel ID="updatePanel1" runat="server" UpdateMode="Conditional">
<ContentTemplate>
<%--<h2 id="h2Title" runat="server">Subscribers</h2>--%>
<telerik:RadGrid runat="server" ID="rgMain" 
    Width="100%" 
    AllowPaging="true"
    AllowCustomPaging="true" 
    AllowSorting="true"
    PageSize="5"
    AutoGenerateColumns="false"
    OnItemDataBound="rgMain_ItemDataBound"
    AllowFilteringByColumn="false"
    EnableLinqExpressions ="false"
    OnNeedDataSource="RadGrid_NeedDataSource">
    <ClientSettings>
        <Resizing AllowColumnResize="true" />
    </ClientSettings>
    <MasterTableView 
        OverrideDataSourceControlSorting="true" 
        NoMasterRecordsText="<div style='padding:10px;'>No subscribers to display.</div>">
        <Columns>
            <telerik:GridTemplateColumn HeaderText="First" ColumnEditorID="colFirst" SortExpression="Name">
                <ItemTemplate>
                    <asp:Label ID="lblFirstName" runat="server" Text='<%# FormatFirstName(Eval("Name"), Eval("User")) %>'></asp:Label>
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn HeaderText="Last" ColumnEditorID="colLast" SortExpression="LastName">
                <ItemTemplate>
                    <asp:Label ID="lblLastName" runat="server" Text='<%# FormatLastName(Eval("LastName"), Eval("User")) %>'></asp:Label>
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn HeaderText="Email" ColumnEditorID="colEmail" SortExpression="EmailAddress">
                <ItemTemplate>
                    <asp:Label ID="lblEmail" runat="server" Text='<%# FormatEmail(Eval("EmailAddress"), Eval("User")) %>'></asp:Label>
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn HeaderText="Type" ColumnEditorID="colType">
                <ItemTemplate>
                    <asp:Label ID="lblType" runat="server" Text='<%# FormatSubscriberType(Eval("User")) %>'></asp:Label>
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn HeaderText="Manage" UniqueName="Manage" ColumnEditorID="colManage">
                <ItemTemplate>
                    <asp:HiddenField ID="hidPk" runat="server" Value='<%# Eval("PrimaryKey") %>' />
                    <asp:HiddenField ID="hidUserFk" runat="server" Value='<%# Eval("UsersFk") %>' />
                    <asp:HiddenField ID="hidEmailAddress" runat="server" Value='<%# Eval("EmailAddress.Address") %>' />
                    <asp:HiddenField ID="hidInvDiscRCreatedU" runat="server" Value='<%# Eval("InventionDisclosure.RCreatedU") %>' />
                    <asp:LinkButton ID="lnkRemove" runat="server" Text="<%# Remove_String() %>" OnClick="lnkRemove_Click"></asp:LinkButton>
                </ItemTemplate>
            </telerik:GridTemplateColumn>
        </Columns>
    </MasterTableView>
</telerik:RadGrid>

<asp:Panel ID="pnlAdd" runat="server" CssClass="content_indent">
    <div style="height:10px;"></div>
    <div style="font-weight:bold;" id="divAddSubTitle" runat="server">Add a Subscriber</div>
    <div style="height:5px;"></div>
    <div id="divAddSubDescription" runat="server">Subscribers do not need to be users of Inventor Portal. Add individuals that you would like to be notified by email of 
    events and actions that take place for this disclosure.</div>
    <div style="height:5px;"></div>
    <table border="0" cellpadding="3" cellspacing="0">
        <tr>
            <td><wtf:FormTextBoxControl ID="txtFirstName" runat="server" LabelText="First Name:" /></td>
            <td><wtf:FormTextBoxControl ID="txtLastName" runat="server" LabelText="Last Name:" /></td>
            <td><wtf:FormTextBoxControl ID="txtEmail" runat="server" LabelText="Email Address:" Width="250" /></td>
        </tr>
    </table>
    <wtf:FormButtonControl ID="btnAdd" runat="server" Text="Add Subscriber" OnClick="btnAdd_Click" LoadingText="Saving..." IsWrapTable="true" />
</asp:Panel>
</ContentTemplate>
</asp:UpdatePanel>

<wtd:ModalDialogControl ID="mdcAdd" runat="server" Title="Match Subscriber to Existing Contact" IsDefaultButtons="true" ButtonText="None of these Match" OnClick="mdcAdd_Click" Width="600">
    <DialogContent>
        <div id="divMatch" runat="server">The following match(s) were found for the information you entered. Click the Choose button of the contact you 
        want to add or click the None of these Match button to add this subscriber as a new Email Address.</div>
        <div style="height:10px;"></div>
        <asp:Panel ID="pnlResults" runat="server" ScrollBars="Vertical" Height="250">
            <asp:GridView ID="gvResults" runat="server" Width="100%" GridLines="None" 
                AllowPaging="false" AllowSorting="false" 
                ShowFooter="false" ShowHeader="false"
                AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <div style="border:solid 1px #c0c0c0; background-color:#efefef; padding:5px;">
                                <asp:HiddenField ID="hidPk" runat="server" Value='<%# Eval("PrimaryKey") %>' />
                                <div><asp:Label ID="lblName" runat="server" Text='<%# FormatSearchName(Container.DataItem) %>'></asp:Label></div>
                                <div style="height:5px;"></div>
                                <div><asp:Label ID="lblEmail" runat="server" Text='<%# FormatSearchEmail(Container.DataItem) %>'></asp:Label></div>
                                <div style="height:5px;"></div>
                                <asp:Button ID="btnChoose" runat="server" Text="<%# Choose_String() %>" OnClick="btnChoose_Click" />
                            </div>
                            <div style="height:10px;"></div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:Panel>
    </DialogContent>
</wtd:ModalDialogControl>

<wtd:ModalDialogControl ID="mdcRemove" runat="server" Title="Confirm Removing Yourself" IsDefaultButtons="true" ButtonText="Yes, Remove Myself" OnClick="mdcRemove_Click">
    <DialogContent>
        <asp:HiddenField ID="hidRemovePk" runat="server" />
        <div id="divRemove" runat="server">You have requested to remove yourself from this disclosure. By doing so, you will no longer be able to access this disclosure. 
        Are you sure you want to continue?</div>
    </DialogContent>
</wtd:ModalDialogControl>