﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InventorPortal/InventorPortal.Master" AutoEventWireup="true" CodeBehind="Migrate.aspx.cs" Inherits="Inteum.Server.Web.InventorPortal.admin.Migrate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <tb:TitleBar ID="title1" runat="server" Text="Data Conversion and Migration" />

<div class="content_indent">
<div class="subtitle">
    This page provides a way to convert the existing disclosures on Inteum Web to use the new disclosure templates on Inventor Portal.
</div>
<div class="prompt_warning">
<asp:Label ID="lblWarning" runat="server">
    NOTE: Please make sure no Inteum user logged in and all Inteum Web instances are closed, also all the views have been rebuilt with 'Materialize' check box checked in SetupWS. 
</asp:Label>
</div>
<div style="height:10px;"></div>

<asp:Panel ID="pnlSubdomain" runat="server">
    <wtf:FormTextBoxControl ID="txtInvType" runat="server" LabelText="Disclosure Type Name: (A default disclosure type will be generated to associate all disclosures with.)" Width="250" Text="Default" />
    <wtf:FormDropDownControl ID="ddnManager" runat="server" LabelText="Default Manager of new Disclosures: *" />
    <wtf:FormDropDownControl ID="ddnInventionId" runat="server" LabelText="Disclosure Invention ID Auto-Fill: *" />
    <wtf:FormDropDownControl ID="ddnTechId" runat="server" LabelText="Technology Tech ID Auto-Fill: *" />
    <div class="fieldlabel"><asp:Button runat="server" ID="btnMigrate" Text="Migrate Data" OnClick="btnMigrate_Click" Width="200" /></div>
</asp:Panel>
 
<asp:UpdatePanel ID="updatePanelProgress" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:Panel ID="pnlShowProgress" runat="server" Visible="false">
            <asp:Panel runat="server" ID="pnlProgressWrapper" CssClass="ProgressWrapper">
                <asp:Panel runat="server" ID="pnlProgress" CssClass="Progress" Visible="false" />
            </asp:Panel>
            <div class="info"><asp:Label ID="lblMessage" runat="server"></asp:Label></div>
        </asp:Panel> 
  
  <div style="height:10px;"></div>
<asp:Panel ID="pnlRefresh" runat="server">
    <div><asp:LinkButton runat="server" ID="lnkLogFile" Text="Migration Log File" OnClick="lnkLogFile_Click" Visible="false"/> </div>

    <wtf:FormButtonControl ID="btnRefresh" runat="server" Text="Refresh Page"  OnClick="btnRefresh_Click" LoadingText="Refeshing..." Visible="false"/>
</asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>

<wtd:ModalDialogControl ID="mdcLoggedIn" runat="server" Title="Log Off Users" IsDefaultButtons="true" RepositionMode="RepositionOnWindowResizeAndScroll" ModalTop="80" Width="600" ButtonText="Log Off All Users" OnClick="mdcLoggedIn_Click">
    <DialogContent>
 <asp:GridView ID="gvLoggedInUsers" runat="server" 
                            AllowSorting="true" 
                            AutoGenerateColumns="false" 
                            ShowHeader="true" 
                            ShowFooter="false"

                            Width="100%" CellPadding="3"
                            AllowPaging="false">
                            <RowStyle BackColor="#ffffff" />
                            <AlternatingRowStyle BackColor="#efefef" />
                            <Columns>
                              <asp:BoundField  HeaderText = "Name"   DataField = "NAME" />
                              <asp:TemplateField  HeaderText = "Work Station"  >
                               <ItemTemplate>
                                <asp:Label ID="lblWorkStation" runat="server" Text='<%# FormatWorkStation(Eval("WorkstName")) %>'></asp:Label>
                               </ItemTemplate>
                              </asp:TemplateField>

                               <asp:TemplateField  HeaderText = "Last Access" >
                               <ItemTemplate>
                                <asp:Label ID="lblLastAccess" runat="server" Text='<%# FormatLastAccess(Eval("LastAccess")) %>'></asp:Label>
                               </ItemTemplate>
                              </asp:TemplateField>
                             
                            </Columns>
                        </asp:GridView>
    </DialogContent>
</wtd:ModalDialogControl> 

<wtc:ConfirmPromptControl ID="cpcInfo" runat="server" PromptType="Information" />

    </div>
</asp:Content>
