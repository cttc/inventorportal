﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InventorPortal/InventorPortal.Master" AutoEventWireup="true" CodeBehind="Notifications.aspx.cs" Inherits="Inteum.Server.Web.InventorPortal.admin.Notifications" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <tb:TitleBar ID="title1" runat="server" Text="Email Notifications" />

<div class="content_indent">
<asp:UpdatePanel ID="updatePanelMain" runat="server">
    <ContentTemplate>
        <table border="0" cellpadding="3" cellspacing="0" width="100%">
            <tr>
                <td colspan="2">
                    <div class="subtitlebar">Inventors</div>
                </td>
            </tr>  
            <tr>
                <td>
                    <asp:CheckBox ID="cbxInventorRequestNewAccount" runat="server" Text="Send email to requesting Inventor after a new portal account is requested and waiting for approval" />
                </td>
                <td>
                    <asp:LinkButton ID="lnkInventorRequestNewAccount" runat="server" Text="Edit Email" OnClick="lnkEditEmail_Click"></asp:LinkButton>
                </td>
            </tr>
            <tr>
                
                <td>
                    <asp:CheckBox ID="cbxInventorAccountApproved" runat="server" Text="Send email to new Inventor when portal account is approved" />
                </td>
                <td>
                    <asp:LinkButton ID="lnkInventorAccountApproved" runat="server" Text="Edit Email" OnClick="lnkEditEmail_Click"></asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:CheckBox ID="cbxInventorAccountDenied" runat="server" Text="Send email to applying Inventor when portal account is denied" />
                </td>
                <td>
                    <asp:LinkButton ID="lnkInventorAccountDenied" runat="server" Text="Edit Email" OnClick="lnkEditEmail_Click"></asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:CheckBox ID="cbxInventorAddedToDisclosure" runat="server" Text="Send email to Inventor when they are added to a disclosure" />
                </td>
                <td>
                    <asp:LinkButton ID="lnkInventorAddedToDisclosure" runat="server" Text="Edit Email" OnClick="lnkEditEmail_Click"></asp:LinkButton>
                </td>
            </tr>
          
            <tr>
                <td>
                    <asp:CheckBox ID="cbxInventorDisclosureSubmitted" runat="server" Text="Send email to Subscribers when a disclosure is submitted" />
                </td>
                <td>
                    <asp:LinkButton ID="lnkInventorDisclosureSubmitted" runat="server" Text="Edit Email" OnClick="lnkEditEmail_Click"></asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:CheckBox ID="cbxInventorDisclosureApproved" runat="server" Text="Send email to Subscribers when a disclosure is approved" />
                </td>
                <td>
                    <asp:LinkButton ID="lnkInventorDisclosureApproved" runat="server" Text="Edit Email" OnClick="lnkEditEmail_Click"></asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:CheckBox ID="cbxInventorDisclosureDraft" runat="server" Text="Send email to Subscribers when a disclosure is set back to draft" />
                </td>
                <td>
                    <asp:LinkButton ID="lnkInventorDisclosureDraft" runat="server" Text="Edit Email" OnClick="lnkEditEmail_Click"></asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:CheckBox ID="cbxInventorDisclosureDigitalSignature" runat="server" Text="Send email to Inventor when a disclosure is ready to be digitally signed" />
                </td>
                <td>
                    <asp:LinkButton ID="lnkInventorDisclosureDigitalSignature" runat="server" Text="Edit Email" OnClick="lnkEditEmail_Click"></asp:LinkButton>
                </td>
            </tr>
            <tr><td style="height:10px;"></td></tr>
            <tr>
                <td colspan="2">
                    <div class="subtitlebar">Administrators</div>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:CheckBox ID="cbxAdminRequestNewAccount" runat="server" Text="Send email to all Portal Administrators when Inventor requests a new portal account" />
                </td>
                <td>
                    <asp:LinkButton ID="lnkAdminRequestNewAccount" runat="server" Text="Edit Email" OnClick="lnkEditEmail_Click"></asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:CheckBox ID="cbxAdminInventorSignature" runat="server" Text="Send email to Disclosure Type Administrators when Inventor digitally signs a disclosure" />
                </td>
                <td>
                    <asp:LinkButton ID="lnkAdminInventorSignature" runat="server" Text="Edit Email" OnClick="lnkEditEmail_Click"></asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:CheckBox ID="cbxAdminDisclosureSubmitted" runat="server" Text="Send email to Disclosure Type Administrators when a disclosure is submitted" />
                </td>
                <td>
                    <asp:LinkButton ID="lnkAdminDisclosureSubmitted" runat="server" Text="Edit Email" OnClick="lnkEditEmail_Click"></asp:LinkButton>
                </td>
            </tr>
            <tr><td style="height:10px;"></td></tr>
            <tr>
                <td colspan="2">
                    <div class="subtitlebar">All</div>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:CheckBox ID="cbxRemarkAdded" runat="server" Text="Send email to Disclosure Type Administrators and Subscribers when a remark is added to a disclosure" />
                </td>
                <td>
                    <asp:LinkButton ID="lnkRemarkAdded" runat="server" Text="Edit Email" OnClick="lnkEditEmail_Click"></asp:LinkButton>
                </td>
            </tr>
            <tr><td style="height:10px;"></td></tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>

<wtf:FormButtonControl ID="btnSave" runat="server" Text="Save Changes" LoadingText="Saving..." OnClick="btnSave_Click" IsWrapTable="true" />
</div>

<wtc:ConfirmPromptControl ID="cpcSuccess" runat="server" PromptType="Success" />

<wtd:ModalDialogControl ID="mdcEdit" runat="server" IsDefaultButtons="true" Title="Edit Email Template" OnClick="mdcEdit_Click" Width="700">
    <DialogContent>
        <asp:HiddenField ID="hidCurrentButtonId" runat="server" />
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td valign="top">
                    <wtf:FormTextBoxControl ID="txtFromEmail" runat="server" LabelText="From Email Address:" Width="400" />
                    <wtf:FormTextBoxControl ID="txtSubject" runat="server" LabelText="Subject:" Width="400" />
                    <wtf:FormTextBoxControl ID="txtBody" runat="server" LabelText="Email Body:" TextMode="MultiLine" Rows="15"  Width="400"/>
                    <asp:LinkButton ID="lnkReset" runat="server" Text="Reset to Factory" OnClick="lnkReset_Click"></asp:LinkButton>
                </td>
                <td style="width:10px;"></td>
                <td valign="top">
                    <div class="fieldlabel">Available Template Fields:</div>
                    <asp:TextBox ID="txtTemplateFields" runat="server" ReadOnly="true" TextMode="MultiLine" Rows="25"  Width="200" ></asp:TextBox>
                </td>
            </tr>
        </table>
    </DialogContent>
</wtd:ModalDialogControl>

</asp:Content>
