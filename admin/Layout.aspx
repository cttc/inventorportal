﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InventorPortal/InventorPortal.Master" AutoEventWireup="true" CodeBehind="Layout.aspx.cs" Inherits="Inteum.Server.Web.InventorPortal.admin.Layout"  ValidateRequest="false" %>
<%@ Register src="~/WebControls/Template.ascx" tagname="Template" tagprefix="wctrl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<tb:TitleBar ID="title1" runat="server" Text="Site Layout" />

<div class="content_indent">
<wtf:FormCheckboxControl ID="cbxIsCustomLogo" runat="server" LabelText="Use a Custom Logo" />

<table border="0" cellpadding="3" cellspacing="0">
    <tr>
        <td valign="top">
            <wtf:FormFileUploadControl ID="fileLogo" runat="server" LabelText="Upload Logo:" />
        </td>
        <td valign="top">
            <wtf:FormImagePlaceholderControl ID="imgLogo" runat="server" LabelText="Current Logo(suggested size 60px x 160px):" ImageWidth="300" ImageHeight="100"/>
        </td>
    </tr>
</table>

<wtf:FormCheckboxControl ID="cbxCustomCss" runat="server" LabelText="Use a Custom CSS Style Sheet" />
<wtf:FormFileUploadControl ID="fileCss" runat="server" LabelText="Upload Custom CSS Style Sheet:" />
<div class="fieldlabel"><asp:HyperLink ID="hypDownload" runat="server" Text="Download Default CSS Style Sheet"></asp:HyperLink></div>
<div class="fieldlabel"><asp:LinkButton ID="lnkCustomCSS" runat="server" Text="Download Custom CSS Style Sheet" OnClick="lnkCustomCSS_Click" Visible="false"></asp:LinkButton></div>
<div style="height:40px;"></div>

<table border="0" cellpadding="3" cellspacing="0">
    <tr>
        <td valign="top">
           <wtf:FormCheckboxControl ID="cbxIsUseCustomHeaderTemplate" runat="server" LabelText="Use a Custom Header"  />
           <wtf:FormCheckboxControl ID="cbxIsUseCustomFooterTemplate" runat="server" LabelText="Use a Custom Footer"  />
        </td>
        <td style="width: 80px;"></td>
        <td valign="top">
            <wctrl:Template ID="templateEdit" runat="server" visible="false"/>
        </td>
    </tr>
</table>

<wtf:FormButtonControl ID="btnSave" runat="server" Text="Save" LoadingText="Saving..." OnClick="btnSave_Click" />
</div>

<wtc:ConfirmPromptControl ID="cpcSuccess" runat="server" PromptType="Success" PromptButtons ="Ok"/>

</asp:Content>
