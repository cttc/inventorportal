﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InventorPortal/InventorPortal.Master" AutoEventWireup="True" CodeBehind="FormTemplate.aspx.cs" Inherits="Inteum.Server.Web.InventorPortal.admin.FormTemplate" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <tb:TitleBar ID="title1" runat="server" Text="Disclosure Template" />

<table border="0" cellpadding="3" cellspacing="0" width="100%">
    <tr>
        <td>
            Disclosure Type: <asp:DropDownList ID="ddnType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddnType_SelectedIndexChanged"></asp:DropDownList>
        </td>
        <td align="right" style="width:200px;">
            <asp:Button ID="btnAddTemplateField" runat="server" Text="Add Template Field" Width="200" OnClick="btnAddTemplateField_Click" />
        </td> 
         <td align="right" style="width:200px;">
           <asp:Button ID="btnCopyTemplateField" runat="server" Text="Copy Template Field" Width="200" OnClick="btnCopyTemplateField_Click" />
        </td> 
     </tr>

</table>
<div style="height:5px;"></div>


<asp:Panel ID="pnlTypeFields" runat="server">
<asp:UpdatePanel ID="updatePanelFields" runat="server">
<ContentTemplate>
<telerik:RadPanelBar ID="RadPanelBar1" runat="server" Width="100%">
    <Items>
        <telerik:RadPanelItem Text="Top Section" Expanded="true">
            <Items>
                <telerik:RadPanelItem>
                    <ContentTemplate>
                        <asp:GridView ID="gvTopSection" runat="server" 
                            AllowSorting="false"
                            AutoGenerateColumns="false" 
                            ShowHeader="false" 
                            ShowFooter="false"
                            OnRowDataBound="gvTopSection_RowDataBound"
                            GridLines="None" 
                            Width="100%" CellPadding="3"
                            AllowPaging="false">
                            <RowStyle BackColor="#ffffff" />
                            <AlternatingRowStyle BackColor="#efefef" />
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <div>
                                            <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                                <tr>
                                                   <td valign="top" style="width:150px;">
                                                        <asp:Label ID="lblGroup" runat="server" Text='<%# Eval("SectType")  %>'></asp:Label>
                                                    </td>
                                                    <td valign="top" align="left">
                                                        <asp:Label ID="lblControlPreviewTop" runat="server" Text='<%# FormatControlPreview(Container.DataItem) %>'></asp:Label>
                                                    </td>
                                                    <td valign="top" style="width:80px;">
                                                        <asp:HiddenField ID="hidPkTop" runat="server" Value='<%# Eval("PrimaryKey") %>' />
                                                        <asp:HiddenField ID="hidOrderNoTop" runat="server" Value='<%# Eval("SortOrder") %>' />
                                                        <div><asp:LinkButton ID="lnkEditTop" runat="server" Text="Edit" OnClick="lnkEdit_Click"></asp:LinkButton></div>
                                                        <div><asp:LinkButton ID="lnkDeleteTop" runat="server" Text="Delete" OnClick="lnkDelete_Click"></asp:LinkButton></div>
                                                        <asp:ConfirmButtonExtender ID="cbeDeleteTop" runat="server" 
                                                            TargetControlID="lnkDeleteTop" 
                                                            ConfirmText="Are you sure you want to delete this field and all disclosure data saved on this field?">
                                                        </asp:ConfirmButtonExtender>
                                                    </td>
                                                    <td valign="top" style="width:80px;">
                                                        <div><asp:LinkButton ID="lnkMoveUpTop" runat="server" Text="Move Up" OnClick="lnkMoveUp_Click"></asp:LinkButton></div>
                                                        <div><asp:LinkButton ID="lnkMoveDownTop" runat="server" Text="Move Down" OnClick="lnkMoveDown_Click"></asp:LinkButton></div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div style="height:10px;"></div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                </telerik:RadPanelItem>
            </Items>
        </telerik:RadPanelItem>
        <telerik:RadPanelItem Text="Middle Left Section" Expanded="true">
            <Items>
                <telerik:RadPanelItem>
                    <ContentTemplate>
                        <asp:GridView ID="gvMiddleLeftSection" runat="server" 
                            AllowSorting="false" 
                            AutoGenerateColumns="false" 
                            ShowHeader="false" 
                            ShowFooter="false"
                            OnRowDataBound="gvMiddleLeftSection_RowDataBound"
                            GridLines="None" 
                            Width="100%" CellPadding="3"
                            AllowPaging="false">
                            <RowStyle BackColor="#ffffff" />
                            <AlternatingRowStyle BackColor="#efefef" />
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <div>
                                            <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                                <tr>
                                                   <td valign="top" style="width:150px;">
                                                        <asp:Label ID="lblGroup" runat="server" Text='<%# Eval("SectType")  %>'></asp:Label>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="lblControlPreviewMiddleLeft" runat="server" Text='<%# FormatControlPreview(Container.DataItem) %>'></asp:Label>
                                                    </td>
                                                    <td valign="top" style="width:80px;">
                                                        <asp:HiddenField ID="hidPkMidLeft" runat="server" Value='<%# Eval("PrimaryKey") %>' />
                                                        <asp:HiddenField ID="hidOrderNoMiddleLeft" runat="server" Value='<%# Eval("SortOrder") %>' />
                                                        <div><asp:LinkButton ID="lnkEditMiddleLeft" runat="server" Text="Edit" OnClick="lnkEdit_Click"></asp:LinkButton></div>
                                                        <div><asp:LinkButton ID="lnkDeleteMiddleLeft" runat="server" Text="Delete" OnClick="lnkDelete_Click"></asp:LinkButton></div>
                                                        <asp:ConfirmButtonExtender ID="cbeDeleteMiddleLeft" runat="server" 
                                                            TargetControlID="lnkDeleteMiddleLeft" 
                                                            ConfirmText="Are you sure you want to delete this field and all disclosure data saved on this field?">
                                                        </asp:ConfirmButtonExtender>
                                                    </td>
                                                    <td valign="top" style="width:80px;">
                                                        <div><asp:LinkButton ID="lnkMoveUpMiddleLeft" runat="server" Text="Move Up" OnClick="lnkMoveUp_Click"></asp:LinkButton></div>
                                                        <div><asp:LinkButton ID="lnkMoveDownMiddleLeft" runat="server" Text="Move Down" OnClick="lnkMoveDown_Click"></asp:LinkButton></div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div style="height:10px;"></div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                </telerik:RadPanelItem>
            </Items>
        </telerik:RadPanelItem>
        <telerik:RadPanelItem Text="Middle Right Section" Expanded="true">
            <Items>
                <telerik:RadPanelItem>
                    <ContentTemplate>
                        <asp:GridView ID="gvMiddleRightSection" runat="server" 
                            AllowSorting="false" 
                            AutoGenerateColumns="false" 
                            ShowHeader="false" 
                            ShowFooter="false"
                            OnRowDataBound="gvMiddleRightSection_RowDataBound"
                            GridLines="None" 
                            Width="100%" CellPadding="3"
                            AllowPaging="false">
                            <RowStyle BackColor="#ffffff" />
                            <AlternatingRowStyle BackColor="#efefef" />
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <div>
                                            <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                                <tr>
                                                    <td valign="top" style="width:150px;">
                                                        <asp:Label ID="lblGroup" runat="server" Text='<%# Eval("SectType")  %>'></asp:Label>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="lblControlPreviewMiddleRight" runat="server" Text='<%# FormatControlPreview(Container.DataItem) %>'></asp:Label>
                                                    </td>
                                                    <td valign="top" style="width:80px;">
                                                        <asp:HiddenField ID="hidPkMidRight" runat="server" Value='<%# Eval("PrimaryKey") %>' />
                                                        <asp:HiddenField ID="hidOrderNoMiddleRight" runat="server" Value='<%# Eval("SortOrder") %>' />
                                                        <div><asp:LinkButton ID="lnkEditMiddleRight" runat="server" Text="Edit" OnClick="lnkEdit_Click"></asp:LinkButton></div>
                                                        <div><asp:LinkButton ID="lnkDeleteMiddleRight" runat="server" Text="Delete" OnClick="lnkDelete_Click"></asp:LinkButton></div>
                                                        <asp:ConfirmButtonExtender ID="cbeDeleteMiddleRight" runat="server" 
                                                            TargetControlID="lnkDeleteMiddleRight" 
                                                            ConfirmText="Are you sure you want to delete this field and all disclosure data saved on this field?">
                                                        </asp:ConfirmButtonExtender>
                                                    </td>
                                                    <td valign="top" style="width:80px;">
                                                        <div><asp:LinkButton ID="lnkMoveUpMiddleRight" runat="server" Text="Move Up" OnClick="lnkMoveUp_Click"></asp:LinkButton></div>
                                                        <div><asp:LinkButton ID="lnkMoveDownMiddleRight" runat="server" Text="Move Down" OnClick="lnkMoveDown_Click"></asp:LinkButton></div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div style="height:10px;"></div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                </telerik:RadPanelItem>
            </Items>
        </telerik:RadPanelItem>
        <telerik:RadPanelItem Text="Bottom Section" Expanded="true">
            <Items>
                <telerik:RadPanelItem>
                    <ContentTemplate>
                        <asp:GridView ID="gvBottomSection" runat="server" 
                            AllowSorting="false" 
                            AutoGenerateColumns="false" 
                            ShowHeader="false" 
                            ShowFooter="false"
                            OnRowDataBound="gvBottomSection_RowDataBound"
                            GridLines="None" 
                            Width="100%" CellPadding="3"
                            AllowPaging="false">
                            <RowStyle BackColor="#ffffff" />
                            <AlternatingRowStyle BackColor="#efefef" />
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <div>
                                            <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                                <tr>
                                                    <td valign="top" style="width:150px;">
                                                        <asp:Label ID="lblGroup" runat="server" Text='<%# Eval("SECTTYPE")  %>'></asp:Label>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="lblControlPreviewBottom" runat="server" Text='<%# FormatControlPreview(Container.DataItem) %>'></asp:Label>
                                                    </td>
                                                    <td valign="top" style="width:80px;">
                                                        <asp:HiddenField ID="hidPkBottom" runat="server" Value='<%# Eval("PRIMARYKEY") %>' />
                                                        <asp:HiddenField ID="hidOrderNoBottom" runat="server" Value='<%# Eval("SORTORDER") %>' />
                                                        <div><asp:LinkButton ID="lnkEditBottom" runat="server" Text="Edit" OnClick="lnkEdit_Click"></asp:LinkButton></div>
                                                        <div><asp:LinkButton ID="lnkDeleteBottom" runat="server" Text="Delete" OnClick="lnkDelete_Click"></asp:LinkButton></div>
                                                        <asp:ConfirmButtonExtender ID="cbeDeleteBottom" runat="server" 
                                                            TargetControlID="lnkDeleteBottom" 
                                                            ConfirmText="Are you sure you want to delete this field and all disclosure data saved on this field?">
                                                        </asp:ConfirmButtonExtender>
                                                    </td>
                                                    <td valign="top" style="width:80px;">
                                                        <div><asp:LinkButton ID="lnkMoveUpBottom" runat="server" Text="Move Up" OnClick="lnkMoveUp_Click"></asp:LinkButton></div>
                                                        <div><asp:LinkButton ID="lnkMoveDownBottom" runat="server" Text="Move Down" OnClick="lnkMoveDown_Click"></asp:LinkButton></div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div style="height:10px;"></div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                </telerik:RadPanelItem>
            </Items>
        </telerik:RadPanelItem>
    </Items>
</telerik:RadPanelBar>


<div style="height:15px;"></div>
<div class="subtitlebar">Built-in Template Forms Section</div>
<table border="0" cellpadding="3" cellspacing="0" width="100%">
    <tr>
        <td valign="top" style="width:300px;">
            <div class="fieldlabel">Available Sections:</div>
            <asp:GridView ID="gvAvailableSections" runat="server" 
                Width="50%" GridLines="None" CellPadding="3" style="border:solid 1px #c0c0c0;"
                AutoGenerateColumns="false"
                AllowPaging="false" 
                AllowSorting="false" 
                ShowFooter="false" 
                ShowHeader="false"
                EmptyDataText="<div style='padding:10px;'>All available sections have already been added to the form.</div>">
                <RowStyle BackColor="#ffffff" />
                <AlternatingRowStyle BackColor="#efefef" />
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Label ID="lblSection" runat="server" Text='<%# Eval("SectionName") %>'></asp:Label>
                            <asp:HiddenField ID="hidSectionId" runat="server" Value='<%# Eval("Id") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Button ID="btnAddAvailableSection" runat="server" Text="Add" OnClick="btnAddAvailableSection_Click" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </td>
    </tr>
</table>

</ContentTemplate>
</asp:UpdatePanel>

</asp:Panel>
     

<wtd:ModalDialogControl ID="mdcEdit" runat="server" Title="[mode] Template Field" IsDefaultButtons="true" RepositionMode="RepositionOnWindowResizeAndScroll" ModalTop="50" Width="600" OnClick="mdcEdit_Click" >
    <DialogContent>
    <asp:Panel ID="pnlTemplateField" runat="server" CssClass="autoScrollPanel">
      <asp:Panel ID="pnlCustField" runat="server" Style="margin-bottom: 30px;">
        <wtf:FormDropDownControl ID="ddnFieldType" runat="server" LabelText="Field Type:" AutoPostBack="true" OnSelectedIndexChanged="ddnFieldType_SelectedIndexChanged" /> 
        <wtf:FormDropDownControl ID="ddnListTable" runat="server" LabelText="List Type:" AutoPostBack="true" OnSelectedIndexChanged="ddnListTable_SelectedIndexChanged"/>
        <wtf:FormTextBoxControl ID="txtFieldTitle" runat="server" LabelText="Field Title:" />
        <wtf:FormCheckboxControl ID="cbxIsRequired" runat="server" LabelText="Required" />
        <wtf:FormTextBoxControl ID="txtMaxLength" runat="server" Width="50" LabelText="Maximum Length:" />  
        <wtf:FormDropDownControl ID="ddnParentField" runat="server" LabelText="Parent Field:" AutoPostBack="true" OnSelectedIndexChanged="ddnParentField_SelectedIndexChanged"/>
        <wtf:FormDropDownControl ID="ddnTriggerValue" runat="server" LabelText="Trigger Value:" Visible="false" />
        <wtf:FormTextBoxControl ID="txtTriggerValue" runat="server" LabelText="Trigger Value:" Visible="false" />
        <asp:Label ID="lbWarning" runat="server" ForeColor="Red" Text="Note: The settings above are shared. Any changes to the values above will affect all the template fields that are copied from this one." />
      </asp:Panel>
        
        <wtf:FormDropDownControl ID="ddnViewable" runat="server" LabelText="Viewable in:" />
        <wtf:FormDropDownControl ID="ddnFormLocation" runat="server" LabelText="Form Location:" />
        <wtf:FormDropDownControl ID="ddnTitleLocation" runat="server" LabelText="Title Location: (for Inteum Web display only, use custom CSS file for the portal)" />
        <wtf:FormDropDownControl ID="ddnGroupName" runat="server" LabelText="Group Name:" AutoPostBack="true" OnSelectedIndexChanged="ddnGroupName_SelectedIndexChanged"/>
        <wtf:FormTextBoxControl ID="txtGroupName" runat="server" LabelText="New Group Name:" Visible="false"  />

        <wtf:FormTextBoxControl ID="txtFieldWidth" runat="server" Width="50" LabelText="Field Width:" />
        <wtf:FormTextBoxControl ID="txtDescription" runat="server" TextMode="MultiLine" Rows="4" Columns="50" LabelText="Description:" />
        <asp:Panel ID="pnlValues" runat="server" DefaultButton="btnAddValue">
            <table border="0" cellpadding="3" cellspacing="0">
                <tr>
                    <td><asp:Label ID="lblValueTitle" runat="server" Text="Values:"></asp:Label></td>
                    <td><asp:TextBox ID="txtValue" runat="server"></asp:TextBox></td>
                    <td><asp:Button ID="btnAddValue" runat="server" Text="Add Value" OnClick="btnAddValue_Click" /></td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="2">
                        <asp:CheckBox ID="cbxIsColumnRequired" runat="server" Text="Required" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:Panel ID="pnlGridValues" runat="server" Visible="false">
                        <telerik:RadGrid runat="server" ID="rgValues" 
                            Width="100%" 
                            OnItemDataBound="rgValues_ItemDataBound"
                            AllowPaging="true" 
                            AllowCustomPaging="true"
                            AllowSorting="true"
                            PageSize="5"
                            AutoGenerateColumns="false"
                            AllowFilteringByColumn="false"
                            OnNeedDataSource="RadGridValues_NeedDataSource">
                            <ClientSettings>
                                <Resizing AllowColumnResize="true" />
                            </ClientSettings>
                            <MasterTableView 
                                OverrideDataSourceControlSorting="true"
                                NoMasterRecordsText="<div style='padding:10px;'>No values have been added yet.</div>">
                                <Columns>
                                    <telerik:GridBoundColumn HeaderText="Value" DataField="Entry"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn HeaderText="Order" DataField="OrderNo"></telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn HeaderText="Manage">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hidPkValue" runat="server" Value='<%# Eval("PrimaryKey") %>' />
                                            <asp:LinkButton ID="lnkDeleteValue" runat="server" Text="Delete" OnClick="lnkDeleteValue_Click"></asp:LinkButton>
                                            <asp:ConfirmButtonExtender ID="cbeDeleteValue" runat="server" 
                                                TargetControlID="lnkDeleteValue" 
                                                ConfirmText="Are you sure you want to delete this value?">
                                            </asp:ConfirmButtonExtender>
                                            &nbsp;|&nbsp;
                                            <asp:LinkButton ID="lnkMoveUpValue" runat="server" Text="Up" OnClick="lnkMoveUpValue_Click"></asp:LinkButton>
                                            &nbsp;|&nbsp;
                                            <asp:LinkButton ID="lnkMoveDownValue" runat="server" Text="Down" OnClick="lnkMoveDownValue_Click"></asp:LinkButton>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                            </MasterTableView>
                        </telerik:RadGrid>                    
                        </asp:Panel>

                        <asp:Panel ID="pnlGridColumns" runat="server" Visible="false">
                        <telerik:RadGrid runat="server" ID="rgColumns" 
                            Width="100%" 
                            OnItemDataBound="rgColumns_ItemDataBound"
                            AllowPaging="true" 
                            AllowCustomPaging="true"
                            AllowSorting="true"
                            PageSize="5"
                            AutoGenerateColumns="false"
                            AllowFilteringByColumn="false"
                            OnNeedDataSource="RadGridColumns_NeedDataSource">
                            <ClientSettings>
                                <Resizing AllowColumnResize="true" />
                            </ClientSettings>
                            <MasterTableView 
                                OverrideDataSourceControlSorting="true" 
                                NoMasterRecordsText="<div style='padding:10px;'>No columns have been added yet.</div>">
                                <Columns>
                                    <telerik:GridBoundColumn HeaderText="Column" DataField="CUSTFELD.NAME"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn HeaderText="Order" DataField="FormSection.SORTORDER"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn HeaderText="Required" DataField="CUSTFELD.Required"></telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn HeaderText="Manage">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hidPkColumn" runat="server" Value='<%# Eval("PrimaryKey") %>' />
                                            <asp:LinkButton ID="lnkDeleteColumn" runat="server" Text="Delete" OnClick="lnkDeleteColumn_Click"></asp:LinkButton>
                                            <asp:ConfirmButtonExtender ID="cbeDeleteColumn" runat="server" 
                                                TargetControlID="lnkDeleteColumn" 
                                                ConfirmText="Are you sure you want to delete this column?">
                                            </asp:ConfirmButtonExtender>
                                            &nbsp;|&nbsp;
                                            <asp:LinkButton ID="lnkMoveUpColumn" runat="server" Text="Up" OnClick="lnkMoveUpColumn_Click"></asp:LinkButton>
                                            &nbsp;|&nbsp;
                                            <asp:LinkButton ID="lnkMoveDownColumn" runat="server" Text="Down" OnClick="lnkMoveDownColumn_Click"></asp:LinkButton>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                            </MasterTableView>
                        </telerik:RadGrid>                   
                        </asp:Panel>
                    </td>
                </tr>
            </table>
            <div style="height:10px;"></div>
        </asp:Panel>
     </asp:Panel>
    </DialogContent>
</wtd:ModalDialogControl>

    <wtd:ModalDialogControl ID="mdcEditForm" runat="server" Title="Edit Built-in Template Form" IsDefaultButtons="true" RepositionMode="RepositionOnWindowResizeAndScroll" ModalTop="50" Width="600" OnClick="mdcEditForm_Click" >
    <DialogContent>
     
        <wtf:FormTextBoxControl ID="tbSectionType" runat="server" LabelText="Group Name:" IsReadOnly="true" />
        <wtf:FormDropDownControl ID="ddnFormSection" runat="server" LabelText="Form Location:" /> 
    </DialogContent>
</wtd:ModalDialogControl>


<wtd:ModalDialogControl ID="mdcInvTypes" runat="server" Title="Select Disclosure type" IsDefaultButtons="true" RepositionMode="RepositionOnWindowResizeAndScroll" ModalTop="80" Width="600" ButtonText="OK" >
    <DialogContent>
         <asp:Panel ID="pnlTypes" runat="server" CssClass="autoScrollPanel">
                    <asp:GridView ID="gvTypes" runat="server" 
                            AllowSorting="true" 
                            AutoGenerateColumns="false" 
                            ShowHeader="true" 
                            ShowFooter="false"

                            Width="100%" CellPadding="3"
                            AllowPaging="false">
                            <RowStyle BackColor="#ffffff" />
                            <AlternatingRowStyle BackColor="#efefef" />
                            <Columns>
                              <asp:BoundField  HeaderText = "Type"   DataField = "NAME" />
                            
                              <asp:TemplateField>
                               <ItemTemplate> 
                                 <asp:HiddenField ID="hidTypePk" runat="server" Value='<%# Eval("PrimaryKey") %>' />
                                 <asp:LinkButton ID="lnkSelect" runat="server" Text="Select" OnClick="lnkSelectType_Click"></asp:LinkButton>
                              </ItemTemplate>
                               
                              </asp:TemplateField>
                             
                            </Columns>
                        </asp:GridView>
             </asp:Panel>
    </DialogContent>
</wtd:ModalDialogControl> 

            
<wtd:ModalDialogControl ID="mdcShared" runat="server" Title="Existing Template Fields" IsDefaultButtons="true" RepositionMode="RepositionOnWindowResizeAndScroll" ModalTop="80" Width="600" ButtonText="OK" >
    <DialogContent>
        <asp:Panel ID="pnlCopy" runat="server" CssClass="autoScrollPanel">

              <asp:GridView ID="gvCopy" runat="server" 
                            AllowSorting="false" 
                            AutoGenerateColumns="false" 
                            ShowHeader="true" 
                            ShowFooter="false"
                            Width="100%" CellPadding="3"
                            AllowPaging="true" OnPageIndexChanging="gvCopy_PageIndexChanging"
                            PageSize="10"
                            EmptyDataText="No template field to display"
                            >
                            <RowStyle BackColor="#ffffff" />
                            <AlternatingRowStyle BackColor="#efefef" />
                            <Columns>

                             <%--IIS 6 doesn't support bound field with sub objects. Have to use Template field instead.
                              http://stackoverflow.com/questions/5680652/a-field-or-property-with-the-name-was-not-found-on-the-selected-data-source --%>

                              <asp:TemplateField  HeaderText = "Title">
                               <ItemTemplate>
                                <asp:Label ID="lblCustFeldTitle" runat="server" Text='<%# Eval("CUSTFELD.NAME") %>'></asp:Label>
                               </ItemTemplate>
                              </asp:TemplateField>
                              <asp:TemplateField  HeaderText = "Data Type">
                               <ItemTemplate>
                                <asp:Label ID="lblCustFeldDataType" runat="server" Text='<%# Eval("CUSTFELD.DATATYPE") %>'></asp:Label>
                               </ItemTemplate>
                              </asp:TemplateField>

                              <asp:BoundField  HeaderText = "Group Name"  DataField = "GroupName">
                                  <ItemStyle Width="100px"  Wrap="true" />
                              </asp:BoundField>
                              <asp:BoundField  HeaderText = "Description" DataField = "Descrip" >
                                  <ItemStyle Width="200px"  Wrap="true" />
                              </asp:BoundField>

                              <asp:TemplateField>
                               <ItemTemplate> 
                                 <asp:HiddenField ID="hidPkCustLink" runat="server" Value='<%# Eval("PrimaryKey") %>' />
                                 <asp:LinkButton ID="lnkSelect" runat="server" Text="Select" OnClick="lnkSelect_Click"></asp:LinkButton>
                              </ItemTemplate>
                               
                              </asp:TemplateField>
                             
                            </Columns>
                        </asp:GridView>
            </asp:Panel>
        <div style="height:5px;"></div>
        <wtf:FormButtonControl ID="btnCopyAll" runat="server" Text="Copy All Template Fields" OnClick="btnCopyAll_Click" />
        
    </DialogContent>
</wtd:ModalDialogControl> 

<wtc:ConfirmPromptControl ID="cpcWarning" runat="server" PromptButtons="Ok" PromptType="Warning" />
</asp:Content>
