﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InventorPortal/InventorPortal.Master" AutoEventWireup="true" CodeBehind="Content.aspx.cs" Inherits="Inteum.Server.Web.InventorPortal.admin.Content" ValidateRequest="false"%>
<%@ Register src="~/WebControls/ContentManager.ascx" tagname="ContentManager" tagprefix="wctrl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <tb:TitleBar ID="title1" runat="server" Text="Content Manager" />

    <div class="content_indent">
    <div class="subtitle">You can edit the content any of the non-admin pages on Inventor Portal here.</div>
    <wctrl:ContentManager ID="contentManager1" runat="server" />
    <wtf:FormButtonControl ID="btnSave" runat="server" Text="Save Changes" OnClick="btnSave_Click" />
    </div>

    <div style="height:10px;"></div>

    <wtc:ConfirmPromptControl ID="cpcSuccess" runat="server" PromptType="Success" />

</asp:Content>
