﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InventorPortal/InventorPortal.Master" AutoEventWireup="true" CodeBehind="DisclosureTypes.aspx.cs" Inherits="Inteum.Server.Web.InventorPortal.admin.DisclosureTypes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="updatePanelMain" runat="server">
    <ContentTemplate>
        <tb:TitleBar ID="title1" runat="server" Text="Disclosure Types" />

        <div class="content_indent">
        <asp:Button ID="btnAddType" runat="server" Text="Add Disclosure Type" OnClick="btnAddType_Click" />
        <div style="height:5px;"></div>

        <telerik:RadGrid runat="server" ID="rgMain" 
            Width="100%" 
            AllowPaging="true" 
            AllowCustomPaging="true"
            AllowSorting="true"
            PageSize="20"
            AutoGenerateColumns="false"
            AllowFilteringByColumn="false"
            OnNeedDataSource="RadGrid_NeedDataSource">
            <ClientSettings>
                <Resizing AllowColumnResize="true" />
            </ClientSettings>
            <MasterTableView 
                OverrideDataSourceControlSorting="true" 
                NoMasterRecordsText="<div style='padding:10px;'>No disclosure types have been added yet.</div>">
                <Columns>
                    <telerik:GridBoundColumn HeaderText="Type" DataField="Name" SortExpression="Name" ></telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderText="Created" DataField="RCreatedD" SortExpression="RCreatedD"></telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn HeaderText="Manage">
                        <ItemTemplate>
                            <asp:HiddenField ID="hidPk" runat="server" Value='<%# Eval("PrimaryKey") %>' />
                            <asp:LinkButton ID="lnkEdit" runat="server" Text="Edit" OnClick="lnkEdit_Click"></asp:LinkButton>
                            &nbsp;|&nbsp;
                            <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete" OnClick="lnkDelete_Click"></asp:LinkButton>
                            <asp:ConfirmButtonExtender ID="cbeDelete" runat="server" 
                                TargetControlID="lnkDelete" 
                                ConfirmText="Are you sure you want to delete this disclosure type?">
                            </asp:ConfirmButtonExtender>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>

        </div>
    </ContentTemplate>
</asp:UpdatePanel>

<wtd:ModalDialogControl ID="mdcEdit" runat="server" Title="[mode] Disclosure Type" IsDefaultButtons="true" OnClick="btnSave_Click" Width="420">
    <DialogContent>
        <asp:Panel ID="Panel1" runat="server" CssClass="autoScrollPanel">
            <wtf:FormTextBoxControl ID="txtName" runat="server" Width="300" LabelText="Name:" />
            <wtf:FormCheckboxControl ID="chActive" runat="server" LabelText="Active" />
            <wtf:FormDropDownControl ID="ddnManager" runat="server" LabelText="Default Manager of new Disclosures: *" />
            <wtf:FormDropDownControl ID="ddnInventionId" runat="server" LabelText="Disclosure Invention ID Auto-Fill: *" />
            <wtf:FormDropDownControl ID="ddnTechId" runat="server" LabelText="Technology Tech ID Auto-Fill: *" />
            <wtf:FormDropDownControl ID="ddnPDFTemplate" runat="server" LabelText="PDF Template:" />
            <wtf:FormDropDownControl ID="ddnWordTemplate" runat="server" LabelText="MS Word Template:" />
            <wtf:FormDropDownControl ID="ddnGrntAwrdRSC" runat="server" LabelText="Grant/Award Record Collection:" />
            <wtf:FormDropDownControl ID="ddnInvDiscRSC" runat="server" LabelText="Disclosure Record Collection:" />
            <wtf:FormDropDownControl ID="ddnTechRSC" runat="server" LabelText="Technology Record Collection:" />
            <wtf:FormDropDownControl ID="ddnMktTgtRSC" runat="server" LabelText="Marketing Target Record Collection:" />
            <wtf:FormDropDownControl ID="ddnDocusignPdfRSC" runat="server" LabelText="Docusign PDF Record Collection:" />
            <div runat="server" ID="divDocusignWarning">
                <wtf:FormCheckboxControl ID="cbxDocusignWarning" runat="server" LabelText="Show DocuSign signature delay warning?"/>
                <div style="margin-left:20px;">
                    <div class="small_desc">When using DocuSign, Envelope status can only be scanned every 20 minutes.   The inventor form will continue to show "Sign Now" until the status has been updated and the disclosure page has been refreshed.</div>
                    <div style="height:7px;"></div>  
                    <div class="small_desc">Note:  Warning message can be updated in Content Management.</div>
                </div>
            </div>
            <div style="height:25px;"></div>   
            <div class="fieldlabel">Assign Administrators for Email Notification:</div>
            <asp:CheckBoxList ID="cbxAdministrators" runat="server"></asp:CheckBoxList>   
        </asp:Panel>
    </DialogContent>
</wtd:ModalDialogControl>

<wtc:ConfirmPromptControl ID="cpcDelete" runat="server" PromptButtons="Ok" PromptType="Error" />

</asp:Content>
