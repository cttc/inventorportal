﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InventorPortal/InventorPortal.Master" AutoEventWireup="True" CodeBehind="Users.aspx.cs" Inherits="Inteum.Server.Web.InventorPortal.admin.Users" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <tb:TitleBar ID="title1" runat="server" Text="Users" />

<div class="content_indent">
<asp:UpdatePanel ID="updatePanelMain" runat="server">
    <ContentTemplate>
        <table border="0" cellpadding="3" cellspacing="0">
            <tr>
                <td valign="top">
                    <wtf:FormSearchControl ID="txtSearch" runat="server" OnClick="btnSearch_Click" CssTextBox="SearchBox"/>
                </td>
                <td>&nbsp;&nbsp;</td>
                <td>Status:</td>
                <td valign="top">
                    <wtf:FormDropDownControl ID="ddnStatus" runat="server" IsDisplayLabel="false" AutoPostBack="true" OnSelectedIndexChanged="ddnStatus_SelectedIndexChanged" />
                </td>
            </tr>
        </table>

        <telerik:RadGrid runat="server" ID="rgMain" 
            Width="100%" 
            OnItemDataBound="rg_ItemDataBound"
            AllowPaging="true" 
            AllowCustomPaging="true"
            AllowSorting="true"
            PageSize="20"
            AutoGenerateColumns="false"
            AllowFilteringByColumn="false"
            OnNeedDataSource="RadGrid_NeedDataSource">
            <ClientSettings>
                <Resizing AllowColumnResize="true" />
            </ClientSettings>
            <MasterTableView 
                OverrideDataSourceControlSorting="true"
                NoMasterRecordsText="<div style='padding:10px;'>No users match this criteria.</div>">
                <Columns>
                     <telerik:GridTemplateColumn HeaderText="User Name" SortExpression="User.LoginID">
                        <ItemTemplate>
                            <asp:Label ID="lblUserID" runat="server" Text='<%# Eval("User.LoginID") %>'></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn HeaderText="First Name" SortExpression="User.Contact.FirstName">
                        <ItemTemplate>
                            <asp:Label ID="lblFirst" runat="server" Text='<%# FormatFirstName(Eval("User.ContactsFk")) %>'></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn HeaderText="Last Name" SortExpression="User.Contact.LastName">
                        <ItemTemplate>
                            <asp:Label ID="lblLast" runat="server" Text='<%# FormatLastName(Eval("User.ContactsFk")) %>'></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn HeaderText="Email" SortExpression="User.Contact.Email">
                        <ItemTemplate>
                            <asp:Label ID="lblEmail" runat="server" Text='<%# FormatEmail(Eval("User.ContactsFk")) %>'></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn HeaderText="Originated" DataField="User.InvPortOr" SortExpression="User.InvPortOr">
                        <ItemTemplate>
                            <asp:Label ID="lblOriginated" runat="server" Text='<%# FormatOrigin(Eval("User.InvPortOr")) %>'></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn HeaderText="Role" DataField="RoleType" SortExpression="RoleType"></telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn HeaderText="Status" DataField="Active" SortExpression="Active">
                        <ItemTemplate>
                            <asp:Label ID="lblStatus" runat="server" Text='<%# FormatStatus(Eval("Active"), Eval("User.InvPortEn")) %>'></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                      <telerik:GridTemplateColumn HeaderText="Security" SortExpression="User.SecurityGroup.NAME" HeaderStyle-Width="200">
                        <ItemTemplate>
                            <asp:HiddenField ID="hidSecGrpFk" runat="server" Value='<%# Eval("User.SecgrpsFK") %>' />
                            <asp:Label ID="lblSecurity" runat="server" Text='<%# FormatSecurity(Eval("User.SecgrpsFK")) %>'></asp:Label>
                            <asp:LinkButton ID="lnkFixSecurity" runat="server" Text="Fix" OnClick="lnkFixSecurity_Click" Visible="false"></asp:LinkButton>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn HeaderText="Manage" AllowFiltering="false" HeaderStyle-Width="250">
                        <ItemTemplate>
                            <asp:HiddenField ID="hidPk" runat="server" Value='<%# Eval("PrimaryKey") %>' />
                            <asp:HiddenField ID="hidUserFk" runat="server" Value='<%# Eval("UsersFk") %>' />
                            <asp:HiddenField ID="hidSecGrpFk2" runat="server" Value='<%# Eval("User.SecgrpsFK") %>' />
                            <asp:HiddenField ID="hidActive" runat="server" Value='<%# Eval("Active") %>' />
                            <asp:HiddenField ID="hidInvPortEn" runat="server" Value='<%# Eval("User.InvPortEn") %>' />
                            <asp:HiddenField ID="hidFirstName" runat="server" Value='<%# Eval("User.Contact.FirstName") %>' />
                            <asp:HiddenField ID="hidLastName" runat="server" Value='<%# Eval("User.Contact.LastName") %>' />
                            <asp:HiddenField ID="hidRole" runat="server" Value='<%# Eval("RoleType") %>' />
                            <asp:LinkButton ID="lnkChangeRole" runat="server" Text="Change Role" OnClick="lnkChangeRole_Click"></asp:LinkButton>
                            &nbsp;|&nbsp;
                            <asp:LinkButton ID="lnkChangeSecurity" runat="server" Text="Change Security" OnClick="lnkChangeSecurity_Click"></asp:LinkButton>
                            <asp:Panel ID="pnlActivate" runat="server" Visible="true" style="display:inline;">
                                &nbsp;|&nbsp;
                                <asp:LinkButton ID="lnkActivate" runat="server" Text="Activate" OnClick="lnkActivate_Click"></asp:LinkButton>
                            </asp:Panel>
                            <asp:Panel ID="pnlApproveDeny" runat="server" Visible="false" style="display:inline;">
                                &nbsp;|&nbsp;
                                <asp:LinkButton ID="btnApprove" runat="server" Text="Approve" OnClick="btnApprove_Click"></asp:LinkButton>
                                &nbsp;|&nbsp;
                                <asp:LinkButton ID="btnDeny" runat="server" Text="Deny" OnClick="btnDeny_Click"></asp:LinkButton>
                            </asp:Panel>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>

    </ContentTemplate>
</asp:UpdatePanel>
</div>

<wtd:ModalDialogControl ID="mdcEditRole" runat="server" Title="Change User Role" IsDefaultButtons="true" OnClick="mdcEditRole_Click">
    <DialogContent>
        <div>Change Role for User:</div>
        <div><asp:Label ID="lblName" runat="server" Font-Bold="true"></asp:Label></div>
        <div style="height:10px;"></div>
        <wtf:FormDropDownControl ID="ddnRole" runat="server" LabelText="Role:" />
    </DialogContent>
</wtd:ModalDialogControl>

<wtd:ModalDialogControl ID="mdcEditSecurity" runat="server" Title="Change User Security" IsDefaultButtons="true" OnClick="mdcEditSecurity_Click">
    <DialogContent>
        <div>Change Security Group for User:</div>
        <div><asp:Label ID="lblName2" runat="server" Font-Bold="true"></asp:Label></div>
        <div style="height:10px;"></div>
        <wtf:FormDropDownControl ID="ddnSecurity" runat="server" LabelText="Security Group:" />
    </DialogContent>
</wtd:ModalDialogControl>

<wtd:ModalDialogControl ID="mdcFixSecurity" runat="server" Title="Fix User Security" IsDefaultButtons="true" OnClick="mdcFixSecurity_Click" Width="400">
    <DialogContent>
        <div>Current security group is missing the following security area:</div>
        <div class="fieldlabel"><asp:ListBox ID="lbxMissingAreas" runat="server" Width="300" Height="300" ></asp:ListBox></div>
        <wtf:FormCheckboxControl ID="chxFixSecurity" runat="server"  LabelText="Increase Security to minimum requirements. <br/><br/>NOTE: This will affect all users assigned to this group.  Once complete, you will need to use SetupWS.exe (Tools->Check Table Structures->Rebuild views with 'Materialize' check box checked) to complete the security changes." />   
    </DialogContent>
</wtd:ModalDialogControl>

<wtc:ConfirmPromptControl ID="cpcError" runat="server" PromptType="Error" CloseText="OK"/>

</asp:Content>
