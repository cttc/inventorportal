﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InventorPortal/InventorPortal.Master" AutoEventWireup="true" CodeBehind="Settings.aspx.cs" Inherits="Inteum.Server.Web.InventorPortal.admin.Settings" ValidateRequest="false" %>
<%@ Register src="~/WebControls/EmailSettings.ascx" tagname="EmailSettings" tagprefix="wctrl" %>
<%@ Register src="~/WebControls/Template.ascx" tagname="Template" tagprefix="wctrl" %>
<%@ Register src="~/WebControls/DocusignSettings.ascx" tagname="DocusignSettings" tagprefix="wctrl" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<tb:TitleBar ID="title1" runat="server" Text="Settings" />

<div class="content_indent">
<div class="subtitlebar">Basic Settings</div>
    <wtf:FormDropDownControl ID="ddnCultureOverride" runat="server" LabelText="Display Language and date format for date selector controls:" />
<div style="height:10px;"></div>
<div>User Account Approval</div>

<asp:RadioButtonList ID="rdoUserAccountApproval" runat="server">
    <asp:ListItem Text="Automatically approve users when an account is requested (you can always deny access later)" Value="automatic"></asp:ListItem>
    <asp:ListItem Text="Require an administrator to approve a user account before allowing access" Value="manual"></asp:ListItem>
</asp:RadioButtonList>
<div style="height:10px;"></div>
<wtf:FormCheckboxControl ID="cbxCheckDuplicate" runat="server" LabelText="Allow new portal users to view/verify first name, last name and email address against similar entries to avoid duplicate contact record creation." />
<wtf:FormCheckboxControl ID="cbxCanUserAddNew" runat="server" LabelText="Allow portal users to add new Company or Contact record if no results found after searching." />
<wtf:FormCheckboxControl ID="cbxCustomGender" runat="server" LabelText="Allow portal users to create new Gender entries by typing new values in Gender list box (Edit Profile and new Contacts)." />
<wtf:FormCheckboxControl ID="chxHideProfile" runat="server" LabelText="Hide profile link for users of type 'General User'." />
<wtf:FormCheckboxControl ID="chxHideMiddleName" runat="server" LabelText="Hide middle name field on contact info page." />
<wtf:FormCheckboxControl ID="cbxRequireTwoAddresses" runat="server" LabelText="Require new users to specify at least <b>2</b> addresses (for compliance with some patent laws)" />
<wtf:FormCheckboxListControl ID="cblRequiredFields" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal" CellPadding="5"  LabelText="Required Fields on Profile and Create Account page:" />

<div style="height:10px;"></div>
<div class="subtitlebar">Site Settings</div>
<wtf:FormTextBoxControl ID="txtSiteAdminEmail" runat="server" LabelText="Default Email Address as sender of Portal Email Notifications: *" Width="300" />
<wtf:FormTextBoxControl ID="txtFeedbackEmail" runat="server" LabelText="Email Address to receive feedback questions submitted on Feedback form: *" Width="300" />
<wtf:FormDropDownControl ID="ddnDefaultSecurityGroup" runat="server" LabelText="Default Security Group for new user accounts created on Inventor Portal: *" />
<wtf:FormTextBoxControl ID="txtForgotPasswordTimeout" runat="server" LabelText="Forgot Password Timeout Hour(s):" Width="50" />
<asp:CheckBox ID="cbxRequireTermsOnLogin" runat="server" Text="Require site terms of use to be accepted before allowing the user to login" AutoPostBack="true" OnCheckedChanged="cbxRequireTermsOnLogin_CheckedChanged"  />
<wctrl:Template ID="SiteTerm" runat="server" Visible="false"/>
    
<div style="height:10px;"></div>
<div id="divEmailSend" runat="server" class="subtitlebar">Email Send Preferences</div>
<wctrl:EmailSettings ID="emailSettings1" runat="server" />
<div style="height:10px;"></div>

<div class="subtitlebar">Disclosure Form Settings</div>
<wtf:FormCheckboxControl ID="chxCollapseSection" runat="server" LabelText="Collapse all sections when form is opened."/>
<wtf:FormCheckboxControl ID="chxAllowAdminEdit" runat="server" LabelText="Allow administrators to edit sumbitted or approved disclosures." /> 
<wtf:FormCheckboxControl ID="chxAllowDocument" runat="server" LabelText="Allow users to upload or delete documents after disclosure has been sumbitted or approved." /> 
<wtf:FormCheckboxControl ID="chxCopyDocumentToTech" runat="server" LabelText="Copy documents from disclosure to linked technology after disclosure has been approved." /> 
<asp:CheckBox ID="chxAutoPdfOnApproval" runat="server" Text="Automatically add PDF copy of disclosure to Technology and Disclosure documents when disclosure is approved."  AutoPostBack="true" OnCheckedChanged="chxAutoPdfOnApproval_CheckedChanged"/>
<wtf:FormTextBoxControl ID="tbPdfSuffix" runat="server" Style="margin-left:20px;" LabelText="PDF Filename suffix (see Disclosure Download Templates for file name)" />
          
<div style="height:10px;"></div>
<div>Display Tooltips for Disclosure Form Fields as:</div>
<asp:RadioButtonList ID="rdoFormToolTips" runat="server">
    <asp:ListItem Text="Show Template Field Tooltips when the user <b>hovers</b> over the input control" Value="hover"></asp:ListItem>
    <asp:ListItem Text="Show Template Field Tooltips inline with the field title" Value="inline"></asp:ListItem>
</asp:RadioButtonList>
<div style="height:10px;"></div>

<div>Create Technology for Disclosure when:</div>
<asp:RadioButtonList ID="rdoCreateTech" runat="server">
    <asp:ListItem Text="Disclosure is created as draft" Value="D"></asp:ListItem>
    <asp:ListItem Text="Disclosure is submitted" Value="S"></asp:ListItem>
    <asp:ListItem Text="Disclosure is approved" Value="A"></asp:ListItem>
</asp:RadioButtonList>
<div style="height:10px;"></div>

<div>Automatically add the creator of new disclosure as:</div>
<asp:RadioButtonList ID="rdoDisclosureCreator" runat="server">
    <asp:ListItem Text="Inventor" Value="I"></asp:ListItem>
    <asp:ListItem Text="Subscriber" Value="S"></asp:ListItem>
    <asp:ListItem Text="Both" Value="B"></asp:ListItem>
    <asp:ListItem Text="None" Value="N"></asp:ListItem>
</asp:RadioButtonList>
<div style="height:10px;"></div>

<div>Grants</div>
<wtf:FormCheckboxControl ID="cbxRequireFunding" runat="server" LabelText="Requires at least one Grant when disclosure is submitted" />
<wtf:FormCheckboxControl ID="cbxRequireGrantTitle" runat="server" LabelText="Make Grant Title required in Inventor Portal and Inteum Web" />
<wtf:FormCheckboxControl ID="cbxRequireGrantNo" runat="server" LabelText="Make Grant Number required in Inventor Portal and Inteum Web" />
<wtf:FormCheckboxControl ID="cbxShowFundingInst" runat="server" LabelText="Show Funding Institutions only in Sponsor Search Results" />
<wtf:FormCheckboxControl ID="cbxInventorsCanSearchAllGrantAwards" runat="server" LabelText="Inventors (Non Inteum Web users) can only see their Grant Awards when adding grants to disclosure" />
<wtf:FormCheckboxControl ID="cbxRequireSponsor" runat="server" LabelText="Make Sponsor required" />
<wtf:FormCheckboxControl ID="cbxRequireInvestigator" runat="server" LabelText="Make Investigator required" />
<wtf:FormCheckboxControl ID="cbxHideInvestigator" runat="server" LabelText="Hide Investigator related fields" />
<wtf:FormCheckboxControl ID="cbxHideInternalId" runat="server" LabelText="Hide Grant Internal ID in Inventor Portal" />
<wtf:FormCheckboxControl ID="cbxRequireInternalId" runat="server" LabelText="Make Grant Internal ID required in Inventor Portal and Inteum Web" />
<wtf:FormCheckboxControl ID="cbxCreateIR" runat="server" LabelText="When Disclsoure is approved also creates an Invention Reporting record from each grant on Funding section for " AutoPostBack="true" OnCheckedChanged="cbxCreateIR_CheckedChanged" />
<asp:RadioButtonList ID="rdoCreateIR" runat="server">
<asp:ListItem Text="Main Sponsor Only" Value="M"></asp:ListItem>
<asp:ListItem Text="Each Sponsor" Value="E"></asp:ListItem>
</asp:RadioButtonList>

<div>Inventors</div>
<wtf:FormCheckboxControl ID="cbxRequireInventor" runat="server" LabelText="Requires at least one inventor when disclosure is submitted" />
<wtf:FormCheckboxControl ID="cbxRequireContribution" runat="server" LabelText="Make Contribution required when adding inventor" />
<wtf:FormCheckboxControl ID="cbxRequireType" runat="server" LabelText="Make Type required when adding an inventor" />
<wtf:FormDropDownControl ID="ddlRoleType" runat="server" LabelText="Default Role Type for new inventor:"/> 
<wtf:FormCheckboxControl ID="cbxShowAddress" runat="server" LabelText="Show Address and Phone Number in Contact Search Results" />
<wtf:FormCheckboxControl ID="cbxShowResearchers" runat="server" LabelText="Show Researchers only in Contact Search Results" />
<wtf:FormCheckboxListControl ID="cblShowInventorFields" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal" CellPadding="5" LabelText="Show Fields to General Users on Inventor Section and Add Inventor page:" />
<wtf:FormCheckboxListControl ID="cblShowPermissonFields" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal" CellPadding="5" LabelText="Show Permission options to General Users on Inventor Section and Add Inventor page:" />

<div class="subtitlebar">Digital Signature Settings</div>
    <asp:CheckBox ID="cbxAllowDigitalSignatures" runat="server" Text="Allow electronic signature of disclosure submissions" AutoPostBack="true" OnCheckedChanged="cbxAllowDigitalSignatures_CheckedChanged" />
    <div style="height:10px;"></div>
    <div ID="pnlSignature" runat="server" style="margin-left:20px;">
        <asp:RadioButton ID="rdoChooseTypeEmail" runat="server" GroupName="chooseSignatureType"
            Text="Send signature requests in PDF format through email"
            AutoPostBack="true" OnCheckedChanged="rdoChooseDigitalSignatureType_CheckChanged" />
        
        <div style="margin-left:20px;">
            <wctrl:Template ID="SignatureTerm" runat="server" visible="false"/>
            <wtf:FormCheckboxControl ID="cbxRequireInventorParticipation" runat="server" LabelText="Require signature on Inventor Participation Agreement" Visible="false"/>
            <wtf:FormCheckboxControl ID="cbxRequireFundingSources" runat="server" LabelText="Require signature on funding sources for non-grant funded disclosures" Visible="false"/>
        </div>
        <div style="height:10px;"></div>
        <asp:RadioButton ID="rdoChooseTypeDocuSign" runat="server" GroupName="chooseSignatureType"
            Text="Use DocuSign to request signatures"
            AutoPostBack="true" OnCheckedChanged="rdoChooseDigitalSignatureType_CheckChanged" />
        <div style="margin-left:20px;" ID="divDocusignSettings" Visible="true" runat="server">
            <wctrl:DocusignSettings ID="docusignSettings" runat="server" />
            
            <div>Envelope Signing Order:</div>
            <div style="margin-left:20px;">
                <asp:RadioButton ID="rdoSequentialOrder" runat="server" GroupName="signingOrder" Text="Ordered Signing" Checked="true"/>
                <span class="small_desc">Inventors will sign in order based on Significance value.</span>
            </div>
            <div style="margin-left:20px;">
                <asp:RadioButton ID="rdoSimultaneous" runat="server" GroupName="signingOrder" Text="Unordered Signing"/>
                <span class="small_desc">Inventors can all sign once the DocuSign envelope is created.</span>
            </div>
        <div style="height:10px;"></div>
        </div>
    </div>
</div>
    
<div style="height:20px;"></div>
<wtf:FormButtonControl ID="btnSave" runat="server" Text="Save Changes" LoadingText="Saving..." OnClick="btnSave_Click" />

<wtc:ConfirmPromptControl ID="cpcSuccess" runat="server" PromptType="Success" />
<wtc:ConfirmPromptControl ID="cpcError" runat="server" PromptType="Error"  />


</asp:Content>
