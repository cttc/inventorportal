﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InventorPortal/InventorPortal.Master" AutoEventWireup="true" CodeBehind="PdfTemplate.aspx.cs" Inherits="Inteum.Server.Web.InventorPortal.admin.PdfTemplate" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<tb:TitleBar ID="title1" runat="server" Text="Disclosure PDF Template" />

<div class="content_indent">
<wtf:FormRadioListControl ID="rdoPdfFileName" runat="server" LabelText="Naming convention to use when a user downloads the disclosure as a PDF:" />
<div style="height:8px;"></div>
<wtf:FormCheckboxControl ID="chxHideNoValueFields" runat="server" LabelText="Suppress template fields that have no data for submitted or approved disclosures" />
<div class="subtitlebar">Edit Disclosure PDF Template</div>

<table border="0" cellpadding="3" cellspacing="0">
    <tr>
        <td valign="top">
            <table border="0" cellpadding="3" cellspacing="0">          
                    <tr>
                        <td>            
                            <wtf:FormDropDownControl ID="ddnTemplate" runat="server" LabelText="PDF Template:" AutoPostBack="true" OnSelectedIndexChanged="ddnTemplate_SelectedIndexChanged"/>
                        </td>
                        <td>
                             <wtf:FormTextBoxControl ID="txtTemplateName" runat="server" LabelText="Template Name:" Visible="false"  />
                        </td>
                         <td>
                            <asp:LinkButton ID="btnAdd" runat="server" Text="Add" OnClick="btnAdd_Click" />
                         </td>
                         <td>
                            <asp:LinkButton ID="btnCopy" runat="server" Text="Copy" OnClick="btnCopy_Click" />
                         </td>
                         <td>
                             <asp:LinkButton ID="btnChange" runat="server" Text="Rename" OnClick="btnEdit_Click" />
                         </td>
                         <td>
                            <asp:LinkButton ID="btnDelete" runat="server" Text="Delete" OnClick="btnDelete_Click" />
                            <asp:ConfirmButtonExtender ID="cbeDelete" runat="server" 
                                         TargetControlID="btnDelete" 
                                         ConfirmText="Are you sure you want to delete this template?">
                             </asp:ConfirmButtonExtender>
            
                         </td>
                    </tr>
                   
                     <tr>
                         <td colspan="6">
                          <asp:TextBox ID="txtPdfTemplate" runat="server" TextMode="MultiLine" Rows="30" columns="110"></asp:TextBox>                         
                         </td>
                     </tr>              
            </table>
            <table border="0" cellpadding="3" cellspacing="0">
                <tr>
                    <td>
                        <wtf:FormButtonControl ID="btnSave" runat="server" Text="Save Changes" OnClick="btnSave_Click" />
                    </td>
                    <td>
                        <asp:LinkButton ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                        <asp:ConfirmButtonExtender ID="cbeCancel" runat="server" 
                        TargetControlID="btnCancel" 
                        ConfirmText="Are you sure you want to cancel and lose all the changes?">
                        </asp:ConfirmButtonExtender>
                    </td>
                    <td>&nbsp;&nbsp;</td>
                    <td>
                        <asp:LinkButton ID="lnkReset" runat="server" Text="Reset to default" OnClick="lnkReset_Click"></asp:LinkButton>
                        <asp:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server"
                            TargetControlID="lnkReset"
                            ConfirmText="Are you sure you want to reset your custom template to default template?">
                        </asp:ConfirmButtonExtender>
                    </td>
                </tr>
            </table>
        </td>
        <td>&nbsp;&nbsp;</td>
        <td valign="top">
            <div style="height:8px;"></div>
            <div class="fieldlabel">Available Template Sections:</div>
            <div style="height:30px;"></div>
            <div class="fieldlabel"><asp:ListBox ID="lbxAvailableSections" runat="server" Width="100%"  Height="480px"></asp:ListBox></div>
            <div class="fieldlabel"><asp:Button ID="btnInsertSection" runat="server" Text="Insert Selected Section" OnClick="btnInsertSection_Click" /></div>
        </td>
        <td valign="top">
            <div style="height:8px;"></div>
            <div class="fieldlabel">Available Template Fields:</div>
            <div style="height:30px;"><asp:CheckBox ID="chShowAll" runat="server" Text ="Show Fields for Title and Description" AutoPostBack="true" OnCheckedChanged="chShowAll_CheckedChanged"/></div>
            <div class="fieldlabel"><asp:ListBox ID="lbxAvailableFields" runat="server" Width="100%"  Height="480px" ></asp:ListBox></div>
            <div class="fieldlabel"><asp:Button ID="btnInsertField" runat="server" Text="Insert Selected Field" OnClick="btnInsertField_Click" /></div>
        </td>
    </tr>
</table>
</div>

<wtc:ConfirmPromptControl ID="cpcSuccess" runat="server" PromptType="Success"  />
<wtc:ConfirmPromptControl ID="cpcError" runat="server" PromptType="Error"  />

</asp:Content>
