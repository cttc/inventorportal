﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InventorPortal/InventorPortal.Master" AutoEventWireup="true" CodeBehind="Signature.aspx.cs" Inherits="Inteum.Server.Web.InventorPortal.Signature" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<tb:TitleBar ID="title1" runat="server" Text="Disclosure Digital Signing" />

<div class="content_indent">
<asp:Panel ID="pnlMain" runat="server">
    <div id="divSubtitle" runat="server" class="subtitle">This page allows you to digitally sign a disclosure and accept the final approved changes to the disclosure. You will enter your 
    full name in the box(s) below to digitally sign this disclosure. An administrator will be notified when your digital signature
    is completed.</div>

    <div id="divDisclosureDetails" runat="server" class="subtitlebar">Disclosure Details</div>
    <table border="0" cellpadding="3" cellspacing="0">
        <tr>
            <td><div id="divInventionId" runat="server">Invention ID: </div></td>
            <td><asp:Label ID="lblInventionId" runat="server" Font-Bold="true"></asp:Label></td>
        </tr>
        <tr>
            <td><div id="divDisclosureTitle" runat="server">Disclosure Title: </div></td>
            <td><asp:Label ID="lblDisclosureTitle" runat="server" Font-Bold="true"></asp:Label></td>
        </tr>
    </table>
    <div style="height:5px;"></div>
    <div><asp:HyperLink ID="hypDisclosureDetails" runat="server" Target="_blank">View Full Disclosure Details Here</asp:HyperLink></div>

    <div style="height:20px;"></div>
    <div id="divInventors" runat="server" class="subtitlebar">Inventors</div>
    <telerik:RadGrid runat="server" ID="rgInventors" 
        Width="100%" 
        AllowPaging="false" 
        AllowSorting="true"
        AutoGenerateColumns="false"
        AllowFilteringByColumn="false"
        EnableLinqExpressions="false">
        <ClientSettings>
            <Resizing AllowColumnResize="true" />
        </ClientSettings>
        <MasterTableView NoMasterRecordsText="<div style='padding:10px;'>No inventors to display.</div>">
            <Columns>
                <telerik:GridTemplateColumn HeaderText="Name" ColumnEditorID="colName">
                    <ItemTemplate>
                        <asp:Label ID="lblName" runat="server" Text='<%# FormatName(Eval("ContactsFk")) %>'></asp:Label>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn HeaderText="Permission" ColumnEditorID="colRole" SortExpression="InvPtRole">
                    <ItemTemplate>
                        <asp:Label ID="lblRole" runat="server" Text='<%# FormatRole(Eval("InvPtRole")) %>'></asp:Label>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn HeaderText="Title" DataField="Title" ColumnEditorID="colTitle" SortExpression="Title"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Department" DataField="ClientDepartment.NAME" SortExpression="ClientDepartment.NAME" ColumnEditorID="colDepartment"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Role Type" DataField="InventorRoleType.NAME" ColumnEditorID="colRoleType"></telerik:GridBoundColumn>
                <telerik:GridTemplateColumn HeaderText="Email" ColumnEditorID="colEmail">
                    <ItemTemplate>
                        <asp:Label ID="lblEmail" runat="server" Text='<%# FormatContactEmail(Eval("Contact")) %>'></asp:Label>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn HeaderText="Phone" ColumnEditorID="colPhone">
                    <ItemTemplate>
                        <asp:Label ID="lblPhone" runat="server" Text='<%# FormatContactPhone(Eval("Contact")) %>'></asp:Label>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
            </Columns>
        </MasterTableView>
    </telerik:RadGrid>

    <asp:Panel ID="pnlParticipation" runat="server" Visible="false">
        <div style="height:20px;"></div>
        <div id="divPartcipation" runat="server" class="subtitlebar">Participation Agreement</div>
        <div id="divParticipationSubtitle" runat="server" class="info">The following describes the royalties and payments to be made for each inventor as defined in the disclosure's inventor
        royalty % section. Enter your full name below as shown to agree to the royalties as shown below:</div>
        <telerik:RadGrid runat="server" ID="rgParticipation" 
            Width="100%" 
            OnItemDataBound="rgParticipation_ItemDataBound"
            AllowPaging="false" 
            AllowSorting="true"
            AutoGenerateColumns="false"
            AllowFilteringByColumn="false"
            EnableLinqExpressions="false">
            <ClientSettings>
                <Resizing AllowColumnResize="true" />
            </ClientSettings>
            <MasterTableView NoMasterRecordsText="<div style='padding:10px;'>No inventors to display.</div>">
                <Columns>
                    <telerik:GridTemplateColumn HeaderText="Name" ColumnEditorID="colPartName">
                        <ItemTemplate>
                            <asp:Label ID="lblName" runat="server" Text='<%# FormatName(Eval("ContactsFk")) %>'></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn HeaderText="Royalty" ColumnEditorID="colPartRoyalty">
                        <ItemTemplate>
                            <asp:Label ID="lblRoyalty" runat="server" Text='<%# FormatRoyalty(Eval("Perc_Contr")) %>'></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn HeaderText="Signature Status" UniqueName="SignatureStatus" ColumnEditorID="colPartSignatureStatus">
                        <ItemTemplate>
                            <asp:HiddenField ID="hidContactFk" runat="server" Value='<%# Eval("ContactsFk") %>' />
                            <asp:Label ID="lblSignatureStatus" runat="server" Text='<%# FormatSignatureStatus(Eval("SignType"), Eval("SignDate")) %>' Visible="true"></asp:Label>
                            <asp:Panel ID="pnlSignature" runat="server" Visible="false">
                                <asp:TextBox ID="txtSignature" runat="server"></asp:TextBox>&nbsp;&nbsp;
                                <asp:Label ID="lblFullName" runat="server" Font-Bold="true" Text='<%# FormatName(Eval("ContactsFk")) %>'></asp:Label>
                            </asp:Panel>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>
    </asp:Panel>

    <asp:Panel ID="pnlFunding" runat="server" Visible="false">
        <div style="height:20px;"></div>
        <div id="divFundingSources" runat="server" class="subtitlebar">Funding Sources</div>
        <telerik:RadGrid runat="server" ID="rgFunding" 
            Width="100%" 
            AllowPaging="false" 
            AllowSorting="true"
            AutoGenerateColumns="false"
            AllowFilteringByColumn="false"
            EnableLinqExpressions="false">
            <ClientSettings>
                <Resizing AllowColumnResize="true" />
            </ClientSettings>
            <MasterTableView NoMasterRecordsText="<div style='padding:10px;'>No funding sources to display.</div>">
                <Columns>
                       
                    <telerik:GridBoundColumn HeaderText="Grant Number" DataField="GRANTNUM" ColumnEditorID="colGrantNumber" SortExpression="GRANTNUM"></telerik:GridBoundColumn>
              
                    <telerik:GridTemplateColumn HeaderText="Granted Date" ColumnEditorID="colGrantedDate" SortExpression="GRANTEDDT">
                        <ItemTemplate>
                            <asp:Label ID="lblAwardDate" runat="server" Text='<%# Inteum.Server.DatabaseModel.InteumWeb.Formatters.FormatShortDate(Eval("GRANTEDDT")) %>'></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
          
                    <telerik:GridTemplateColumn HeaderText="Sponsors" ColumnEditorID="colSponsor" UniqueName="colSponsor">
                        <ItemTemplate>
                            <asp:Label ID="lblSponsor" runat="server" Text='<%# FormatSponsors(Eval("GrantSponsors")) %>'></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>

                     <telerik:GridTemplateColumn HeaderText="Investigators" ColumnEditorID="colInvestigator" UniqueName="colInvestigator">
                        <ItemTemplate>
                            <asp:Label ID="lblInventor" runat="server" Text='<%# FormatInvestigators(Eval("GrantInventors")) %>'></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>

                </Columns>
            </MasterTableView>
        </telerik:RadGrid>
        <div style="height:10px;"></div>
        <div><asp:CheckBox ID="cbxNoGrants" runat="server" AutoPostBack="true" OnCheckedChanged="cbxNoGrants_CheckedChanged" 
                Text="No grants were used in the funding of this disclosure." /></div>
        <asp:Panel ID="pnlNoGrants" runat="server">
            <div style="height:10px;"></div>
            <div id="divSignBy" runat="server" class="info">Sign by entering your full name exactly as shown below:</div>
            <table border="0" cellpadding="3" cellspacing="0">
                <tr>
                    <td>
                        <wtf:FormTextBoxControl ID="txtSignGrants" runat="server" IsDisplayLabel="false" />
                    </td>
                    <td>
                        <asp:Label ID="lblNameGrants" runat="server" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </asp:Panel>

    <div style="height:20px;"></div>
    <div id="divSignature" runat="server" class="subtitlebar">Signature Terms</div>
    <div id="divSignatureSubtitle" runat="server" class="info">Read the terms below carefully, then check the box to agree to these terms:</div>
    <div><asp:TextBox ID="txtTerms" runat="server" ReadOnly="true" TextMode="MultiLine" Rows="7" Columns="50"></asp:TextBox></div>
    <div style="height:10px;"></div>
    <div><wtf:FormCheckboxControl ID="cbxTerms" runat="server" LabelText="By digitally signing this document, I agree to the terms listed above and the disclosure information presented." /></div>
    <table border="0" cellpadding="3" cellspacing="0">
        <tr>
            <td>
                <wtf:FormTextBoxControl ID="txtSignTerms" runat="server" IsDisplayLabel="false" />
            </td>
            <td>
                <asp:Label ID="lblNameTerms" runat="server" Font-Bold="true"></asp:Label>
            </td>
        </tr>
    </table>

    <wtf:FormButtonControl ID="btnSign" runat="server" Text="Sign this Disclosure" OnClick="btnSign_Click" LoadingText="Saving Signature..." IsWrapTable="true" />
</asp:Panel>

<asp:Panel ID="pnlAlreadySigned" runat="server" Visible="false">
    <div class="subtitle" id="divAlreadySigned" runat="server">Sorry, you have already signed this disclosure and cannot sign it again. Below is the current status of the signatures for this disclosure.</div>
    <telerik:RadGrid runat="server" ID="rgSignatureStatus" 
        Width="100%" 
        AllowPaging="false" 
        AllowSorting="true"
        AutoGenerateColumns="false"
        AllowFilteringByColumn="false"
        EnableLinqExpressions="false">
        <ClientSettings>
            <Resizing AllowColumnResize="true" />
        </ClientSettings>
        <MasterTableView NoMasterRecordsText="<div style='padding:10px;'>No inventors to display.</div>">
            <Columns>
                <telerik:GridTemplateColumn HeaderText="Name" ColumnEditorID="colSignName">
                    <ItemTemplate>
                        <asp:Label ID="lblName" runat="server" Text='<%# FormatName(Eval("ContactsFk")) %>'></asp:Label>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn HeaderText="Signature Status" ColumnEditorID="colSignStatus">
                    <ItemTemplate>
                        <asp:Label ID="lblSignatureStatus" runat="server" Text='<%# FormatSignatureStatus(Eval("SignType"), Eval("SignDate")) %>'></asp:Label>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
            </Columns>
        </MasterTableView>
    </telerik:RadGrid>
</asp:Panel>

<asp:Panel ID="pnlNotInventor" runat="server" Visible="false">
    <div class="subtitle" id="divNotInventor" runat="server">You are not an inventor on this disclosure and cannot be granted access to view the details of this disclosure.</div>
    <div style="height:10px;"></div>
    <div><a href="mydisclosures.aspx"><span id="spanGoToMyDisclosures1" runat="server">Go to My Disclosures</span></a></div>
</asp:Panel>

<asp:Panel ID="pnlNotApproved" runat="server" Visible="false">
    <div class="subtitle" id="divNotReadyToSign" runat="server">This disclosure is not ready to sign yet because it has not been approved. Either submit this disclosure and wait for approval or contact a system administrator for more details.</div>
    <div style="height:10px;"></div>
    <div><a href="mydisclosures.aspx"><span id="spanGoToMyDisclosures2" runat="server">Go to My Disclosures</span></a></div>
</asp:Panel>

<asp:Panel ID="pnlSignaturesNotAllowed" runat="server" Visible="false">
    <div class="subtitle" id="divSignaturesNotEnabled" runat="server">Digital signatures are not enabled for this Inventor Portal. An administrator must enable digital signatures in the system admin.</div>
    <div style="height:10px;"></div>
    <div><a href="mydisclosures.aspx"><span id="spanGoToMyDisclosures3" runat="server">Go to My Disclosures</span></a></div>
</asp:Panel>
</div>

<wtc:ConfirmPromptControl ID="cpcError" runat="server" PromptType="Error" PromptButtons="Ok"/>
<wtd:ModalDialogControl ID="mdcSuccess" runat="server" IsDefaultButtons="false" IsHideTopClose="true" Title="Signature Received">
    <DialogContent>
        <div id="divSuccessMsg" runat="server">You have successfully digitally signed this disclosure. An administrator has been notified
        of your signature and will contact you if there are any further steps.</div>
    </DialogContent>
    <DialogFooter>
        <asp:Button ID="btnOk" runat="server" Text="OK" OnClientClick="window.location.href='mydisclosures.aspx'" />
    </DialogFooter>
</wtd:ModalDialogControl>

</asp:Content>
