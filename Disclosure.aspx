﻿<%@ Page Title="" 
        Language="C#" 
        MasterPageFile="~/InventorPortal/InventorPortal.Master" 
        AutoEventWireup="true" 
        CodeBehind="Disclosure.aspx.cs" 
        Inherits="Inteum.Server.Web.InventorPortal.Disclosure" 
        EnableEventValidation="false"
         %>
<%@ Register src="~/WebControls/FormCustomFields.ascx" tagname="FormCustomFields" tagprefix="wctrl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
    function toggleExpCol(anchor, divId) {
        var toToggle = document.getElementById(divId);
        if (toToggle.style.display == 'none') {
            toToggle.style.display = 'block';
            anchor.setAttribute("class", "disc_exp");
            anchor.setAttribute("className", "disc_exp");
        }
        else {
            toToggle.style.display = 'none';
            anchor.setAttribute("class", "disc_col");
            anchor.setAttribute("className", "disc_col");
        }
    }
</script>
<script type="text/javascript">
    var isDirty = false;
    var confirmExitPrompt = '<%= ConfirmExitPrompt_String() %>';
    function setDirtyFlag() {
        isDirty = true;
        if (typeof customSetDirtyFlag === "function")
            customSetDirtyFlag()

    }

    function releaseDirtyFlag() {
        isDirty = false;
        if (typeof customReleaseDirtyFlag === "function")
            customReleaseDirtyFlag()
    }

    window.onbeforeunload = confirmExit;
    function confirmExit() {
        if (isDirty) {
            return confirmExitPrompt;
        } 
    }

    var nav = window.Event ? true : false;
    if (nav) {
        window.captureEvents(Event.KEYDOWN);
        window.onkeydown = NetscapeEventHandler_KeyDown;
    } else {
        document.onkeydown = MicrosoftEventHandler_KeyDown;
    }

    function NetscapeEventHandler_KeyDown(e) {
        if (e.which == 13 && e.target.type != 'textarea' && e.target.type != 'submit') {
            return false;
        }
        return true;
    }

    function MicrosoftEventHandler_KeyDown() {
        if (event.keyCode == 13 && event.srcElement.type != 'textarea' &&
event.srcElement.type != 'submit')
            return false;
        return true;
    }
</script>


</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:Panel ID="pnlMain" runat="server">
    <tb:TitleBar ID="title1" runat="server" Text="Disclosure" />

    <div class="content_indent">
    <table border="0" cellpadding="3" cellspacing="0">
        <tr>
            <td>
                <asp:UpdatePanel ID="updatePanelSaveAsDraft" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <wtf:FormButtonControl ID="btnSaveAsDraftTop" runat="server" CssButton="SaveAsDraftTopButton" Text="Save As Draft" OnClientClick="releaseDirtyFlag();" OnClick="btnSaveAsDraft_Click" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td><asp:Button ID="btnDownloadAsPdf" CssClass="PDFDownloadLink" runat="server" Text="Download As PDF" OnClientClick="releaseDirtyFlag();" OnClick="btnDownloadPdf_Click" /></td>
            <td><asp:Button ID="btnDownloadAsWord" CssClass="WordDownloadLink" runat="server" Text="Download As Word" OnClientClick="releaseDirtyFlag();" OnClick="btnDownloadWord_Click" /></td>
            <td style="width:400px;"></td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:Panel ID="panelDocusign" runat="server" CssClass="docusignbox" Visible="false">
                    <div ID="divDocusignTitle" runat="server" class="docusigntitle">DocuSign Envelope Status: <span ID="spanDocusignStatus" class="docusigntitlestatus" runat="server">unknown</span> </div>
                    <div ID="divDocusignError" runat="server" class="docusignspacer" Visible="false">test error text</div>
                    <div ID="divDocusignErrorDetails" runat="server" class="docusignerrorspacer" Visible="false"></div>
                    <div ID="divButtonRow" runat="server" class="statusspacercentered" Visible="false">
                        <wtf:FormButtonControl ID="btnResendDocusign" runat="server" IsShowLoadingImageOnly="true" IsWrapTable="false" OnClick="btnCreateEnvelope_Click" 
                                               OnClientClick="releaseDirtyFlag();" Text="Create Envelope" />
                    </div>
                </asp:Panel>
            </td>
        </tr>
    </table>

    <asp:UpdatePanel ID="updatePanelDisclosure" runat="server" UpdateMode="Conditional" >
        <ContentTemplate>
            <div style="height:5px;"></div>
            <wtc:ConfirmInfoControl ID="cicInfoTop" runat="server" />
            <div style="height:5px;"></div>

            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td valign="top">
                     <table border="0" cellpadding="0" cellspacing="5" width="100%">
                     <tr>  
                         <td valign="top" style="min-width: 150px;">                     
                          <wtf:FormDropDownControl ID="ddnType" runat="server"  LabelText="Disclosure Type:" AutoPostBack="true" OnSelectedIndexChanged="ddnType_SelectedIndexChanged" />
                          <wtf:FormTextBoxControl ID="txtDisclosureStatus" runat="server"  LabelText="Status:" Enabled="false" />
                       </td> 
                         <td colspan="3"> 
                        <wtf:FormTextBoxControl ID="txtDisclosureTitle" runat="server" TextMode="MultiLine" Rows="5" Columns="80" LabelText="Disclosure Title:" />
                         </td> 
                     </tr>
                     <tr>  
                        <td> <wtf:FormTextBoxControl ID="txtDisclosureID" runat="server"  LabelText="Invention Id:" Enabled="false"/></td>
                        <td> <wtf:FormTextBoxControl ID="txtTechID" runat="server"  LabelText="Tech Id:" Enabled="false" /></td>
                       
                        </tr>
                         <tr>
                              <td> <wtf:FormTextBoxControl ID="txtOrigSubmitDate" runat="server"  LabelText="Original Submitted Date:" Enabled="false" visible="false"/></td>
                              <td> <wtf:FormTextBoxControl ID="txtLastSubmitDate" runat="server"  LabelText="Last Submitted Date:" Enabled="false" visible="false"/></td>
                         </tr>
                       </table>
                    </td>
 
                        <td style="width:50px;">
                        </td>
                        <td style="width:220px;" valign="top">
                            <asp:Panel ID="pnlStatusDraft" runat="server" CssClass="statusdraftbox">
                                <div ID="divDraft" runat="server" class="statustitle">
                                    Draft</div>
                                <div ID="divDraftSubtitle" runat="server" class="statusspacer">
                                    This disclosure is in draft status. When you are finished editing, Submit the 
                                    disclosure for administrator review using the button below.</div>
                                <div ID="divDraftSaveAsDraft" runat="server" class="statusspacercentered">
                                    <wtf:FormButtonControl ID="lnkSaveAsDraftStatus" runat="server" 
                                        IsLinkButton="true" IsShowLoadingImageOnly="true" IsWrapTable="false"  
                                        OnClick="btnSaveAsDraft_Click" OnClientClick="releaseDirtyFlag();" 
                                        Text="Save As Draft" />
                                </div>
                                <div ID="divDraftSubmitForReview" runat="server" class="statusspacercentered">
                                    <wtf:FormButtonControl ID="btnSubmit" runat="server"
                                        IsShowLoadingImageOnly="true" IsWrapTable="false" OnClick="btnSubmit_Click" 
                                        OnClientClick="releaseDirtyFlag();" Text="Submit for Review" />
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="pnlStatusSubmitted" runat="server" CssClass="statussubmittedbox">
                                <div ID="divSubmitted" runat="server" class="statustitle">
                                    Submitted</div>
                                <div ID="divSubmittedSubtitle" runat="server" class="statusspacer">
                                    This disclosure has been submitted. You cannot edit the disclosure but remarks 
                                    can still be added. An administrator will review this disclosure.</div>
                                <div ID="divSubmittedStatus" runat="server" class="statusspacercentered">
                                    <wtf:FormDropDownControl ID="ddnStatus" runat="server" LabelText="Status:" AutoPostBack="true" OnSelectedIndexChanged="ddnStatus_SelectedIndexChanged"/>
                                </div>
                                <div ID="divSubmittedApprove" runat="server" class="statusspacercentered">
                                    <wtf:FormButtonControl ID="btnApprove" runat="server" 
                                        IsShowLoadingImageOnly="true" IsWrapTable="false" OnClick="btnApprove_Click" 
                                        OnClientClick="releaseDirtyFlag();" Text="Approve" />
                                </div>
                                <div ID="divSubmittedBackToDraft" runat="server" class="statusspacercentered">
                                    <wtf:FormButtonControl ID="lnkBackToDraftSubmitted" runat="server" 
                                        IsLinkButton="true" IsShowLoadingImageOnly="true" IsWrapTable="false" 
                                        OnClick="lnkBackToDraft_Click" OnClientClick="releaseDirtyFlag();" 
                                        Text="Set Back to Draft" />
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="pnlStatusApproved" runat="server" CssClass="statusapprovedbox">
                                <div ID="divApproved" runat="server" class="statustitle">
                                    Approved</div>
                                <div ID="divApprovedSubtitle" runat="server" class="statusspacer">
                                    This disclosure has been approved. You cannot edit the disclosure but remarks 
                                    can still be added. Check back often for updates on this disclosure such as new 
                                    patents or agreements.</div>
                                <div ID="divApprovedBackToDraft" runat="server" class="statusspacercentered">
                                    <wtf:FormButtonControl ID="lnkBackToDraftApproved" runat="server" 
                                        IsLinkButton="true" IsShowLoadingImageOnly="true" IsWrapTable="false" 
                                        OnClick="lnkBackToDraft_Click" OnClientClick="releaseDirtyFlag();" 
                                        Text="Set Back to Draft" />
                                </div>
                            </asp:Panel>
                        </td>
                        <td style="width:20px;">
                        </td>
                    </tr>
                </tr>
            </table>
            <div style="height:10px;"></div>
            <wctrl:FormCustomFields ID="customFields1" runat="server" />  
            <div style="height:5px;"></div>
            <wtc:ConfirmInfoControl ID="cicInfoBottom" runat="server" />
            </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdateProgress ID="progress" runat="server" DynamicLayout="true" DisplayAfter="0" AssociatedUpdatePanelID="updatePanelDisclosure">
            <ProgressTemplate>
                <div class="loadingTemplate">
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress> 
       
    <div style="height:5px;"></div>

    <table border="0" cellpadding="3" cellspacing="0">
        <tr>
            <td>
                <asp:UpdatePanel ID="updatePanelSaveAsDraftBottom" runat="server">
                    <ContentTemplate>
                        <wtf:FormButtonControl ID="btnSaveAsDraftBottom" runat="server" CssButton="SaveAsDraftBottomButton" Text="Save As Draft" OnClientClick="releaseDirtyFlag();" OnClick="btnSaveAsDraft_Click" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
             <td>
                <asp:UpdatePanel ID="updatePanelSubmitBottom" runat="server">
                    <ContentTemplate>
                        <wtf:FormButtonControl ID="btnSubmitBottom" runat="server"  IsShowLoadingImageOnly="true" IsWrapTable="false" OnClick="btnSubmit_Click"  OnClientClick="releaseDirtyFlag();" Text="Submit for Review" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td><asp:Button ID="btnDownloadAsPdfBottom" runat="server" CssClass="PDFDownloadLink" Text="Download As PDF" OnClientClick="releaseDirtyFlag();" OnClick="btnDownloadPdf_Click" /></td>
            <td><asp:Button ID="btnDownloadAsWordBottom" runat="server" CssClass="WordDownloadLink" Text="Download As Word" OnClientClick="releaseDirtyFlag();" OnClick="btnDownloadWord_Click" /></td>

        </tr>
    </table>
    </div>
</asp:Panel>

<wtc:ConfirmPromptControl ID="cpcError" runat="server" PromptType="Error" />
<wtc:ConfirmPromptControl ID="cpcChangeType" runat="server" PromptType="Warning" PromptButtons="YesNo" OnPromptButtonClick="cpcChangeType_PromptButtonClick" />
<wtc:ConfirmPromptControl ID="cpcSubmit" runat="server" PromptType="Information" PromptButtons="YesNo" OnPromptButtonClick="cpcSubmit_PromptButtonClick" />

<wtd:ModalDialogControl ID="mdcNewDisclosure" runat="server" Title="New Disclosure" IsDefaultButtons="false" IsHideTopClose="true" Width="600">
    <DialogContent>
        <div id="divNewDisclosureSubtitle" runat="server" class="info">You are creating a new disclosure. Before you can edit your disclosure, you must first enter a title
        and choose the type of disclosure.</div>
        <wtf:FormTextBoxControl ID="txtNewDisclosureTitle" runat="server" LabelText="Disclosure Title:" TextMode="MultiLine" Rows="5" Columns="70" />
        <wtf:FormDropDownControl ID="ddnNewType" runat="server" LabelText="Choose the Type of Disclosure:" />
    </DialogContent>
    <DialogFooter>
        <table border="0" cellpadding="0" cellspacing="0" width="100%"><tr><td align="center">
        <table border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <wtf:FormButtonControl ID="btnNewDisclosure" runat="server" Text="Create New Disclosure"  OnClientClick="releaseDirtyFlag();" OnClick="btnNewDisclosure_Click" IsShowLoadingImageOnly="true" />
                </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>
                    <a href="mydisclosures.aspx"><span id="lnkCancel1"  runat="server">Cancel</span></a>
                </td>
            </tr>
        </table>
        </td></tr></table>
    </DialogFooter>
</wtd:ModalDialogControl>

<wtd:ModalDialogControl ID="mdcChooseType" runat="server" Title="Choose Type of Disclosure" IsDefaultButtons="false" IsHideTopClose="true">
    <DialogContent>
        <div id="divChooseSubtitle" runat="server" class="info">This disclosure does not have a type associated with it. You must choose the type of disclosure before continuing.</div>
        <wtf:FormDropDownControl ID="ddnChooseType" runat="server" LabelText="Choose the Type of Disclosure:" />
    </DialogContent>
    <DialogFooter>
        <table border="0" cellpadding="0" cellspacing="0" width="100%"><tr><td align="center">
        <table border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <wtf:FormButtonControl ID="btnChooseType" runat="server" Text="Save Changes"  OnClientClick="releaseDirtyFlag();" OnClick="btnChooseType_Click" />
                </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>
                    <a href="mydisclosures.aspx"><span id="lnkCancel2" runat="server">Cancel</span></a>
                </td>
            </tr>
        </table>
        </td></tr></table>
    </DialogFooter>
</wtd:ModalDialogControl>

<wtd:ModalDialogControl ID="mdcChooseRSC" runat="server" Title="Confirm Record Security Collection"   IsDefaultButtons="false" IsHideTopClose="true" RepositionMode="RepositionOnWindowResizeAndScroll" ModalTop="100" Width="500" >
    <DialogContent>
        <div id="div1" runat="server" class="info">Please confirm the record security collection. It's required by Inteum Web System Settings.</div>
        <wtf:FormDropDownControl ID="ddnChooseGrntAwrdRSC" runat="server" LabelText="Grant/Award Record Security Collection:" />
        <wtf:FormDropDownControl ID="ddnChooseDiscRSC" runat="server" LabelText="Disclosure Record Security Collection:" />
        <wtf:FormDropDownControl ID="ddnChooseTechRSC" runat="server" LabelText="Technology Record Security Collection:" />
        <wtf:FormDropDownControl ID="ddnChooseMktTgtRSC" runat="server" LabelText="Marketing Target Record Security Collection:" />
    </DialogContent>
     <DialogFooter>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr><td align="center">     
                    <wtf:FormButtonControl ID="btnChooseRSC" runat="server" Text="Save Changes" IsShowLoadingImageOnly="true" OnClientClick="releaseDirtyFlag();" OnClick="btnChooseRSC_Click" />
                </td>

            </tr>
        </table>
    </DialogFooter>
</wtd:ModalDialogControl>

</asp:Content>
