﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InventorPortal/InventorPortal.Master" AutoEventWireup="true" CodeBehind="RequestAccount.aspx.cs" Inherits="Inteum.Server.Web.InventorPortal.RequestAccount" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<tb:TitleBar ID="title1" runat="server" Text="Request New Account" />

<div class="content_indent">
<div id="divSubtitle" runat="server" class="subtitle">Please fill in the email address below to submit your request for a new account. If you have logged in to the internal system, you just need to verify your profile on the next page to create the account. Otherwise a verification email will be sent to the email address you provided.</div>
<wtf:FormTextBoxControl ID="txtEmail" runat="server" LabelText="Your Email Address:" Width="300" />

<telerik:RadCaptcha ID="RadCaptchaControl" runat="server" ErrorMessage="The code you entered is not valid." EnableRefreshImage="true">
                         </telerik:RadCaptcha>

 <div style="height:10px;"></div>
 <wtf:FormButtonControl ID="btnSubmit" runat="server" Text="Submit" LoadingText="Submitting..." IsDisplayAsDiv="true" OnClick="btnSubmit_Click" />
 <asp:HiddenField ID="hidRequestAcknowledgeMessage" runat="server" Value="" />

</div>

<wtc:ConfirmPromptControl ID="cpcSuccess" runat="server" PromptType="Success" PromptButtons="Ok" OnPromptButtonClick="cpcSuccess_PromptButtonClick"/>


</asp:Content>
