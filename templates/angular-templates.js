<script type="text/ng-template" id="/templates/maintenanceModal.html">
  <div class="modal-header">
    <h3 class="modal-title" id="modal-title">{{$ctrl.title}}</h3>
      <button type="button" class="close" data-ng-click="$ctrl.cance()" style="position: absolute;right: 5px;top: 5px;">
        <span class="glyphicon glyphicon-remove-circle"></span>
      </button>
    </div>
    <div style="display:inline-flex" class="modal-body" id="modal-body">    
      <div class="col-sm-3 col-md-2 link-card" style="margin-top: 20px; flex: 1; text-align: center;">
        <div class="link-card-icon ng-scope {{$ctrl.getIcon}}"></div>     
      </div>
      <div class="col-sm-9 col-md-10" >
        <p>&nbsp;</p>
        <p></p>
        <p>The Inventor Portal will be down for <strong>maintenance on {{$ctrl.getDates}}</strong>. During this time, there will be no access to the Inventor Portal Service. If you need to submit an invention disclosure during this outage, please <a href="message-us">contact us</a> to request a paper form.</p> <p> We apologize for the inconvenience.</p>
      </div>
    </div>
    <div class="modal-footer text-center">
        <button class="btn btn-warning" type="button" ng-click="$ctrl.cancel()">Close</button>
    </div>
  </div>
</script>