﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InventorPortal/InventorPortal.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Inteum.Server.Web.InventorPortal.Login" %>
<%@ Register src="~/WebControls/SetupAccount.ascx" tagname="SetupAccount" tagprefix="wctrl" %>

<script src="scripts/cttc.js"></script>  
<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular-animate.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular-sanitize.js"></script>
<script src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-2.5.0.js"></script>
<script src="scripts/cttc_app.js"></script>  


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<asp:UpdatePanel ID="updatePanelLogin" runat="server">
<ContentTemplate>
<asp:Panel ID="pnlLogin" runat="server" Visible="true">

    <div class="compat-desktop">
      <h2 id="Browser_compatibility">Browser Compatibility Check</h2>
      <p></p>
      <noscript>
        <div class="awesome-fancy-styling">
          <p>This site requires JavaScript. Please enable JavaScript prior to continuing.</p>
        </div>
        <style>
          table#login_credentials,
          div.htab,
          input[type=submit] {
            display:none;
            visibility: hidden;
          }
          
          #login_credentials_failed {
            display: block;
            visibility: visible;
          }
          
        </style>
      </noscript>
      <script> // Google Analistics
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-25683423-3', 'auto');
        ga('send', 'pageview');

      </script>      <div class="htab"> 
        <div id="compat-desktop" style="display: block;">
          <table class="compat-table">
            <tbody>
              <tr>
                <th>Feature</th>
                <th>Chrome</th>
                <th>Firefox <br />(Gecko)</th>
                <th>Internet <br />Explorer</th>
                <th>Opera</th>
                <th>Safari</th>
              </tr>
              <tr class="version">
                <td>Basic support:</td>
                <td>37.0+</td>
                <td>38.0+</td>
                <td>9.0+</td>
                <td>30.0+</td>
                <td>6.0+</td>
              </tr>
              <tr class="version">
                <td>You have:</td>
                <td id="Chrome" data-value="37"></td>
                <td id="Firefox" data-value="38"></td>
                <td id="IE" data-value="9"></td>
                <td id="Opera" data-value="30"></td>
                <td id="Safari" data-value="6"></td>
              </tr>              
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td valign="top" align="center">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left">
                            <div class="login_prompt"><wtc:ConfirmInfoControl ID="cicInfo" runat="server" /></div>
                            <div class="login">
                               <div class="login_content">
                                <table border="0" cellpadding="3" cellspacing="0" width="90%">
                                <tr >
                            <td align="left" valign="top">
                                <!--<asp:HyperLink ID="hypLogo" runat="server" NavigateUrl="default.aspx"> <asp:Image ID="imgLogo" runat="server" CssClass="logo" />
                                </asp:HyperLink>-->
                            </td>
                            </tr>
                                    <tr>
                                        <td>
                                            <div id="divTitle" runat="server" class="large_subtitle" style="margin-left:150px;">Login to Inventor Portal</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Panel ID="pnlDefaultButtonLogin" runat="server">
                                            <div id="browser_support_error">
                                              <i class="fa fa-info-circle" aria-hidden=""></i>
                                              <p id="error_msg">This site requires JavaScript. Please enable JavaScript prior to continuing.</p>
                                            </div> 
                                            <noscript>
                                              <style>
                                                .login_content > div,
                                                input[type=submit] {
                                                  display:none;
                                                  visibility: hidden;
                                                }
                                                #browser_support_error {
                                                  display: block;
                                                  visibility: visible;
                                                }
                                              </style>
                                            </noscript>  
                                            <table border="0" cellpadding="3" cellspacing="0" style="margin-left:60px; width: 475px;" id="login_credentials">
                                                <!--
                                                <tr>
                                                    <td><div id="divUserName" runat="server">User Name:</div></td>
                                                    <td>&nbsp;&nbsp;</td>
                                                    <td><asp:TextBox ID="txtUsername" runat="server" Width="300"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td><div id="divPassword" runat="server">Password:</div></td>
                                                    <td></td>
                                                    <td><asp:TextBox ID="txtPassword" runat="server" TextMode="Password" autocomplete="off" Width="300"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td style="width:300px;">
                                                        <wtc:ConfirmInfoControl ID="cicFeedback" runat="server" />
                                                    </td>
                                                </tr>
                                                -->
                                                
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td> 
                                                      <p>The Inventor Portal will be down for maintenance <strong>maintenance on Wednesday, September 13th starting at 11:30 CST. Service should resume at 12:00 CST</strong>. During this time, there will be no access to the Inventor Portal Service. If you need to submit an invention disclosure during this outage, please <a href="message-us">contact us</a> to request a paper form.</p>
          <p> We apologize for the inconvenience.</p>
                                                </td>
                                                </tr>
                                                
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <!--
                                                    <td>
                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td id="tdForgotPassword" runat="server">
                                                                    <a href="forgotpassword.aspx" id="hrefForgotPassword" runat="server" class="nav_login">Forgot password?</a>&nbsp;&nbsp;&nbsp;</td>
                                                                    -->
                                                                <td>
                                                                   <!-- <a href="http://cttc.co/message-us/inventorportal" class="nav_login">Questions or Comments?</a> -->
                                                                    <!--<a href="Feedback.aspx" id="hrefFeedback" runat="server" class="nav_login">Questions or Comments?</a>-->
                                                                </td> 
                                                                <!--
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    -->
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td>
                                                        <!--
                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td>
                                                                    <wtf:FormButtonControl ID="btnLogin" runat="server" Text="Login" CssButton="loginButton" OnClick="btnLogin_Click" IsWrapTable="true" />
                                                                </td>
                                                                <td style="width:20px;">&nbsp;</td>
                                                                <td>
                                                                    <wtf:FormButtonControl ID="btnCreateAccount" runat="server" Text="Request Account" CssButton="requestAccountButton" OnClick="btnRequest_Click" IsWrapTable="true" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        -->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3" align="center">
                                                        <wtf:FormLoadingIndicator ID="fli" runat="server" AssociatedUpdatePanelID="updatePanelLogin" Text="Logging in..." DynamicLayout="true" StyleValue="margin-top:50px;" />
                                                    </td>
                                                </tr>
                                            </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    </table>
                               </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    
    <script type="text/javascript">
      var browser = get_browser();        
      var el = document.getElementById(browser.name);       
      var minVer = el.getAttribute('data-value');
      
      if(  parseFloat(browser.version) >=  parseFloat(minVer) ) 
        el.innerHTML = "<i class='fa fa-check' style='color: green; font-size:2em;'></i>";
      else {
        el.innerHTML = "<i class='fa fa-times' style='color: green; font-size:2em;'></i>";

        var errMsg = document.getElementById("error_msg");
        errMsg.innerHTML = 'Your browser (' + browser.name  + ' version: ' + browser.version + ') is out of date. It may have known security flaws and may not display all features of this websites. Please update your browser and try again.';
        
        /* Hide the login */
        el = document.getElementById('login_credentials').className += " version_error";        
        /* Show the error message */
        el = document.getElementById('browser_support_error').className += " version_error";
      }
    </script>              
    
</asp:Panel>

<asp:Panel ID="pnlTerms" runat="server" Visible="false">
    <tb:TitleBar ID="titleTerms" runat="server" Text="Agree to Terms of Use" />
    <div class="content_indent">
    <div id="divTerms" runat="server" class="subtitle">This site requires that all users agree to the following terms of use before accessing the site's functionality.</div>
    <asp:TextBox ID="txtTerms" runat="server" ReadOnly="true" TextMode="MultiLine" Columns="100" Rows="10"></asp:TextBox>
    <wtf:FormCheckboxControl ID="cbxAgree" runat="server" LabelText="I agree to terms of use listed above" />
    <wtf:FormButtonControl ID="btnAgree" runat="server" Text="Continue to Inventor Portal" OnClick="btnAgree_Click" IsWrapTable="true" />
    </div>
</asp:Panel>

<%--<asp:Panel ID="pnlSetupAccount" runat="server" Visible="false">
    <tb:TitleBar ID="titleSetup" runat="server" Text="Account Setup Required" />
    <div class="content_indent">
    <div id="divSubtitleSetup" runat="server" class="subtitle">This is the first time you are logging into Inventor Portal. Before you can continue, we need to associate your account 
    with an existing Inteum contact.</div>
    <wctrl:SetupAccount ID="setupAccount1" runat="server" IsDisplayCredentials="false" OnSelectionMade="setupAccount1_SelectionMade" />
    <wtf:FormButtonControl ID="btnSave" runat="server" Text="Create an Account" OnClick="btnSave_Click" LoadingText="Saving..." IsWrapTable="true" />
    </div>
</asp:Panel>--%>

</ContentTemplate>
</asp:UpdatePanel>

<wtd:ModalDialogControl ID="mdcNextSteps" runat="server" IsDefaultButtons="false" Title="Next Steps" IsHideTopClose="true">
    <DialogContent>
        <asp:Label ID="lblNextSteps" runat="server"></asp:Label>
    </DialogContent>
    <DialogFooter>
        <asp:Button ID="btnOk" runat="server" Text="OK" OnClick="btnOk_Click" />
    </DialogFooter>
</wtd:ModalDialogControl>

</asp:Content>


<button type="button" class="btn btn-default" ng-click="$ctrl.openComponentModal()">Testing</button>
     