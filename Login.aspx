﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InventorPortal/InventorPortal.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Inteum.Server.Web.InventorPortal.Login" %>
<%@ Register src="~/WebControls/SetupAccount.ascx" tagname="SetupAccount" tagprefix="wctrl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<asp:UpdatePanel ID="updatePanelLogin" runat="server">
<ContentTemplate>
<asp:Panel ID="pnlLogin" runat="server" Visible="true">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td valign="top" align="center">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left">
                            <div class="login_prompt"><wtc:ConfirmInfoControl ID="cicInfo" runat="server" /></div>
                            <div class="login">
                               <div class="login_content">
                                <table border="0" cellpadding="3" cellspacing="0" style="width: 95%">
                                <tr >
                            <td align="left" valign="top">
                                <asp:HyperLink ID="hypLogo" runat="server" NavigateUrl="default.aspx"> <asp:Image ID="imgLogo" runat="server" CssClass="logo" />
                                </asp:HyperLink>
                            </td>
                                    <td align="right" valign="top">
                                        <div><asp:Label runat="server" ID="tbVersion" /></div>
                                        <div><asp:Label runat="server" ID ="tbBuildDate" /></div>
                                    </td>
                            </tr>
                                    <tr>
                                        <td>
                                            <div id="divTitle" runat="server" class="large_subtitle" style="margin-left:150px;">Login to Inventor Portal</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            
                                            <table border="0" cellpadding="3" cellspacing="0" style="margin-left:60px;">
                                                <tr>
                                                    <td><div id="divUserName" runat="server">User Name:</div></td>
                                                    <td>&nbsp;&nbsp;</td>
                                                    <td><asp:TextBox ID="txtUsername" runat="server" Width="300"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td><div id="divPassword" runat="server">Password:</div></td>
                                                    <td></td>
                                                    <td><asp:TextBox ID="txtPassword" runat="server" TextMode="Password" autocomplete="off" Width="300"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td style="width:300px;">
                                                        <wtc:ConfirmInfoControl ID="cicFeedback" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td>
                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td id="tdForgotPassword" runat="server">
                                                                    <a href="forgotpassword.aspx" id="hrefForgotPassword" runat="server" class="nav_login">Forgot password?</a>&nbsp;&nbsp;&nbsp;</td>
                                                                <td>
                                                                    <a href="Feedback.aspx" id="hrefFeedback" runat="server" class="nav_login">Questions or Comments?</a>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td>
                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td>
                                                                    <wtf:FormButtonControl ID="btnLogin" runat="server" Text="Login" CssButton="loginButton" OnClick="btnLogin_Click" IsWrapTable="true" />
                                                                </td>
                                                                <td style="width:20px;">&nbsp;</td>
                                                                <td>
                                                                    <wtf:FormButtonControl ID="btnCreateAccount" runat="server" Text="Request Account" CssButton="requestAccountButton" OnClick="btnRequest_Click" IsWrapTable="true" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                             
                                            </table>
                     
                                        </td>
                                    </tr>
                                </table>
                               </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Panel>

<asp:Panel ID="pnlTerms" runat="server" Visible="false">
    <tb:TitleBar ID="titleTerms" runat="server" Text="Agree to Terms of Use" />
    <div class="content_indent">
    <div id="divTerms" runat="server" class="subtitle">This site requires that all users agree to the following terms of use before accessing the site's functionality.</div>
    <asp:TextBox ID="txtTerms" runat="server" ReadOnly="true" TextMode="MultiLine" Columns="100" Rows="10"></asp:TextBox>
    <wtf:FormCheckboxControl ID="cbxAgree" runat="server" LabelText="I agree to terms of use listed above" />
    <wtf:FormButtonControl ID="btnAgree" runat="server" Text="Continue to Inventor Portal" OnClick="btnAgree_Click" IsWrapTable="true" />
    </div>
</asp:Panel>

</ContentTemplate>
</asp:UpdatePanel>

<wtd:ModalDialogControl ID="mdcNextSteps" runat="server" IsDefaultButtons="false" Title="Next Steps" IsHideTopClose="true">
    <DialogContent>
        <asp:Label ID="lblNextSteps" runat="server"></asp:Label>
    </DialogContent>
    <DialogFooter>
        <asp:Button ID="btnOk" runat="server" Text="OK" OnClick="btnOk_Click" />
    </DialogFooter>
</wtd:ModalDialogControl>

</asp:Content>
