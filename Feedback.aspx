﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InventorPortal/InventorPortal.Master" AutoEventWireup="true" CodeBehind="Feedback.aspx.cs" Inherits="Inteum.Server.Web.InventorPortal.Feedback" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<tb:TitleBar ID="title1" runat="server" Text="Submit Feedback or Questions" />

<div class="content_indent">
<div id="divSubtitle" runat="server" class="subtitle">Fill in the form below to submit your feedback or questions to a site administrator.</div>
<wtf:FormTextBoxControl ID="txtEmail" runat="server" LabelText="Your Email Address:" Width="250" />
<wtf:FormTextBoxControl ID="txtFeedback" runat="server" LabelText="Feedback / Comments / Questions:" TextMode="MultiLine" Rows="10" Columns="80" />
<telerik:RadCaptcha ID="RadCaptchaControl" runat="server" ErrorMessage="The code you entered is not valid." EnableRefreshImage="true"></telerik:RadCaptcha>
<wtf:FormButtonControl ID="btnSubmit" runat="server" Text="Submit" LoadingText="Submitting..." IsDisplayAsDiv="true" OnClick="btnSubmit_Click" />
</div>

<wtc:ConfirmPromptControl ID="cpcSuccess" runat="server" PromptType="Success" PromptButtons="Ok" />

</asp:Content>
