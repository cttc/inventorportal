﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InventorPortal/InventorPortal.Master" AutoEventWireup="true" CodeBehind="MyDisclosures.aspx.cs" Inherits="Inteum.Server.Web.InventorPortal.MyDisclosures" %>
<%@ Register src="~/WebControls/DateRange.ascx" tagname="DateRange" tagprefix="wctrl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<tb:TitleBar ID="title1" runat="server" Text="Disclosures" />

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td><wtf:FormSearchControl ID="txtSearch" runat="server" IsDisplayLabel="false" OnClick="txtSearch_Click" CssTextBox="SearchBox"/></td>
        <td style="width:10px;"></td>
        <td><span id="spanType" runat="server">Type:</span>&nbsp;</td>
        <td><asp:DropDownList ID="ddnType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddnType_SelectedIndexChanged"></asp:DropDownList></td>
        <td style="width:10px;"></td>
        <td><span id="spanStatus" runat="server">Review Stage:</span>&nbsp;</td>
        <td><asp:DropDownList ID="ddnStatus" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddnStatus_SelectedIndexChanged"></asp:DropDownList></td>
        <td style="width:10px;"></td>
        <td><span id="spanDateRange" runat="server">Created Date:</span>&nbsp;</td>
        <td><asp:DropDownList ID="ddnDateRange" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddnDateRange_SelectedIndexChanged"></asp:DropDownList></td>
        <td style="width:10px;"></td>
        <td><wtf:FormDateRangeControl ID="dateRange1" runat="server" Visible="false" /></td>
        <td style="width:10px;"></td>
        <td><asp:Button ID="btnFilter" runat="server" Text="Filter" OnClick="btnFilter_Click" Visible="false" /></td>
    </tr>
</table>
<div style="height:20px;"></div>

<telerik:RadGrid runat="server" ID="rgMain"
    Width="99%"
    OnItemDataBound="rg_ItemDataBound"
    AllowPaging="true" 
    AllowCustomPaging="true"
    AllowSorting="true"
    PageSize="20"
    AutoGenerateColumns="false"
    AllowFilteringByColumn="false"
    EnableLinqExpressions ="false"
    OnNeedDataSource="RadGrid_NeedDataSource"
    OnSortCommand="RadGrid_SortCommand">
    <ClientSettings>
        <Resizing AllowColumnResize="true" />
    </ClientSettings>
    <MasterTableView 
        OverrideDataSourceControlSorting="true" 
        NoMasterRecordsText="<div style='padding:10px;'>No disclosures to display.</div>">
        <SortExpressions>
        <telerik:GridSortExpression FieldName="RCreatedD" SortOrder="Descending" />
       </SortExpressions>
        <Columns>
          <%--  <telerik:GridBoundColumn HeaderText="Invention Id" DataField="InventId" ColumnEditorID="colInventionId"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn HeaderText="Tech Id" DataField="Technology.TechId" ColumnEditorID="colTechId"></telerik:GridBoundColumn>--%>

             <telerik:GridTemplateColumn HeaderText="Invention Id" SortExpression="InventId">
                <ItemTemplate>
                    <asp:Label ID="lblInventId" runat="server" Text='<%# FormatName(Eval("InventId")) %>'></asp:Label>
                </ItemTemplate>
            </telerik:GridTemplateColumn>

             <telerik:GridTemplateColumn HeaderText="Tech Id" SortExpression="Technology.TechId">
                <ItemTemplate>
                    <asp:Label ID="lblTechId" runat="server" Text='<%# FormatName(Eval("Technology.TechId")) %>'></asp:Label>
                </ItemTemplate>
            </telerik:GridTemplateColumn>

            <telerik:GridTemplateColumn HeaderText="Title" SortExpression="Name">
                <ItemTemplate>
                    <asp:Label ID="lblName" runat="server" Text='<%# FormatName(Eval("Name")) %>'></asp:Label>
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridBoundColumn HeaderText="Type" DataField="InventionDisclosureType.Name" ColumnEditorID="colType"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn HeaderText="Status" DataField="InventionStatus.Name" ColumnEditorID="colStatus"></telerik:GridBoundColumn>
            <telerik:GridTemplateColumn HeaderText="Review Stage" SortExpression="ReviewStg" ColumnEditorID="colReviewStage">
                <ItemTemplate>
                    <asp:Label ID="lblReviewStg" runat="server" Text='<%# FormatReviewStage(Eval("ReviewStg")) %>'></asp:Label>
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn HeaderText="Inventors" SortExpression="V_DisclosureSummarized.TechInventor">
                <ItemTemplate>
                    <asp:Label ID="lblInventors" runat="server" Text='<%# FormatName(Eval("V_DisclosureSummarized.TechInventors")) %>'></asp:Label>
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn HeaderText="All Signatures Received" SortExpression="V_DisclosureSummarized.AllSignaturesReceived" ColumnEditorID="colSignatures">
                <ItemTemplate>
                    <asp:Label ID="lblAllSignaturesRecvd" runat="server" Text='<%# FormatYesNo(Eval("V_DisclosureSummarized.AllSignaturesReceived")) %>'></asp:Label>
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn HeaderText="Entered By" SortExpression="RCreatedU" ColumnEditorID="colEnteredBy">
                <ItemTemplate>
                    <asp:Label ID="lblEnteredBy" runat="server" Text='<%# FormatEnteredBy(Eval("RCreatedU")) %>'></asp:Label>
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn HeaderText="Created Date" SortExpression="RCreatedD" ColumnEditorID="colCreatedDate">
                <ItemTemplate>
                    <asp:Label ID="lblCreatedDate" runat="server" Text='<%# FormatCreatedDate(Eval("RCreatedD")) %>'></asp:Label>
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn HeaderText="Manage" AllowFiltering="false" HeaderStyle-Width="200" ColumnEditorID="colManage">
                <ItemTemplate>
                    <asp:HiddenField ID="hidPk" runat="server" Value='<%# Eval("PrimaryKey") %>' />
                    <asp:HiddenField ID="hidTechnolFk" runat="server" Value='<%# Eval("TechnolFk") %>' />
                    <asp:HiddenField ID="hidReviewStg" runat="server" Value='<%# Eval("ReviewStg") %>' />
                    <asp:HyperLink ID="hypEdit" runat="server" Text="<%# View_String() %>" NavigateUrl='<%# "disclosure.aspx?id=" + Eval("PrimaryKey") %>'></asp:HyperLink>
                    &nbsp;|&nbsp;
                    <asp:LinkButton ID="lnkDelete" runat="server" Text="<%# Delete_String() %>" OnClick="lnkDelete_Click"></asp:LinkButton>
                    <asp:ConfirmButtonExtender ID="cbeDelete" runat="server" 
                        TargetControlID="lnkDelete" 
                        ConfirmText="<%# DeleteConfirm_String() %>">
                    </asp:ConfirmButtonExtender>
                </ItemTemplate>
            </telerik:GridTemplateColumn>
        </Columns>
    </MasterTableView>
</telerik:RadGrid>

<wtc:ConfirmPromptControl ID="cpcError" runat="server" PromptType="Error" PromptButtons="Ok" />

</asp:Content>
