﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ApprovalPending.aspx.cs" Inherits="Inteum.Server.Web.InventorPortal.ApprovalPending" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="css/style.css" rel="stylesheet" type="text/css" id="css1" runat="server" />
    <link rel="SHORTCUT ICON" href="../images/favicon.ico"/>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Panel ID="pnlAwaitingApproval" runat="server" Visible="true">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td valign="top" align="center">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="left">
                                    <div class="login_prompt"><wtc:ConfirmInfoControl ID="cicInfo" runat="server" /></div>
                                    <div class="login">
                                       <div class="login_content">
                                            <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                                <tr >
                                                    <td align="left" valign="top">
                                                        <asp:HyperLink ID="hypLogo" runat="server" NavigateUrl="default.aspx"> <asp:Image ID="imgLogo" runat="server" 
                                                        ImageUrl="../images/InteumLogoSmallPort.png" CssClass="logo"  Visible="false"/>
                                                        </asp:HyperLink>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div id="divTitle" runat="server" class="large_subtitle" style="text-align: left;">Account Awaiting Approval</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div style="height: 30px;" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div style="width: 648px" id="ApprovalPendingMsg" runat="server">
                                                        Your account is awaiting approval.  If inventor portal is configured to auto approve accounts, then your account has been disabled by your administrator. 
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                       </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </form>
</body>
</html>
